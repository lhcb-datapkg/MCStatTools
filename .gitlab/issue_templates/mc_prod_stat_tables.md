## Physics working group details

(Please, mention here any peculiarities about the working group under which the new tables must be registered. This applies 
to working groups split in subgroups who may want the statistics indexed by subgroup name rather than the larger working group name. 
Please, do not forget to choose a label of the form `MCStats-WG_name` to properly mark the physics working group)


## Public temporary repository of new statistics tables

(Put here the location where the files with the new tables data are stored by you. Please, make sure that the files are 
readable to members of the LHCb community - best ask a friend/colleague to test read access with his/her account credentials.
Please, make sure the location is a file path accessible from lxplus shell. Please, keep the directory hierarchy as simple as 
possible - best put all new tables in the same directory. Optionally, consider adding extra data saved during the gathering and 
merging process to ease debugging if issues should occur.)

## Details

(Add here any details about the prepared statistics tables which you consider important to ease solving "conflicts" with 
previously published statistics tables, e.g., if some new table should override already published table for same MC generation 
conditions, please, do specify the production ID)

## Issues

(Please, mention here any issues encountered during generation of the tables and the correspoding production IDs in order 
to ease debugging and raise early red flags on data which may need supplemental inspection.)

/title Update of `PhysWG` MC Statistics Tables
/assign @agrecu
/subscribe