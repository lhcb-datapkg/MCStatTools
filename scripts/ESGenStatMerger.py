# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright CERN for the benefit of the LHCb Collaboration                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Project:     DBASE/MCStatTools
# Name:        ESGenStatMerger.py
# Purpose:     Parse ES records and provide a mechanism for merging the
#              the generator counters. Object model extensible to support
#              other data sources in the future.
#
# Author:      Alex T. GRECU <alexandru.grecu@nipne.ro>
# Created:     19.03.2024 [dd.mm.yyyy]
# -------------------------------------------------------------------------------
"""Module including all XML processing and merging code."""

__version__ = '20240319'

import os
import sys
import xml.dom.minidom as xdom
from xml.parsers.expat import ExpatError
# try:
#     import xmltodict
#     hasXMLToDict = True
# except ImportError:
#     hasXMLtoDict = False
import re
import math
import six

import requests
# import shutil
import json
from datetime import datetime
import zlib
import tarfile
import gzip
from io import BytesIO as zipBytes

import logging

mlog = logging.getLogger('lbMcST.%s' % __name__)
mlog.setLevel(mlog.parent.name == 'root' and logging.INFO or mlog.parent.level)
mlog.info('Module %s v. %s loaded.' % (__name__, __version__))

appUA = 'MCStatTools/6.0.0'

logBaseName = 'GeneratorLog'
xml_log_name = '%s.xml' % logBaseName
# Global list of process IDs to consider for Special Signal generation
specialSignalProcesses = []

# Fill following list with object names for which merging must be done regardless of their presence
# in the GenStats to be merged
# buggyStatObjects = []
# following dictionary defines rare processes for which missing cross-section stats are safe to ignore at merge
# process codes correspond to Pythia 8 definition
# bSO_defaults = {201: 'Prompt photon: q g --> q gamma',
#                 202: 'Prompt photon: q qbar --> g gamma',
#                 221: 'Weak single boson: f fbar --> gamma*/Z0',
#                 222: 'Weak single boson: f fbar --> W+-',
#                 401: 'Charmonium: g g --> ccbar[3S1(1)] g',
#                 402: 'Charmonium: g g --> ccbar[3S1(8)] g',
#                 403: 'Charmonium: q g --> ccbar[3S1(8)] q',
#                 404: 'Charmonium: q qbar --> ccbar[3S1(8)] g',
#                 405: 'Charmonium: g g --> ccbar[1S0(8)] g',
#                 406: 'Charmonium: q g --> ccbar[1S0(8)] q',
#                 407: 'Charmonium: q qbar --> ccbar[1S0(8)] g',
#                 408: 'Charmonium: g g --> ccbar[3PJ(8)] g',
#                 409: 'Charmonium: q g --> ccbar[3PJ(8)] q',
#                 410: 'Charmonium: q qbar --> ccbar[3PJ(8)] g',
#                 411: 'Charmonium: g g --> ccbar[3S1(1)] gamma / ccbar[3PJ(1)] g',
#                 412: 'Charmonium: q g --> ccbar[3PJ(1)] q',
#                 413: 'Charmonium: q qbar --> ccbar[3PJ(1)] g',
#                 414: 'Charmonium: g g --> ccbar[3S1(8)] g',
#                 415: 'Charmonium: q g --> ccbar[3S1(8)] q',
#                 416: 'Charmonium: q qbar --> ccbar[3S1(8)] g',
#                 417: 'Charmonium: g g --> ccbar[3DJ(1)] g',
#                 418: 'Charmonium: g g --> ccbar[3PJ(8)] g',
#                 419: 'Charmonium: q g --> ccbar[3PJ(8)] q',
#                 420: 'Charmonium: q qbar --> ccbar[3PJ(8)] g',
#                 }
# buggyStatObjects += ["pp%d" % x for x in bSO_defaults.keys()]


class GenXmlNode(object):
    xElementName = None
    xAttribNames = []
    xMandatoryAttribNames = []
    xChildNames = []
    
    def __init__(self, xmlElement=None):
        if xmlElement is not None and not isinstance(xmlElement, xdom.Element):
            xmlElement = None
        self._xElem = xmlElement
        self._attribs = dict()
        self._children = dict()
        self._value = None  # property will contain additive quantities only
        self._id = None
        self._mcache = {}
        
    def _dumpCache(self, parent):
        self._mcache = {}
    
    def getAttribute(self, attrId):
        ret = None
        if self.hasAttribute(attrId):
            ret = self._attribs[attrId]
        return ret
    
    def hasAttribute(self, attrId):
        return attrId in self._attribs and self._attribs[attrId] is not None
    
    def hasChild(self, childId):
        childId = str(childId)
        return childId in self._children and self._children[childId] is not None
    
    def getChild(self, childId):
        ret = None
        childId = str(childId)
        if self.hasChild(childId):
            ret = self._children[childId]
        return ret
    
    def getNextSiblingNode(self):
        if self._xElem is None:
            return None
        xnd = self._xElem.nextSibling
        while xnd.nodeType != xdom.Element.ELEMENT_NODE:
            xnd = xnd.nextSibling
            if xnd is None:
                break
        return xnd

    def getId(self):
        return self._id

    def genericId(self, **params):
        """
        Returns the generic (hard-coded) counter id based on params passed
        as a dictionary with key names given by corresponding XML node
        attributes. If no data is provided this is just an alias for getId()
        """
        raise NotImplementedError('This method is only implemented by subclasses.')
    
    def getValue(self):
        return self._value
    
    def isBoundToXml(self):
        return self._xElem is not None
    
    @staticmethod
    def _getInt(value):
        if value is None:
            return None
        if isinstance(value, six.string_types):
            value = value.strip()
        try:
            ret = int(value)
        except ValueError as exx:
            mlog.debug("{0}: {1}".format(sys.exc_info()[0], exx))
            ret = None
        return ret
    
    @staticmethod
    def _getFloat(value):
        if value is None:
            return None
        if isinstance(value, six.string_types):
            value = value.strip()
        try:
            ret = float(value)
        except ValueError as exx:
            mlog.debug("{0}: {1}".format(sys.exc_info()[0], exx))
            ret = None
        return ret
    
    def __convertProperties(self):
        raise NotImplementedError('This method is only implemented in derived classes.')

    # noinspection PyUnresolvedReferences
    def fromXmlElement(self, xElem):
        """
        In subclass re-implementations(overloads), this method returns True when
        critical XML children fail to contain valid (numerical) data, e.g. nan.
        Args:
            xElem (xdom.Element): Element node in XDOM representation of parsed XML file

        Returns:
            void.
        """
        if not isinstance(xElem, xdom.Element):
            raise TypeError('Invalid argument, xml.dom.minidom.Element expected.')
        if xElem.nodeType != xdom.Element.ELEMENT_NODE or \
                xElem.nodeName != self.xElementName or \
                not xElem.hasChildNodes():
            # mlog.debug(self._xElem.toprettyxml())
            raise ValueError('Invalid or malformed <%s> node.' % self.xElementName)
        for an in self.xAttribNames:
            if not xElem.hasAttribute(an):
                msg = '<%s> element missing \'%s\' attribute.' % (self.xElementName, an)
                if an not in self.xMandatoryAttribNames:
                    mlog.warning(msg)
                else:
                    raise ValueError('Malformed ' + msg)
        self._xElem = xElem
        cn = xElem.firstChild
        while cn is not None:
            if cn.nodeType == xdom.Element.TEXT_NODE:
                if cn.nodeValue is not None:
                    tnv = cn.nodeValue.strip()
                    if len(tnv) > 0:
                        if self._value is None:
                            self._value = tnv
                        else:
                            self._value += tnv
            if cn.nodeType != xdom.Element.ELEMENT_NODE:
                cn = cn.nextSibling
                continue
            if cn.nodeName not in self.xChildNames:
                mlog.debug('Unknown child <%s> of <%s> detected. Ignoring...' % (cn.nodeName, self.xElementName))
            else:
                nv = None
                if cn.hasChildNodes():
                    vnn = cn.firstChild
                    while vnn is not None and vnn.nodeType != xdom.Element.TEXT_NODE:
                        vnn = vnn.nextSibling
                    nv = vnn.nodeValue.strip()
                self._children[cn.nodeName] = nv
            cn = cn.nextSibling
        for an in self.xAttribNames:
            self._attribs[an] = None
            if xElem.hasAttribute(an):
                self._attribs[an] = xElem.getAttribute(an)
    
    def hasChildElement(self, ceName, getList=False):
        if not self.isBoundToXml() or not self._xElem.hasChildNodes():
            if getList:
                return []
            else:
                return False
        passChNodes = []
        for cn in self._xElem.childNodes:
            if cn.nodeType != xdom.Element.ELEMENT_NODE:
                continue
            if cn.nodeName == ceName:
                passChNodes.append(cn)
        if getList:
            ret = passChNodes
        else:
            ret = (len(passChNodes) > 0)
        return ret
    
    def toXmlElement(self, XNode):
        if not self.isBoundToXml():
            self._xElem = XNode.ownerDocument.createElement(self.xElementName)
            XNode.appendChild(self._xElem)
        if not self.isBoundToXml():
            raise ValueError('Invalid value for associated XML node.')
        if len(self.xMandatoryAttribNames) > 0:
            for an in self.xMandatoryAttribNames:
                self._xElem.setAttribute(an, 'not set')
        for cn in self.xChildNames:
            tcns = self.hasChildElement(cn, True)
            if len(tcns) == 0:
                tce = self._xElem.ownerDocument.createElement(cn)
                self._xElem.appendChild(tce)
            elif len(tcns) > 1:
                for ctn in tcns[1:]:
                    self._xElem.removeChild(ctn)
                tce = tcns[0]
            else:
                tce = tcns[0]
            tln = list(tce.childNodes)
            for ctn in tln:
                tce.removeChild(ctn)


# noinspection PyAbstractClass
class GenMetaInfo(GenXmlNode):
    """
    Implements the XML-Python interface for global parameters in the .xml log file.
    On 'addition' retains value of first operand and a warning is printed if
    the value of second operand (as stripped, case insensitive string) differs. May be used to
    enforce validity of logs being merged. The Id property is computed by default
    by adding the 'xLog_' prefix (e.g. version -> xLog_version) and used as key
    in the final GenStats dictionary.
    """
    
    def __init__(self, xNode=None):
        super(GenMetaInfo, self).__init__()
        if xNode is not None:
            self.fromXmlElement(xNode)
    
    def fromXmlElement(self, xElem):
        self.xElementName = getattr(xElem, 'nodeName', None)
        if self.xElementName is None:
            raise TypeError('Can get tag name for XML element of type %s.' % (str(xElem.__class__)))
        super(GenMetaInfo, self).fromXmlElement(xElem)
        self._id = self.genericId()
    
    def genericId(self, **params):
        """
        See GenXmlNode method for help.
        """
        return 'xLog_' + self.xElementName
    
    def __add__(self, other):
        if other is None:
            return self
        ov = other.getValue()
        if not isinstance(ov, six.string_types):
            mlog.info("Value type (%s) of <%s> node differs from previous file. Will ignore change..." %
                      (str(ov.__class__), self.xElementName))
        tv = str(self.getValue())
        if ov.strip().lower() != tv.strip().lower():
            mlog.info(
                "Value of <%s> node differs from previous file. \n (this) \"%s\" vs. (new) \"%s\""
                "\nWill ignore change..." % (self.xElementName, tv, ov))
        return self
    
    def _mergeFlush(self, parent):
        pass
    
    def recompute(self, coll):
        pass
    
    def getCounters(self):  # to ensure information is copied in the dict use to generate the web page
        return [(self.getId(), self.getValue()), ]
    
    def toXmlElement(self, xNode):
        xDoc = xNode.ownerDocument
        if not self.isBoundToXml() or self._xElem.ownerDocument != xDoc:
            self._xElem = xDoc.createElement(self.xElementName)
            xNode.appendChild(self._xElem)
        if not self.isBoundToXml():
            raise ValueError('Invalid value for associated XML node.')
        tvn = xDoc.createTextNode(str(self.getValue()).strip())
        self._xElem.appendChild(tvn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum."""
        return {'type': self.xElementName,
                'id': self._id,
                'value': self.getValue()
                }
    
    def fromDict(self, jsDict):
        """
        Populates current instance with data provided in jsDict loaded from JSON.
        """
        self.xElementName = jsDict['type']
        self._id = self.genericId()
        self._value = jsDict['value']
    
    def __repr__(self):
        ret = '%s.%s(\'<%s>%s</%s>\')' % \
              (__name__, self.__class__.__name__, self.xElementName, self.getValue(), self.xElementName)
        return ret


class GenCounter(GenXmlNode):
    """
    XML-Python interface for <counter> nodes. The Id in the final dictionary is
    given by reverse mapping of the mandatory name attribute from idmap dictionary.
    """
    idmap = {'L1': "all events (including empty events)",
             'L2': "events with 0 interaction",
             'N1': "generated events",
             'N2': "generated interactions",
             'N3': "accepted events",
             'N4': "interactions in accepted events",
             'N5': "generated interactions with >= 1b",
             'N6': "generated interactions with >= 3b",
             'N7': "generated interactions with 1 prompt B",
             'N8': "generated interactions with >= 1c",
             'N9': "generated interactions with >= 3c",
             'N10': "generated interactions with >= prompt C",
             'N11': "generated interactions with b and c",
             'N12': "accepted interactions with >= 1b",
             'N13': "accepted interactions with >= 3b",
             'N14': "accepted interactions with 1 prompt B",
             'N15': "accepted interactions with >= 1c",
             'N16': "accepted interactions with >= 3c",
             'N17': "accepted interactions with >= prompt C",
             'N18': "accepted interactions with b and c",
             'N21': "z-inverted events",
             'I13': "generated (bb)", 'I24': "accepted (bb)",
             'I33': "generated (cc)", 'I42': "accepted (cc)"
             }
    xElementName = 'counter'
    xAttribNames = ['name', ]
    xMandatoryAttribNames = ['name', ]
    xChildNames = ['value', ]
    
    def __init__(self, oid='NS0', name=None, value=-1, xelem=None):
        super(GenCounter, self).__init__()
        self._id = oid
        if xelem is not None and isinstance(xelem, xdom.Element):
            self.fromXmlElement(xelem)
        else:
            if name is not None:
                self._attribs['name'] = name
            if value > -1:
                self._children['value'] = value
                self._value = value
    
    def genericId(self, **params):
        """ See GenXmlNode method for help."""
        if len(params) == 0 or 'name' not in params:
            return self.getId()
        oid = ''
        name = params['name']
        for (tid, nn) in GenCounter.idmap.items():
            if name.lower() == nn.lower():
                oid = tid
                break
        if len(oid) == 0:
            return self.getId()
        return oid
    
    def __convertProperties(self):
        corrupted = False
        xmlName = self.getAttribute('name')
        self._id = self.genericId(name=xmlName)
        sv = self.getChild('value')
        rt = GenXmlNode._getInt(sv)
        if rt is None:
            rt = 0
            corrupted = True
        self._children['value'] = rt
        self._value = rt
        return corrupted
    
    def fromXmlElement(self, xElem):
        if not super(GenCounter, self).fromXmlElement(xElem):
            self._id = self.genericId()
        return self.__convertProperties()
    
    def getCounters(self):
        return [(self.getId(), self._children['value']), ]
    
    def __add__(self, other):
        if other is None:
            return self
        if not isinstance(other, GenCounter):
            raise TypeError('You cannot merge GenCounter and %s instances.' % getattr(other, '__class__', 'Unknown'))
        myid = self.getId()
        if myid != other.getId():
            raise ValueError('Makes no sense to add counter \'%s\' to counter \'%s\'.' % (myid, other.getId()))
        vname = self.getAttribute('name')
        if vname != other.getAttribute('name'):
            mlog.warning('\'name\' attributes differ. Will consider value from first operand: \'%s\'.' % vname)
        self._mcache['value'] = 0
        self._mcache['value'] += other.getValue()
        return self
    
    def _mergeFlush(self, parent):
        if 'value' not in self._mcache:
            return
        self._value += self._mcache['value']
        self._children['value'] = self._value
        self._dumpCache(parent)
    
    def recompute(self, coll):
        pass  # for compatibility with other counter types
    
    def toXmlElement(self, xNode):
        super(GenCounter, self).toXmlElement(xNode)
        self._xElem.setAttribute('name', GenCounter.idmap[self._id])
        vn = self.hasChildElement('value', True)
        tvn = vn[0]
        dn = self._xElem.ownerDocument.createTextNode(str(self.getChild('value')))
        tvn.appendChild(dn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum."""
        return {'type': self.xElementName,
                'id': self.genericId(name=self.getAttribute('name')),
                'value': self.getValue()
                }
    
    def fromDict(self, jsDict):
        """ Populates current instance with data provided in jsDict loaded from JSON. """
        self._attribs['name'] = GenCounter.idmap[jsDict['id']]
        self._id = jsDict['id']
        self._value = jsDict['value']
        self._children['value'] = self._value
    
    def __str__(self):
        ret = self.toDict()
        del ret['type']
        tt = '%s%s' % (self.__class__.__name__, str(ret))
        return tt


class GenXSection(GenXmlNode):
    """
    XML-Python interface for <crosssection> nodes. The Id in the final dictionary is
    computed by adding 'pp' prefix to the mandatory 'id' attribute. The final 'value'
    is computed as mean of intermediary values stored in _children['tvalues']! The
    'description' value is kept from the first parsed log file. Default unit is millibarn.
    """
    xElementName = 'crosssection'
    xAttribNames = ['id', ]
    xMandatoryAttribNames = ['id', ]
    xChildNames = ['description', 'generated', 'value']
    
    def __init__(self, xelem=None):
        super(GenXSection, self).__init__()
        self._value = -1.0  # the medium value when merged
        self._procCount = -1
        self._procid = None
        if xelem is not None and isinstance(xelem, xdom.Element):
            self.fromXmlElement(xelem)
    
    def setProcId(self, procId):
        if isinstance(procId, six.string_types):
            procId = int(procId.strip())
        self._procid = procId
        self._id = self.genericId(id=self._procid)
        self._attribs['id'] = procId
    
    def getProcId(self):
        """
        Value of 'id' XML node attribute with no prefix added.
        """
        return self._procid
    
    def genericId(self, **params):
        """
        See GenXmlNode method for help.
        """
        if len(params) == 0 or 'id' not in params.keys():
            return self.getId()
        return 'pp%s' % (str(params['id']))
    
    def getChildValue(self, chName):
        if not isinstance(chName, six.string_types):
            return None
        chname = chName.lower()
        if chname not in GenXSection.xChildNames:
            return None
        if chname not in self._children.keys():
            return None
        return self._children[chname]
    
    def __convertProperties(self):
        corrupted = False
        self.setProcId(self.getAttribute('id'))
        dd = self.getChild('description')
        if dd is not None:  # cleaning the string of trailing non-ascii characters
            self._children['description'] = dd.strip('" *')
        rt = GenXmlNode._getInt(self.getChild('generated'))
        if rt is None:
            rt = 0
            corrupted = True
        self._children['generated'] = rt
        rt = GenXmlNode._getFloat(self.getChild('value'))
        if rt is None:
            rt = 0.0
            corrupted = True
        self._children['value'] = rt
        return corrupted
    
    def fromXmlElement(self, xElem):
        """
        Overload of method in GenXmlNode.
        
        Args:
            xElem (xdom.Element): Element node object from parsed XML DOM

        Returns:
            No return.
        """
        super(GenXSection, self).fromXmlElement(xElem)
        return self.__convertProperties()
    
    def getCounters(self):
        return [(self.getId(), self._children['value']), ]
    
    def __add__(self, other):
        # very important to maintain correct number of xsection values being added -> correct mean value
        if other is None:
            return self
        if not isinstance(other, GenXSection):
            raise TypeError('You cannot add GenXSection instances to %s. Counter \'%s\' not on ignored list.' %
                            (getattr(other, '__class__', 'Unknown'), self._id))
        if self.getId() != other.getId():
            raise ValueError('Makes no sense to add cross-sections for processes \'%d\' and \'%d\'.' %
                             (self.getProcId(), other.getProcId()))
        if 'tvalues' not in self._children:
            self._children['tvalues'] = [self._children['value'] * float(self._children['generated'])]
        self._mcache['generated'] = 0
        self._mcache['generated'] += other._children['generated']
        self._mcache['tvalue'] = 0.0
        self._mcache['tvalue'] += other._children['value'] * float(self._mcache['generated'])
        return self
    
    def _mergeFlush(self, parent):
        if not all([pn in self._mcache for pn in ('generated', 'tvalue')]):
            return
        self._children['generated'] += self._mcache['generated']
        self._children['tvalues'].append(self._mcache['tvalue'])
        self._dumpCache(parent)
    
    def toXmlElement(self, xElem):
        super(GenXSection, self).toXmlElement(xElem)
        self._xElem.setAttribute('id', str(self.getProcId()))
        for ten in GenXSection.xChildNames[0:2]:
            tn = self.hasChildElement(ten, True)[0]
            ttn = self._xElem.ownerDocument.createTextNode(str(self.getChild(ten)))
            tn.appendChild(ttn)
        tn = self.hasChildElement('value', True)[0]
        ttn = self._xElem.ownerDocument.createTextNode('%.5g' % (self.getChild('value')))
        tn.appendChild(ttn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum.
        """
        ret = dict([('type', self.xElementName), ('id', self._id), ('description', self.getChild('description')),
                    ('processId', self._procid), ('value', self._children['value']),
                    ('generated', self._children['generated'])])
        if 'tvalues' in self._children:
            ret['tvalues'] = list(self._children['tvalues'])
        return ret
    
    def fromDict(self, jsDict):
        """ Populates current instance with data provided in jsDict loaded from JSON.
        """
        self._id = jsDict['id']
        self._procid = jsDict['processId']
        self._children['value'] = jsDict['value']
        self._children['generated'] = jsDict['generated']
        self._children['description'] = jsDict['description']
        if 'tvalues' in jsDict:
            self._children['tvalues'] = list(jsDict['tvalues'])
            self.recompute({})
    
    def recompute(self, coll):
        if 'tvalues' in self._children:
            sumxs = sum(self._children['tvalues'])
            mxs = -1.
            # for back-tracing tvalues are stored as
            # xs_i * nGenerated_i so when recomputing
            # one needs to divide by total nGenerated
            xngen = self._children['generated']
            xsval = self._children['value']
            if xngen > 0 and xsval > 1.e-20:
                if sumxs/xsval/float(xngen) > 0.97:
                    mxs = sumxs / float(xngen)
            # otherwise just take mean
            if mxs < 0.:
                # verify file produced by very old version
                nbv = len(self._children['tvalues'])
                vset = list(set(self._children['tvalues']))
                if float(len(vset))/float(nbv) > 0.1:
                    mlog.error('Weird temporary values for xsection %s detected!' % self.getId())
                    mxs = self._children['value']
                else:
                    # do not count zeroes when computing arithmetic mean - buggy!?
                    nvalid = len(set([xv for xv in self._children['tvalues'] if xv > 1.e-20]))
                    if nvalid > 0:
                        mxs = sumxs / float(nvalid)
                    else:
                        mxs = 0.
            self._children['value'] = mxs
            self._value = mxs
            del self._children['tvalues']
        else:  # case for only one file!
            self._value = self._children['value']
    
    def __str__(self):
        ret = self.toDict()
        del ret['type']
        ret['value [mb]'] = ret['value']
        del ret['value']
        tt = '%s%s' % (self.__class__.__name__, str(ret))
        return tt


class GenEfficiency(GenXmlNode):
    """
    XML-Python interface for <efficiency> nodes. The Id in the final dictionary is
    given by reverse mapping of the mandatory name attribute from idmap dictionary.
    'value' and 'error' are computed on !final! summed up values.
    """
    idmap = {'E0': "full event cut",
             'E1': "generator level cut",
             'E2': "generator particle level cut",
             'E3': "generator anti-particle level cut"
             }
    cntNameMap = {'E0': ('N19', 'N20'), 'E1': ('I1', 'I2'), 'E2': ('S1', 'S2'), 'E3': ('S3', 'S4')}
    xElementName = 'efficiency'
    xAttribNames = ['name', ]
    xMandatoryAttribNames = ['name', ]
    xChildNames = ['before', 'after', 'value', 'error']
    
    def __init__(self, xelem=None):
        super(GenEfficiency, self).__init__()
        if xelem is not None and isinstance(xelem, xdom.Element):
            self.fromXmlElement(xelem)
    
    def getCounters(self):
        return [(v, GenXmlNode._getInt(self._children[k])) for (k, v) in
                zip(GenEfficiency.xChildNames[0:2], GenEfficiency.cntNameMap[self.getId()])]
    
    def genericId(self, **params):
        """
        See GenXmlNode method for help.
        """
        if len(params) == 0 or 'name' not in params.keys():
            return self.getId()
        tid = ''
        xname = params['name']
        for (tid, nn) in GenEfficiency.idmap.items():
            if xname.lower() == nn.lower():
                break
        if len(tid) == 0:
            return self.getId()
        return tid
    
    def __convertProperties(self):
        corrupted = False
        nn = self.getAttribute('name')
        self._id = self.genericId(name=nn)
        rt = GenXmlNode._getInt(self.getChild('before'))
        if rt is None:
            rt = 0
            corrupted = True
        self._children['before'] = rt
        rt = GenXmlNode._getInt(self.getChild('after'))
        if rt is None:
            rt = 0
            corrupted = True
        self._children['after'] = rt
        rt = self._getFloat(self.getChild('value'))
        self._children['value'] = rt
        if rt is None:
            rt = 0.0
        rt = self._getFloat(self.getChild('error'))
        if rt is None:
            rt = 0.0
        self._children['error'] = rt
        return corrupted
    
    def fromXmlElement(self, xElem):
        super(GenEfficiency, self).fromXmlElement(xElem)
        return self.__convertProperties()
    
    def __add__(self, other):
        if other is None:  # allow efficiencies to miss across production
            return self
        if not isinstance(other, GenEfficiency):
            raise TypeError('You cannot add GenEfficiency instances to %s.' % getattr(other, '__class__', 'Unknown'))
        if self.getId() != other.getId():
            raise ValueError(
                'Makes no sense to add different efficiencies \'%s\' and \'%s\'.' % (self.getId(), other.getId()))
        if self.getAttribute('name') != other.getAttribute('name'):
            raise ValueError('Name attributes for GenEfficiency objects may not differ.')
        self._mcache['before'] = 0
        self._mcache['before'] += other._children['before']
        self._mcache['after'] = 0
        self._mcache['after'] += other._children['after']
        return self
    
    def _mergeFlush(self, parent):
        if not all([pn in self._mcache for pn in ('before', 'after')]):
            return
        self._children['before'] += self._mcache['before']
        self._children['after'] += self._mcache['after']
        self._children['value'] = -1.0
        self._children['error'] = -1.0
        self._dumpCache(parent)
        
    def recompute(self, coll):
        if self._value == -1.0 and self._children['value'] == self._children['error']:
            i1 = self.getChild('before')
            i2 = self.getChild('after')
            if i1 == 0:
                self._value = 0.0
                self._children['error'] = 0.0
            else:
                self._value = float(i2) / float(i1)
                self._children['error'] = math.sqrt(i2 * (i1 - i2) / i1 / i1 / i1)
            self._children['value'] = self._value
    
    def toXmlElement(self, xElem):
        super(GenEfficiency, self).toXmlElement(xElem)
        self._xElem.setAttribute('name', GenEfficiency.idmap[self._id])
        for ten in GenEfficiency.xChildNames[0:2]:
            tn = self.hasChildElement(ten, True)[0]
            ttn = self._xElem.ownerDocument.createTextNode(str(self.getChild(ten)))
            tn.appendChild(ttn)
        for ten in GenEfficiency.xChildNames[2:]:
            tn = self.hasChildElement(ten, True)[0]
            ttn = self._xElem.ownerDocument.createTextNode('%.5g' % (self.getChild(ten)))
            tn.appendChild(ttn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum."""
        return dict([('type', self.xElementName), ('id', self.genericId(name=self.getAttribute('name'))),
                     ('before', self.getChild('before')), ('after', self.getChild('after'))])
    
    def fromDict(self, jsDict):
        """ Populates current instance with data provided in jsDict loaded from JSON.
        """
        self._id = jsDict['id']
        self._attribs['name'] = GenEfficiency.idmap[self._id]
        self._children['after'] = jsDict['after']
        self._children['before'] = jsDict['before']
        self._children['value'] = -1.0
        self._children['error'] = -1.0
    
    def __str__(self):
        ret = self.toDict()
        del ret['type']
        tt = '%s%s' % (self.__class__.__name__, str(ret))
        return tt


class GenFraction(GenXmlNode):
    """
    XML-Python interface for <fraction> nodes. The Id in the final dictionary is
    given by reverse mapping of the mandatory name attribute from dictionary selected
    from 'groups' using the idmap() method return. 'value' and 'error' are recomputed
    on final summed up 'number' values. See counter documentation for details.
    (!) list of signals ('sig' group) needs to be updated on case-by-case basis to match production signal particle name
    """
    groups = {'b':   {'gp_': (('B0', 'B+', 'Bs0', 'b-Baryon'), (14, 2), (3, 2)),
                      'gp~': (('anti-B0', 'B-', 'anti-Bs0', 'anti-b-Baryon'), (15, 2), (4, 2)),
                      'g1x': (('Bc+', 'Bc-'), (22, 1), (11, 1)),
                      'gp*': (('B(L=0,J=0)', 'B* (L=0, J=1)', 'B** (L=1, J=0,1,2)'), (46, 1), (43, 1))},
              'c':   {'gp_': (('D0', 'D+', 'Ds+', 'c-Baryon'), (34, 2), (25, 2)),
                      'gp~': (('anti-D0', 'D-', 'Ds-', 'anti-c-Baryon'), (35, 2), (26, 2)),
                      'gp*': (('D(L=0,J=0)', 'D* (L=0, J=1)', 'D** (L=1, J=0,1,2)'), (52, 1), (49, 1))},
              'sig': {'gp_': (('', 'D*(2010)+', 'D_s+', 'B_c-', 'tau-'), (), (90, 0)),
                      'gp~': (('~', 'D*(2010)-', 'D_s-', 'B_c+', 'tau+'), (), (99, 0))}
              }
    xElementName = 'fraction'
    xAttribNames = ['name', ]
    xMandatoryAttribNames = ['name', ]
    xChildNames = ['number', 'value', 'error']
    
    def __init__(self, xelem=None):
        super(GenFraction, self).__init__()
        self._mcache = {}
        self._value = -1.0  # use recompute to get final value in this field.
        if xelem is not None and isinstance(xelem, xdom.Element):
            self.fromXmlElement(xelem)
    
    def getCounters(self):
        return [(self.getId(), GenXmlNode._getInt(self._children['number'])), ]
    
    def isAntiParticle(self):
        return (self._pgroup == 'gp~') or (self._part.find('anti-') > -1) or (self._part.find('~') > -1)
    
    @staticmethod
    def __idmap(fType, qgrp, pgrp, idx, pname='C_unknown'):
        cName = pname
        tdidx = (fType == 'accepted' and 1) or (fType in ('generated', 'signal') and 2)
        try:
            idx0 = GenFraction.groups[qgrp][pgrp][tdidx][0]
            mul = GenFraction.groups[qgrp][pgrp][tdidx][1]
            cName = 'I%d' % (idx0 + idx * mul)
        except IndexError as err:
            mlog.error("Failed to determine GenFraction unique type.\n" + str(err))
        return cName
    
    def idmap(self, name):
        if isinstance(name, bytes):
            name = str(name)
        tokens = name.split(' ')
        if len(tokens) <= 1:
            raise ValueError('Could not find GenFraction id as name might be invalid.')
        _type = tokens[0].strip()
        _part = tokens[1].strip()
        _pgroup = 'zzz'
        _apart = 'unknown'      # will remain default for neutrals especially
        _mq = 'q'
        cl = 'I%d'
        cn = -1
        q = None
        if len(tokens) > 2:
            suff = ' '.join(tokens[2:])
            if suff != 'in sample':
                _part += (' ' + suff)
        if _type != 'signal':
            for (q, di) in GenFraction.groups.items():
                for (igp, plist) in di.items():
                    if _part in plist[0]:
                        cn = plist[0].index(_part)
                        _pgroup = igp
                        break
                if cn > -1:
                    _mq = q
                    break
        else:  # signal fraction
            _mq = 'sig'
            _pgroup = 'gp_'     # use as default
            tpgrp = None
            # search all definitions to determine if particle or anti-particle
            for (q, di) in GenFraction.groups.items():
                for igp in ('gp_', 'gp~'):
                    if igp not in di:
                        continue
                    plist = di[igp][0]
                    if q == _mq:
                        plist = plist[1:]
                    if _part in plist:
                        tpgrp = igp
                        break
                if tpgrp is not None:
                    break
            if tpgrp is None and _part.find('~') > -1:
                _pgroup = 'gp~'
            if tpgrp is not None and tpgrp != _pgroup:
                _pgroup = tpgrp     # overwrite w/ detected particle type
            # if tpgrp is None:     # triggered too often for neutrals
            #     mlog.error('Could not determine a grouping for particle %s...' % _part)
            cn = 0
        if _part in GenFraction.groups[q][_pgroup][0]:
            p = GenFraction.groups[q][_pgroup][0].index(_part)
            agrp = 'gp_'
            if _pgroup == 'gp_':
                agrp = 'gp~'
            _apart = GenFraction.groups[q][agrp][0][p]
        cl = self.__idmap(_type, _mq, _pgroup, cn, _part)
        return cl, _type, _part, _apart, _mq, _pgroup, cn
    
    def genericId(self, **params):
        """
        See GenXmlNode method for help. Duplicates GenFraction::idmap().
        """
        if len(params) == 0 or 'name' not in params.keys():
            return self.getId()
        xname = params['name']
        lid = self.idmap(xname)[0]
        if len(lid) == 0:
            return self.getId()
        return lid
    
    def __convertProperties(self):
        corrupted = False
        nn = self.getAttribute('name')
        (self._id, self._type, self._part, self._apart, self._mq, self._pgroup, iii) = self.idmap(nn)
        if self._pgroup == 'zzz' and iii == -1:
            mlog.debug('Fraction formula for %s %s %s was is not defined. Please, update script and Twiki. \
                        Ignoring fraction...' % (self._type, self._part, str(self.getGroupInfo())))
            return True
        rt = GenXmlNode._getInt(self.getChild('number'))
        if rt is None:
            rt = 0
            return True  # no need to further fill object as it should be discarded
        self._children['number'] = rt
        rt = GenXmlNode._getFloat(self.getChild('value'))
        if rt is None:
            rt = 0.0
        self._children['value'] = rt
        rt = GenXmlNode._getFloat(self.getChild('error'))
        if rt is None:
            rt = 0.0
        self._children['error'] = rt
        return corrupted
    
    def fromXmlElement(self, xElem):
        super(GenFraction, self).fromXmlElement(xElem)
        return self.__convertProperties()
    
    def __add__(self, other):
        if other is None:  # allow missing fractions across production
            return self
        if not isinstance(other, GenFraction):
            raise TypeError('You cannot add GenFraction instances to %s.' % getattr(other, '__class__', 'Uknown'))
        if self.getId() != other.getId():
            raise ValueError(
                'Makes no sense to add different fractions with id \'%s\' and \'%s\'.' % (self.getId(), other.getId()))
        tname = self.getAttribute('name')
        oname = other.getAttribute('name')
        if tname != oname:
            raise ValueError('Name attributes for GenFraction objects may not differ (%s vs. %s).' % (tname, oname))
        self._mcache['number'] = 0.0
        self._mcache['number'] += other._children['number']
        return self
    
    def _mergeFlush(self, parent):
        if 'number' not in self._mcache:
            return
        self._children['number'] += self._mcache['number']
        self._children['value'] = -1.0
        self._children['error'] = -1.0
        self.value = -1.0
        self._dumpCache(parent)
    
    def recompute(self, coll):
        if self._value > -1.0:
            return
        isum = self.getCounters()[0][1]
        (myqc, mygrp, myii) = self.getGroupInfo()
        if myii > -1:
            for ii in range(len(GenFraction.groups[myqc][mygrp][0])):
                if ii == myii:
                    continue
                oid = self.__idmap(self.getFracType(), myqc, mygrp, ii)
                if oid not in coll:
                    mlog.error(str(coll))
                    raise ValueError('Cannot compute fraction \'%s\' since counter \'%s\' is missing.' %
                                     (self.getAttribute('name'), oid))
                ps = coll[oid].getCounters()[0][1]
                if ps < 0:
                    raise ValueError('Invalid counter value for counter \'%s\'.' % oid)
                isum += ps
        else:  # for signal counters - alternatively, search for GenFraction for anti-particle name ?!
            # pn = self.getPartName()
            # if self.isAntiParticle():  # change rule from particle name to anti-particle name is not clear
            #     pn = pn.replace('~', '')
            # isum = 0.0
            isum = sum([ox.getCounters()[0][1] for ox in coll.getCountersByType('fraction')
                        if ox.getId() in ('I90', 'I99')])
            # for fid in coll.getCounterIdsByType('fraction'):
            #     pfid = fid.replace('~', '')
            #     if pfid.find(pn) > -1:
            #         isum += coll[fid].getCounters()[0][1]
        cval = float(self.getCounters()[0][1])
        if int(isum) == 0:
            err = 0.0
            isum = 1
        else:
            if cval > isum:
                mlog.error(str(self))
            err = math.sqrt(cval * (isum - cval) / isum / isum / isum)
        self._value = cval / float(isum)
        self._children['error'] = err
        self._children['value'] = self._value
    
    def getPartName(self):
        return self._part
    
    def getAntiPartName(self):
        return self._apart
    
    def getFracType(self):
        return self._type
    
    def getGroupInfo(self):
        ii = -1
        if self._mq != 'sig':
            if self._mq in GenFraction.groups and self._pgroup in GenFraction.groups[self._mq]:
                grp = GenFraction.groups[self._mq][self._pgroup][0]
                if self._part in grp:
                    ii = grp.index(self._part)
        return self._mq, self._pgroup, ii
    
    def getNameFromTokens(self, otype=None, part=None):
        ret = None
        if otype is None:
            otype = self._type
        if part is None:
            part = self._part
        if otype == 'signal':
            ret = '%s %s in sample' % (otype, part)
        else:
            ret = '%s %s' % (otype, part)
        return ret
    
    def toXmlElement(self, xElem):
        super(GenFraction, self).toXmlElement(xElem)
        self._xElem.setAttribute('name', self.getNameFromTokens())
        tn = self.hasChildElement('number', True)[0]
        ttn = self._xElem.ownerDocument.createTextNode(str(self.getChild('number')))
        tn.appendChild(ttn)
        for ten in GenFraction.xChildNames[1:]:
            tn = self.hasChildElement(ten, True)[0]
            ttn = self._xElem.ownerDocument.createTextNode('%.5g' % (self.getChild(ten)))
            tn.appendChild(ttn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum."""
        ret = {'name': self.getAttribute('name'), 'type': self.xElementName, 'number': self.getChild('number')}
        ret['id'] = self.genericId(name=ret['name'])
        return ret
    
    def fromDict(self, jsDict):
        """ Populates current instance with data provided in jsDict loaded from JSON.
        """
        self._attribs['name'] = jsDict['name']
        (cn, self._type, self._part, self._apart, self._mq, self._pgroup, iii) = self.idmap(jsDict['name'])
        self._id = jsDict['id']
        self._value = -1.0
        self._children['number'] = jsDict['number']
        self._children['value'] = -1.0
        self._children['error'] = -1.0
    
    def __str__(self):
        ret = self.toDict()
        del ret['type']
        srcCnt = 'grpInfo: %s' % (str(self.getGroupInfo()))
        tt = '%s(%s;%s)' % (self.__class__.__name__, str(ret), srcCnt)
        return tt


class GenInstance(object):
    """
    Providing a hard link between MC Generator instance and corresponding counters in a log file
    in order to match order of generator instances writing to the XML file
    """
    
    def __init__(self, genstat, genName, toolName, evtType=None, njobs=1):
        """
        Initializing objects of this type.
        Args:
            genstat (GenStat): parent object
            genName (str): generator name as read from input
            toolName (str): generator tool name as read from input
            evtType (str): event type code
            njobs (int): number of job logs in input statistics
        """
        if not isinstance(genstat, GenStat):
            raise TypeError('First parameter must be a GenStat instance')
        self._parent = genstat
        self.generator = genName
        self.tool = toolName
        self.evtID = evtType
        self._nbjobs = njobs
        self._cnts = []
        
    def fastMC(self):
        return self.generator.lower() in ('redecay', 'pgun')
    
    def isGenSpecial(self):
        return self.generator.lower() in ('bcvegpy', 'genxicc') or '.special' in self.tool.lower()
    
    def nb_jobs(self):
        return self._nbjobs
    
    def isSpillover(self):
        return any([self.tool.lower().find(t) > -1 for t in ['prev.', 'next.']])
    
    def isSig(self):
        if self._parent.xmodel is None:
            return False
        ret = False
        rsig = self._parent.xmodel.getMethodByType('sig')
        if rsig is not None and self == rsig:
            return True
        return ret
        
    def defined(self):
        r = [isinstance(self._parent, GenStat), isinstance(self.generator, six.string_types)]
        r += [isinstance(self.tool, six.string_types), len(self.generator) > 0, len(self.tool) > 0]
        r += [self.evtID is not None and len(self.evtID) == 8]
        return all(r)
    
    def toDict(self):
        return {'generator': self.generator, 'generator_method': self.tool,
                'eventType': self.evtID, 'nb_merged': self._nbjobs}
    
    def fullName(self):
        return "{}.{}{}".format(self.generator, self.tool, self.evtID is not None and ".{}".format(self.evtID) or "")
    
    def __ne__(self, other):
        return not self == other
        
    def __eq__(self, other):
        thisid = "{}.{}{}".format(self.generator, self.tool,
                                  self.evtID is not None and ".{}".format(self.evtID) or "")
        otherid = "{}.{}{}".format(other.generator, other.tool,
                                   other.evtID is not None and ".{}".format(other.evtID) or "")
        return thisid == otherid
    
    def __findMatchingCounter(self, ocnt):
        if not isinstance(ocnt, tuple(GenStat.ChildrenMap.values())):
            raise TypeError('Unknown counter type object.')
        acns = [x[0] for x in ocnt.getCounters()]
        rr = None
        for xo in self.getCountersByType(ocnt.xElementName):
            if all([ct[0] in acns for ct in xo.getCounters()]):
                rr = xo
                break
        return rr
    
    def addCounter(self, ocnt):
        self._cnts.append(ocnt)
    
    def __add__(self, other):
        olog = self._parent.logger
        if self != other:
            raise TypeError('Cannot perform addition between GenInstances %s v. %s' %
                            (self.fullName(), other.fullName()))
        for oc in other.getCounters():
            tco = self.__findMatchingCounter(oc)
            try:
                if tco is None:
                    self._cnts.append(oc)
                else:
                    # skip total xsection to force re-computing it later
                    if self._parent.xmodel.SpilloverCount() > 0 and self.isSig() and \
                            oc.xElementName == "crosssection" and oc.getId() == 'pp0':
                        olog.debug('Ignoring total cross-section. Will recompute it later...')
                        continue
                    tco += oc
            except (ValueError, TypeError, IOError) as err:
                msg = "Failed to merge {} counters '{}' in generator method {}.".format(
                    oc.xElementName, ', '.join([x[0] for x in tco.getCounters()]), self.fullName())
                if not self.isSpillover() and not self.fastMC():
                    olog.error(msg)
                else:
                    olog.warning(msg)
                self._brDumpCache()
                raise err
        self._brMergeCache()
        self._nbjobs += 1
        return self

    # noinspection PyProtectedMember
    def _brDumpCache(self):
        for oc in self._cnts:
            oc._dumpCache(self)

    # noinspection PyProtectedMember
    def _brMergeCache(self):
        for oc in self._cnts:
            oc._mergeFlush(self)

    def getCounters(self):
        return tuple(self._cnts)
    
    def getCountersByType(self, ctype):
        """
        Args:
            ctype (str): Name of counter type to search for

        Returns:
            List of counters of specified type in counter collection
        """
        if ctype.lower() not in GenStat.ChildrenMap:
            return None
        ret = []
        tclass = GenStat.ChildrenMap[ctype.lower()]
        for cnt in self._cnts:
            # noinspection PyTypeHints
            if isinstance(cnt, tclass):
                ret.append(cnt)
        return ret

    def getCounterIdsByType(self, statName, dosort=False):
        """
        Args:
            statName (str): object type for which IDs are needed
            dosort (bool): specify to sort list or returned IDs
        Returns:
            List of ids for specified object type in GenStats object.
        """
        if statName not in GenStat.ChildrenMap:
            raise ValueError('No statistics object of type \'%s\' is known to this class.' % statName)
        ret = [sobj.getId() for sobj in self.getCountersByType(statName)]
        if dosort:
            ret.sort()
        return ret

    def __locate(self, key, getobj=False):
        ret = None
        for oc in self._cnts:
            oid = oc.getId()
            if key == oid:
                ret = oc
                break
        if not getobj:
            ret = ret is not None
        return ret

    def __getitem__(self, oid):
        return self.__locate(oid, getobj=True)

    def __contains__(self, key):
        return self.__locate(key)


def getFileContents(fPath, rundry=False, target=None):
    """
    Retrieves a file from filesystem and automatically unpacks if necessary
    Args:
        fPath (string):
        rundry (bool):
        target (string): May be a string or re.Pattern to match archive catalog items in tarball

    Returns:
        Return None if error, True if dryrun or utf-8 encoded contents is successful
    """
    if fPath.startswith('file://'):
        fPath = fPath[7:]
    if not os.path.isfile(fPath):
        return None
    if rundry:
        return True
    with (open(fPath, 'rb')) as fp:
        raw = fp.read()
        fp.close()
    if fPath.endswith('.gz'):
        zbts = zipBytes(raw)
        zfp = gzip.GzipFile(fileobj=zbts)
        ret = zfp.read()
        zfp.close()
        zbts.close()
        raw = ret
    if fPath.endswith('.tar.gz') or fPath.endswith('.tgz'):
        zfp = tarfile.open(fPath, mode='r:gz')
        arhNames = zfp.getnames()
        target_name = None
        if target is None and len(arhNames) == 1:
            target_name = arhNames[0]
        if isinstance(target, six.string_types):
            if target not in arhNames:
                zfp.close()
                return None
            target_name = target
        if isinstance(target, re.Pattern):
            for xn in arhNames:
                # pick first catalog entry matching compiled RE
                if target.match(xn):
                    target_name = xn
                    break
        if target_name is None:
            zfp.close()
            return None
        fp = zfp.extractfile(target_name)
        raw = fp.read()
        fp.close()
        zfp.close()
    ret = raw.decode('utf-8', errors='ignore')
    return ret


def getURLContents(fUrl, rundry=False, target=None):
    if not fUrl.startswith('http://') and not fUrl.startswith('https://'):
        return None
    if rundry:
        return True
    headers = {'user-agent': appUA, 'accept-encoding': 'gzip, deflate'}
    rr = requests.get(fUrl, headers=headers, stream=True)
    raw = b''
    for chunk in rr.iter_content(chunk_size=128):
        raw += chunk
    ret = raw.decode('utf-8')
    return ret


class GenXMLModel(object):
    """
    Class to implement and heuristically determine best model for XMLs spit out from production jobs.
    Controls mapping between event generator details and associated counters.
    """
    def __init__(self, gmeths):
        if not isinstance(gmeths, (tuple, list)) or len(gmeths) < 1 or not isinstance(gmeths[0], GenInstance):
            raise TypeError('Please, provide a non-empty tuple of GenInstance objects as argument')
        self.methods = gmeths
        self._matchFilters = {}
        self._bootstrap()
        
    def SpilloverCount(self):
        return self._spillNb
    
    def _bootstrap(self):
        slevel = 0
        self._matchFilters['type'] = 'byindex'
        self._sigs = []
        self._spills = []
        self._spillNb = 0
        self.fastDecay = False
        if len(self.methods) == 1:
            self._sigs.append(self.methods[0])
            self._matchFilters['ctmap'] = {0: 'sig'}
            self._matchFilters['xsmap'] = self._matchFilters['ctmap']
            return
        mths = [lmn.tool.lower() for lmn in self.methods]
        self.fastDecay = any([not x.isSpillover() and x.fastMC() for x in self.methods])
        if any([x.find('next.') > -1 for x in mths]) or any([x.find('prev.') > -1 for x in mths]):
            slevel += 1
        if any([x.find('nextnext.') > -1 for x in mths]) or any([x.find('prevprev.') > -1 for x in mths]):
            slevel += 1
        if slevel == 0 and self.fastDecay:
            self._sigs = self.methods
            self._matchFilters['ctmap'] = {0: 'sig', 1: 'fsig'}
            self._matchFilters['xsmap'] = self._matchFilters['ctmap']
            return
        for j in range(len(self.methods)):
            mth = self.methods[j]
            if not mth.isSpillover():
                self._sigs.append(mth)
            if mths[j].find('prev.') > -1 or mths[j].find('next.') > -1:
                self._spills.append(mth)
        # for method list order: [fsig] nn n p pp sig -- 0 1 2 3 4 5
        # assume map for counters: sig pp p nn n [fsig]
        # assume map for x-sections: n nn p pp [fsig/may be missing!] sig
        if slevel == 1:
            if self.fastDecay:
                self._matchFilters['ctmap'] = {0: 'sig', 1: 'next', 2: 'prev', 3: 'fsig'}
                self._matchFilters['xsmap'] = {0: 'prev', 1: 'next', 2: 'fsig', 3: 'sig'}
            else:
                self._matchFilters['ctmap'] = {0: 'sig', 1: 'next', 2: 'prev'}
                self._matchFilters['xsmap'] = {0: 'prev', 1: 'next', 2: 'sig'}
        if slevel == 2:
            if self.fastDecay:
                self._matchFilters['ctmap'] = {0: 'sig', 1: 'nextnext', 2: 'next', 3: 'prevprev', 4: 'prev', 5: 'fsig'}
                self._matchFilters['xsmap'] = {0: 'prev', 1: 'prevprev', 2: 'next', 3: 'nextnext', 4: 'fsig', 5: 'sig'}
            else:
                self._matchFilters['ctmap'] = {0: 'sig', 1: 'nextnext', 2: 'next', 3: 'prevprev', 4: 'prev'}
                self._matchFilters['xsmap'] = {0: 'prev', 1: 'prevprev', 2: 'next', 3: 'nextnext', 4: 'sig'}
        self._miss = False
        if len(self._spills) + len(self._sigs) != self.fastDecay and 2 or 1 + 2*slevel:
            self._miss = True
        self._spillNb = slevel
    
    def getMethodByType(self, mtype=None):
        """
        Get method of specified type.
        Args:
            mtype (string): predefined string to select specific generator instance:
            'sig' - signal; 'fsig' - fast gen for signal; 'prev/next' - first spillover
            events; 'prevprev/nextnext' - second order spillover events

        Returns:
            GenInstance object for specified type or None if is no instance matches for current GenStat model.
        """
        method_fids = ('sig', 'fsig', 'prev', 'next', 'prevprev', 'nextnext')
        if mtype is None or not mtype.lower() in method_fids:
            return None
        r = None
        mfilt = mtype.lower()
        if mfilt in method_fids[2:]:
            for o in self._spills:
                mtname = o.tool.lower()
                if mfilt == 'prev' and mtname.find('prev.') > -1 and mtname.find('prevprev.') == -1:
                    r = o
                    break
                if mfilt == 'prevprev' and mtname.find('prevprev.') > -1:
                    r = o
                    break
                if mfilt == 'next' and mtname.find('next.') > -1 and mtname.find('nextnext.') == -1:
                    r = o
                    break
                if mfilt == 'nextnext' and mtname.find('nextnext.') > -1:
                    r = o
                    break
        else:
            for o in self._sigs:
                if mfilt == 'sig' and not o.fastMC():
                    r = o
                    break
                if mfilt == 'fsig' and o.fastMC():
                    r = o
                    break
        return r
    
    def getMethodForIndex(self, ii, tmap='counter'):
        mmap = None
        if tmap == 'counter':
            mmap = self._matchFilters['ctmap']
        if tmap == 'xsection':
            mmap = self._matchFilters['xsmap']
        if mmap is None or ii not in mmap:
            return None
        return self.getMethodByType(mmap[ii])

    def getMethodTypes(self):
        method_fids = ['sig', ]
        sc = self.SpilloverCount()
        if sc > 0:
            method_fids += ['prev', 'next']
        if sc > 1:
            method_fids += ['prevprev', 'nextnext']
        if self.fastDecay:
            method_fids.append('fsig')
        return method_fids


class fmtVersion(object):
    """
    Internal class to implement smart versioning in format a.b.c where a,b,c can be integers or strings.
    """

    def __init__(self, verstring):
        if isinstance(verstring, fmtVersion):
            self.raw = verstring.raw
            self.ver = tuple(verstring.ver)
            self.version = verstring.version
        else:
            if not isinstance(verstring, six.string_types):
                verstring = str(verstring)
            verstring.strip(',. \n\t\r')
            self.raw = tuple(verstring.split('.'))
            tver = []
            for ts in self.raw:
                if ts.isdigit():
                    sts = ts.lstrip('0')
                    if len(sts) == 0:
                        sts = '0'
                    tver.append(int(sts))
                else:
                    tver.append(ts)
            self.ver = tuple(tver)
            self.version = verstring

    def duplicate(self):
        return fmtVersion(self)

    def __prepare_arg__(self, other):
        if isinstance(other, six.string_types):
            return fmtVersion(other)
        if not isinstance(other, fmtVersion):
            raise NotImplementedError('Cannot compare with this version encoding...')
        return other

    def __eq__(self, other):
        other = self.__prepare_arg__(other)
        if len(self.raw) != len(other.raw):
            vobj = None
            nm = max(len(self.raw), len(other.raw))
            mm = min(len(self.raw), len(other.raw))
            if nm > len(self.raw):
                if self.raw != other.raw[0:len(self.raw)]:
                    return False
                vobj = other
            if nm > len(other.raw):
                if other.raw != self.raw[0:len(other.raw)]:
                    return False
                vobj = self
            ret = True
            for i in range(mm, nm):
                tt = vobj.raw[i]
                if not tt.startswith('0') or len(tt) > 1:
                    ret = False
                    break
            return ret
        else:
            return self.raw == other.raw

    def __ne__(self, other):
        return not self.__eq__(other)

    def __ge__(self, other):
        return self > other or self == other

    def __le__(self, other):
        raise self < other or self == other

    def __gt__(self, other):
        other = self.__prepare_arg__(other)
        if len(self.raw) != len(other.raw):
            nm = max(len(self.raw), len(other.raw))
            mm = min(len(self.raw), len(other.raw))
            vobj = None
            if nm > len(self.raw):
                if self.raw != other.raw[0:len(self.raw)]:
                    return self.raw > other.raw[0:len(self.raw)]
                vobj = other
            if nm > len(other.raw):
                if other.raw != self.raw[0:len(other.raw)]:
                    return self.raw[0:len(other.raw)] > other.raw
                vobj = self
            ret = False
            for i in range(mm, nm):
                tt = vobj.raw[i]
                if not tt.startswith('0') or len(tt) > 1:
                    ret = True
                    break
            return ret
        else:
            return self.raw > other.raw

    def __lt__(self, other):
        return not self.__gt__(other)


class simpleXmlStatParser(object):
    """
    Implement a simple XML parser for GeneratorLog.xml files
    """
    logBaseName = "GeneratorLog"
    rawContentDownloadMap = {'filesystem': getFileContents, 'http': getURLContents}

    # to correct malformed XML on-the-fly
    correctiveREs = {
        r'[\x00-\x1f\x7f-\xff]': '',  # keep this filter until utf-8 is properly supported if ever
        r'crosssection\s+id = (\d+)\s*>': r'crosssection id="\1">',
        # may be removed when all available logs are properly formed!
        r'<"': '"',
        r'<([^>]+)<': r'\1<',
        # introduced to make XML parsing stabler
        
    }

    def __init__(self, filepath, method="filesystem", logger=mlog):
        """
        Initialize a new parser of counters store in XML file.
        Args:
            filepath (string): file location on local storage or remote access URL
            method (string): specifies whether :arg filepath is to be accessed on filesystem or at http URL
            logger (logging.Logger): log object to print-out status messages
        """
        self.__flocation = None
        self.__ftype = None
        self._slog = logger
        self.xdoc = None
        if not isinstance(filepath, six.string_types):
            raise TypeError('First argument must be a string indicating file location.')
        if method not in simpleXmlStatParser.rawContentDownloadMap.keys():
            raise ValueError('Cannot handle specified protocol for accesing file.')
        if simpleXmlStatParser.rawContentDownloadMap[method](filepath, rundry=True):
            self.__flocation = filepath
            self.__ftype = method
        else:
            raise ValueError('Provided dile location is not valid for specified protocol.')

    def _downloadContentParse(self):
        """
        Download XML file content and try to parse the XML structure or apply the defined runtime corrections.
        Returns:
            Returns True if error occurs else it sets the internal _xdoc property to reference the parsed XML DOM.
        """
        try:
            sxml = simpleXmlStatParser.rawContentDownloadMap[self.__ftype](self.__flocation,
                                                                           target=self.logBaseName + '.xml')
        except (IOError, EOFError, zlib.error, tarfile.ReadError, gzip.BadGzipFile) as err:
            self._slog.error(str(err), exc_info=True)
            return True
        sxFormedContent = None
        if sxml is None or len(sxml) == 0:
            self._slog.error('Could not read data from \'%s\'.' % self.__flocation)
            return True
        try:
            xdoc = xdom.parseString(sxml)
        except ExpatError:  # try to recover by correcting the XML on the fly!
            self._slog.debug('Failed parsing (%s).\nAttempting to recover XML structure on the fly.' % self.__flocation)
            sxFormedContent = sxml
            for (reExpr, reRepl) in simpleXmlStatParser.correctiveREs.items():
                sxFormedContent = re.sub(reExpr, reRepl, sxFormedContent)
            xdoc = xdom.parseString(sxFormedContent)
        if xdoc.documentElement is None or xdoc.documentElement.nodeName != GenStat.RootElementName:
            self._slog.error('XML invalid from location ' + self.__flocation)
            return True
        if not xdoc.documentElement.hasChildNodes():
            self._slog.error('XML at location %s seems truncated. Did Gauss crash?' % self.__flocation)
            return True
        self.xdoc = xdoc
        if sxFormedContent is not None:
            self._slog.info('Succesfully recovered parsing XML in \'%s\'' % self.__flocation)
        return False

    def retrieveDOM(self):
        if self._downloadContentParse():
            return None
        return self.xdoc


class GenStat(object):
    """
    XML-Python interface parsing/merging/saving GeneratorLog*.xml files. Parsing is perfomed by
    GenXmlNode derived classes and loosely controlled by GenXMLModel for given XML log files.
    Spillover model determines sets of counters for each generator instance which saved data
    to XML. Node name to class mapping is given by 'ChildrenMap' static property.
    """
    TimeStampFormat = '%a, %d %b %Y %H:%M:%S -0000'     # as email.utils.formatdate; RFC 2822
    RootElementName = 'generatorCounters'
    ChildrenMap = {'counter':      GenCounter,
                   'crosssection': GenXSection,
                   'efficiency':   GenEfficiency,
                   'fraction':     GenFraction,
                   # 'version':      GenMetaInfo,
                   # 'eventType':    GenMetaInfo,
                   # 'method':       GenMetaInfo,
                   # 'generator':    GenMetaInfo
                   }
    
    def __init__(self, filepath=None, slogger=mlog):
        """
            Initialize the GenStats object.
        Args:
            filepath (string): path to XML file to parse
            slogger  (logging.Logger): log object to print-out messages
        """
        self.logger = slogger
        self._src = None
        self._xdoc = None
        self._gens = list()
        self.xmodel = None
        self._fullpath = None
        self.isMerged = False
        self._xmlFiles = list()     # needed?!
        self._fmtver = fmtVersion('0.1')  # a generic value to default to most primitive parsing method
        if filepath is not None:
            self._fullpath = filepath
            self.loadXML(self._fullpath)
            self._xmlFiles.append(self._src)
    
    def getXMLFullPath(self):
        return self._fullpath
    
    def __str__(self):
        tt = ''
        if self._xdoc is None and self.getXMLFullPath() is None and len(self._gens) == 0:
            tt = 'uninitialized'
        if self._src is not None:
            tt = '(%s)' % self._src
        msgs = ['GenStats %s object:' % tt, ]
        if len(self._xmlFiles) == 0 and len(self._gens) == 0:
            msgs.append('!empty object!')
            return '\n'.join(msgs)
        msgs.append(' - %d generator counter sets\n' % (len(self._gens)))
        for om in self._gens:
            msgs.append('*** Generator instance %s:\n' % om.fullName())
            for kk in GenStat.ChildrenMap.keys():
                nn = len(om.getCountersByType(kk))
                msgs.append(' - %d %s object(s)' % (nn, kk))
        return '\n'.join(msgs)
    
    def xVersion(self):
        return self._fmtver
    
    def _addGenMethod(self, geninfo):
        """
        Creates a new entry in ._gens containing generator method metainformation
        Args:
            geninfo (GenInstance): meta information about current generator
        Returns:

        """
        if not isinstance(geninfo, GenInstance):
            raise TypeError('Argument must be a GenInstance object')
        self._gens.append(geninfo)
        
    def getGenMethod(self, method=None, mcgen=None, evt=None, getsig=False):
        """
        Return first GenInstance object which matches any of the provided filter values
        Args:
            method (string): matches if generator method (GenInstance.tool) contains string (case-insensitive)
            mcgen (string): matches all objects for which generator name contains value
            evt (string): matches all objects having event type ID equal to value
            getsig (bool): when True do the best to return only signal instance (highest priority!)
        Returns:
            Reference to one or more GenInstance objects. or empty list, and None if failure
        """
        ret = []
        if len(self._gens) == 0:
            return ret
        for gio in self._gens:
            if not gio.defined():
                ret = None
                break
            doAppend = getsig and not gio.isSpillover()
            if not doAppend:
                doAppend = method is not None and gio.tool.lower().find(method.lower()) > -1
            if not doAppend:
                doAppend = mcgen is not None and gio.generator.lower().find(mcgen.lower()) > -1
            if not doAppend:
                doAppend = evt is not None and gio.evtID == evt
            if doAppend:
                ret.append(gio)
        return ret
      
    def loadXML(self, filepath):
        """
        Perform common initial checks and call parsing code based on log format <version>.
        """
        oo = simpleXmlStatParser(filepath, logger=self.logger)
        self._xdoc = oo.retrieveDOM()
        if self._xdoc is None:
            return
        ve = self._xdoc.documentElement.getElementsByTagName('version')
        if len(ve) == 1:
            vo = GenMetaInfo(ve[0])
            # may need to be sent to XMLmodel (see parseXDoc!)
            self._fmtver = fmtVersion(vo.getValue())
        # XML format version selection here
        if self._fmtver > "1.0":
            if self._fmtver > "1.1":
                mlog.warning('XML logs version %s may not be fully supported.' % self._fmtver.version)
            self.parseXDoc(self._fmtver.version)
        else:
            self.logger.error('XML file version %s not supported by parser.' % self._fmtver.version)
            return
        self._src = os.path.split(filepath)[1]
    
    def parseXDoc(self, fmtVer='1.1'):
        # get list of generators and tools
        olog = self.logger
        mes = self._xdoc.documentElement.getElementsByTagName('method')
        fn = os.path.split(self._fullpath)[1]
        for i in range(len(mes)):
            me = mes[i]
            mo = GenMetaInfo(me)
            ne = mo.getNextSiblingNode()
            if ne is None or not ne.nodeName == u'generator':
                continue
            go = GenMetaInfo(ne)
            gio = GenInstance(self, go.getValue(), mo.getValue())
            self._addGenMethod(gio)
        self.xmodel = GenXMLModel(tuple(self._gens))
        xevts = self._xdoc.documentElement.getElementsByTagName('eventType')
        nsigs = self.xmodel.fastDecay and 2 or 1
        nmetds = nsigs+2*self.xmodel.SpilloverCount()
        if len(xevts) > nmetds:
            olog.error('Unsupported XML file version (%s). Too many event type nodes detected...' % fmtVer)
            raise ValueError('Too many event type nodes detected. Please, report production ID on JIRA.')
        for j in range(len(xevts)):  # parse event type nodes
            oo = GenMetaInfo(xevts[j])
            moo = self.xmodel.getMethodForIndex(j)
            if moo is None:
                raise ValueError('Could not locate generator method at #%d for event type %s' % (j, oo.getValue()))
            if moo.evtID is None:
                moo.evtID = oo.getValue()
            ce = oo.getNextSiblingNode()
            while ce is not None:
                nt = ce.nodeName
                if nt == 'eventType' or nt == 'method':
                    break
                ox = GenXmlNode(ce)
                if nt in ['counter', 'efficiency', 'fraction']:
                    oo = GenStat.ChildrenMap[ce.nodeName]()
                    if not oo.fromXmlElement(ce):
                        moo.addCounter(oo)
                    else:
                        olog.debug('Bad <%s> in %s:\n%s' % (nt, fn, ce.toprettyxml(indent='  ')))
                else:
                    mlog.warning('Unknown XML node <%s> in %s. Ignoring...' % (ce.nodeName, fn))
                ce = ox.getNextSiblingNode()
        xxs = self._xdoc.documentElement.getElementsByTagName('crosssection')
        ii = 0
        moo = self.xmodel.getMethodForIndex(ii, tmap='xsection')
        for j in range(len(xxs)):  # parse xsection nodes
            oo = GenXSection()
            corrupted = oo.fromXmlElement(xxs[j])
            if corrupted:
                olog.debug('Bad <crosssection> in %s:\n%s' % (fn, xxs[j].toprettyxml(indent='  ')))
                continue
            xsid = oo.getId()
            if xsid in moo:
                ii += 1
                moo = self.xmodel.getMethodForIndex(ii, tmap='xsection')
            moo.addCounter(oo)
        for jj in range(len(self._gens)):
            mth = self._gens[jj]
            oxsl = mth.getCountersByType(ctype='crosssection')
            if oxsl is not None and len(oxsl) > 0:
                continue
            mlog.debug('Group <crosssection> missing for %s.' % mth.fullName())
            # enable parsing hack for anything - pave way to drop spill-over statistics (sept 2022)
            if mth.isSig():
                # if self.xmodel.fastDecay and jj > 0:
                if jj > 0:
                    # this parsing hack makes spillover statistics quite unusable!
                    pmth = self.xmodel.getMethodForIndex(ii, tmap='xsection')   # GenInstance w/ last xsection group
                    olog.debug("In %s copy last <crosssection> group for %s to signal." % (fn, pmth.fullName()))
                    oxsl = pmth.getCountersByType(ctype='crosssection')
                    for xs in oxsl:
                        mth.addCounter(xs)
                else:
                    raise ValueError("No <crosssection> nodes could be parsed for signal. "
                                     "Corrupted XML log file %s." % fn)
        # special validation for other generators e.g. BcVegPy
        # hasRepeatedHadronization = any([x.tool.lower().find("repeatedhadronization") > -1 for x in self.xmodel.methods])
        msig = self.xmodel.getMethodByType('sig')
        genToolName = msig.tool.lower()
        if genToolName.find('signal') == -1 and not msig.isGenSpecial() and genToolName.find('repeatdecay') == -1:
            olog.warning('No \'signal\' in %s for signal in file %s...' % (msig.fullName(), fn))
        # temporarily (!?) skip validation checks for generators other than Pythia (i.e. not all counters for BcVegPy)
        if msig.generator.lower().find('pythia') == -1:
            return
        (ol1, ol2, on1, on2) = tuple([msig[nk] for nk in ['L1', 'L2', 'N1', 'N2']])
        if ol2 is None and msig.isGenSpecial():    # for special signal it may be possible
            ol2 = GenCounter(oid='L2', value=0)
        if any([ax is None for ax in (ol1, ol2, on1, on2)]):
            cmiss = [ck for ck in ['L1', 'L2', 'N1', 'N2'] if msig[ck] is None]
            # do not print-out warning for specific fixed pile-up productions which do not have empty events!
            if not (all([ax is None for ax in (ol1, ol2)]) and
                    on1 is not None and on2 is not None and on1.getValue() == on2.getValue()):
                olog.warning('Could not locate all counters for consistency check in file %s. Missing counters: %s' %
                             (fn, str(cmiss)))
        if all([ax is not None for ax in (ol1, ol2, on1,on2)]):
            (l1, l2, n1, n2) = (x.getValue() for x in (ol1, ol2, on1, on2))
            if int(l2) == 0:
                olog.warning('File %s: Number of events with no interactions is zero.' % fn)
            if int(l1) != int(l2 + n1):
                olog.warning('File %s: All event count != empty + non-empty events.' % fn)
        xsl = msig.getCountersByType(ctype='crosssection')
        n2 = on2 is None and 0 or on2.getValue()
        isum = 0
        clxsl = []
        for xs in xsl[::-1]:
            if isum == n2:
                olog.debug('Matched number of interactions w/ xsections at index #%d. Truncating...' % xsl.index(xs))
                break
            if xs.getProcId() == 0:
                continue
            xsg = xs.getChildValue('generated')
            if xsg is None:
                olog.debug('Cannot read <generated> for cross-section:\n%s' % str(xs))
                # remove xsection w/o generated value
                # todo: implement GenInstance.removeCounter and maybe rewrite all this stuff
                continue
            isum += xsg
            clxsl.append(xs)
        # TODO: To see in the future what still applies of the checks below - detects when signal xsection not written
        if all([ax is not None for ax in (on2, ol2)]):
            n2 = on2.getValue()
            l2 = ol2.getValue()
            if n2 == 0:
                raise IOError('File %s: Number of generated interactions is zero.' % fn)
            if isum == 0:
                olog.error('Used %d crosssection nodes to computer nb of interactions.\n%s' % (len(xsl), str(self)))
                raise IOError('File %s: Total number of interactions reported in cross-sections is zero.' % fn)
            toll = ((float(n2) - float(isum)) / float(n2) * 100.)   # tolerance in %
            bcheckFail = n2 < isum or toll > 0.05
            if genToolName.find('repeatdecay') > -1:    # disable check for Generation.RepeatDecay.*
                bcheckFail = False
            if genToolName.find("repeatedhadronization") > -1:
                # mlog.debug("Production w/ SignalRepeatedHadronization - increase tolerance...")
                toll = abs(toll) * float(n2) / float(max(n2, isum))
                bcheckFail = toll > 0.25
            # introduce 0.25% tolerance for SignalRepeatedHadronization productions
            # disable checks for all Special productions
            if genToolName.find('special') > -1:
                # for special signal w/ fixed luminosity L2=0!
                bcheckFail = False
            # add high tolerance for SignalPlain productions which may use exotic filtering
            if bcheckFail and genToolName.find('signalplain') > -1:
                bcheckFail = toll > 5.      # changed from 2.5%
            # match between N2 and iSum may be used to extract correct processes for signal in spill-over prods
            if bcheckFail:
                raise IOError('File %s: Total generated interactions not recovered (within tolerance) '
                              'in cross-sections [%3.2f%%: %d vs. %d].' % (fn, toll, int(n2), isum))
    
    def getXmlFileName(self):
        if self._fullpath is None and len(self._xmlFiles) > 0:
            return 'GenStats_Sum(%d files)' % len(self._xmlFiles)
        if self._fullpath is None or not os.path.isfile(self._fullpath):
            return 'empty/invalid GenStats object'
        return os.path.split(self._fullpath)[1]
    
    def isCompatible(self, other):
        ilog = self.logger
        gFailed = False
        othisFn = self.getXmlFileName()
        otherFn = other.getXmlFileName()
        this_ver = self.xVersion()
        other_ver = other.xVersion()
        if this_ver != other_ver:
            gFailed = True
            ilog.error('Different <version> value detected (%s vs. %s) in file %s.' %
                       (this_ver.version, other_ver.version, otherFn))
        this_sig = self.getGenMethod(getsig=True)[0]
        other_sig = other.getGenMethod(getsig=True)[0]
        if this_sig != other_sig:
            gFailed = True
            ilog.error('Incompatible signal generator method or event type detected (%s vs. %s) in file %s.' %
                       (this_sig.fullName(), other_sig.fullName(), otherFn))
        tsc = self.xmodel.SpilloverCount()
        osc = other.xmodel.SpilloverCount()
        if tsc != osc:
            ilog.warning('Different number of spillover events in GenStats (%d vs. %d).\n'
                         'Will continue merge but spillover counters may be compromised.' % (tsc, osc))
        if gFailed:
            return False
        return not gFailed

    def __add__(self, other):
        alog = self.logger
        bFailed = False
        if not self.isCompatible(other):  # test compatibility between statistics!
            alog.warning('Ignoring incompatible counter set in \'%s\'...' % (other.getXmlFileName()))
            # keep failing while missing counters are correctly vetoed
            return self
        for mtype in self.xmodel.getMethodTypes():
            try:
                mthis = self.xmodel.getMethodByType(mtype)
                mothr = other.xmodel.getMethodByType(mtype)
                # be inclusive so everything is merged
                if mthis is None and mothr is not None:
                    self._addGenMethod(mothr)
                    continue
                if mthis is not None and mothr is None:
                    continue
                mthis += mothr
            except (ValueError, TypeError, IOError):
                bFailed = True
                msg = "Failed merging to "
                # TODO: change message here
                alog.warning("Error merging object\n%s\nTO\n%s\n### Conflicting GenStats will be ignored...\n" %
                             (str(other), str(self)), exc_info=True)
                # if 'sig' fails should broadcast to all other GenInstances, but 'sig' is always first so avoidable!
                # if mtype == 'sig': fail and return self; if sig  merge fails broadcast to all other methods?!
        # if always returning self addition success of failure can be masked!
        if not bFailed:
            self._xmlFiles.append(os.path.split(other.getXMLFullPath())[1])
            # self._fullpath = None   # keep this to mark first file in chain sum
            self._src = "sum of %d" % (len(self._xmlFiles))
        # TODO: setting other.xmodel = None ; other = None would yield gcollection of objects and recovery of memory?!
        # continue from here adding GenInstances when matching!
        return self
    
    def brRecompute(self):
        """
        Broadcast recompute message on all generator methods.
        Returns:
            Nothing.
        """
        for gmi in self._gens:
            # self.logger.info('Recomputing counters for %s...' % gmi.fullName())
            for ocnt in gmi.getCounters():
                ocnt.recompute(gmi)
        self.isMerged = True
    
    def saveFile(self, filepath, sformat='json', compress=False):
        """
        Store (merged) generator statistics in a given format (JSON or XML).
        Beware, the XML format currently saves only data for the signal.
        All data streams are saved in JSON format (generating an array of GenStats objects).
        """
        if os.path.isfile(filepath):
            mlog.warning('File \'%s\' exists. Overwriting it...' % (os.path.split(filepath)[1]))
        if sformat.lower() == 'xml':
            contents = self.encodeXML_old().encode('utf-8')
        elif sformat.lower() == 'json':
            contents = self.encodeJSON().encode('utf-8')
        else:
            raise NotImplementedError('Cannot persistify object in unknown (%s) format.' % sformat)
        if compress:
            fp = gzip.open(filepath + '.gz', 'wb')
        else:
            fp = open(filepath, 'wb')
        fp.write(contents)
        fp.flush()
        fp.close()
    
    def encodeJSON(self):
        tstamp = datetime.utcnow().strftime(GenStat.TimeStampFormat)      # as email.utils.formatdate; RFC 2822
        ret = {'timestamp':  tstamp, 'methods': [], 'isMerged': self.isMerged, 'streamsCRC32': '',
               'fmtVersion': self._fmtver.version}
        for gmo in self._gens:
            gmd = gmo.toDict()
            gmd['counters'] = list()
            for ocnt in gmo.getCounters():
                gmd['counters'].append(ocnt.toDict())
            ret['methods'].append(gmd)
        senc = json.dumps(ret['methods'], sort_keys=True).encode('utf-8')
        cksum = zlib.crc32(senc) & 0xffffffff
        ret['streamsCRC32'] = '0x%08x' % cksum
        ret['nbLogs'] = len(self._xmlFiles)
        ret['xmlFiles'] = self._xmlFiles
        return json.dumps(ret, sort_keys=True, indent=2, separators=(',', ': '))
        
    def loadJSON(self, filepath, fmt='own'):
        """
        Loads a GenStat object from JSON serialized format.
        Args:
            filepath (str): Path of the JSON file.
            fmt (str): Format of the JSON file. Defaults to 'own' format as prepared by encodeJSON

        Returns:
            True is successful, False if any error ocurred.
        """
        olog = self.logger
        if not os.path.exists(filepath):
            olog.error('JSON file not found: \'%s\'' % filepath)
            return False
        fconts = getFileContents(filepath)
        if fconts is None:
            return False
        odict = json.loads(fconts)
        if u'streamsCRC32' in odict:
            cksuma = int(odict['streamsCRC32'], 16)
            senc = json.dumps(odict['methods'], sort_keys=True).encode('utf-8')
            cksumb = zlib.crc32(senc) & 0xffffffff
            if cksuma != cksumb:
                olog.warning('Found CRC32 checksum for streams data differs from computed checksum.')
                olog.debug('Stored checksum 0x%08x vs. 0x%08x computed.' % (cksuma, cksumb))
        self.isMerged = odict['isMerged']
        _nbfiles = odict['nbLogs']
        self._xmlFiles = list(odict['xmlFiles'])
        if _nbfiles != len(self._xmlFiles):
            olog.warning('Number of XML files in merged object may be different (%d vs. %d).' %
                         (_nbfiles, len(self._xmlFiles)))
        self._fmtver = fmtVersion(odict['fmtVersion'])  # kept <version> in counter list, corruption check!
        for jj in range(len(odict['methods'])):
            sData = odict['methods'][jj]
            self._addGenMethod(GenInstance(self, sData['generator'], sData['generator_method'],
                                           evtType=sData['eventType'], njobs=sData['nb_merged']))
            for jdict in sData['counters']:
                obj = GenStat.ChildrenMap[jdict['type']]()
                obj.fromDict(jdict)
                self._gens[-1].addCounter(obj)
        self.xmodel = GenXMLModel(tuple(self._gens))
        self._src = os.path.split(filepath)[1]
        self._xmlFiles.append(self._src)
        return True
    
    def makeGenerationResults(self):
        ret = {'version': self._fmtver.version}
        sigmo = self.xmodel.getMethodByType('sig')
        for (k, v) in sigmo.toDict().items():
            ret[k] = v
        for oc in sigmo.getCounters():
            for jd in oc.getCounters():
                try:
                    ret[jd[0]] = float(jd[1])
                except ValueError as vex:
                    self.logger.warning("{0}: {1}".format(sys.exc_info()[0], vex))
                    ret[jd[0]] = jd[1]
                except Exception as zerr:
                    print(str(jd))
                    raise zerr
                # signal/anti-signal not correlated w/ particle/anti-particle!
                if jd[0] == 'I90':
                    ret['signal'] = oc.getPartName()
                if jd[0] == 'I99':
                    ret['anti-signal'] = oc.getPartName()
        self.brRecompute()
        if sigmo.isGenSpecial() and 'pp0' in ret:
            if '%3.1f' % ret['pp0'] == '0.0':
                # temporarily add special value to inhibit xsection calcs
                ret['C1N2'] = -1.0
            # yet another hack for productions with special signal embedded in Pythia8 - physical meaning?!
            if len(specialSignalProcesses) > 0:
                del ret['pp0'], ret['C1N2']
                # add Pythia8 main processes to special list in order to force xsection recomputing ?!
                # for xsn in range(101, 106):
                #     xname = 'pp%d' % xsn
                #     if xsn not in specialSignalProcesses and xname in ret:
                #         specialSignalProcesses.append(xsn)
        # self.logger.debug('Special signal list for signal embedded in Pythia: %s' % str(specialSignalProcesses))
        # computing total XS for signal productions
        nspp = len(specialSignalProcesses)
        if 'pp0' not in ret:
            xs = 0.  # will be in mb
            for oxs in sigmo.getCountersByType('crosssection'):
                # total xsection derived only from externally provided proc IDs
                if nspp > 0 and sigmo.isGenSpecial() and \
                        oxs.getProcId() not in specialSignalProcesses:
                    continue
                self.logger.debug('Add %s to total XS value %g mb' % (oxs.getProcId(), xs))
                xs += oxs.getValue()
            self.logger.warning('Total cross-section \x1b[1mcomputed\x1b[0m: %6.3f mb' % xs)
            ret['pp0'] = xs
        ret['C1'] = ret['pp0']
        del ret['pp0']
        if 'N2' in ret and 'C1N2' not in ret:
            ret['C1N2'] = ret['C1'] * float(ret['N2'])
        return ret
    
    def __eq__(self, other):
        if not self.isCompatible(other):
            return False
        mycnts = self.makeGenerationResults()
        otcnts = other.makeGenerationResults()
        ret = True
        for (k, v) in mycnts.items():
            if v != otcnts[k]:
                ret = False
                break
        return ret
    
    def getAdditionCounter(self):
        return len(self._xmlFiles)


if __name__ == '__main__':
    mlog.error("This is a Python module and should not be used as a script.\n"
               "Import and use it in your scripts in the following way:")
    print(" from GenXmlLogParser import GenStats")
    print(" statObj1 = GenStats(filepath1) # filepath1 can also be an URL using http(s) or ftp")
    print(" statObj2 = GenStats(filepath2)")
    print(" statObj1 += statObj2")
    print(" statObj1.brRecompute()  # recomputing is also done automatically when saving the merged XML")
    print("then use saveFile or makeGenerationResults to save merged \"counters\" to XML")
    print("or create a data dictionary to use in GaussStat.py to generate statistics web pages.")
    print("Production metadata is extracted from LHCbDirac book-keeping so a valid grid certificate/proxy is needed.")
