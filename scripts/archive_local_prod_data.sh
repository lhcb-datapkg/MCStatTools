#!/bin/bash

# Archives all data stored locally for a given production ID

if [ $# -lt 1 ]; then
  echo -e "Usage:\n\t\$<script> <comma-separated prod IDs>"
  exit 1
fi

declare -a pidList=$(echo "$*" | sed -e 's/,/ /g')

for p in ${pidList[@]}; do 
  [ -e prod-$p.tgz ] && rm -f prod-$p.tgz;
  [ -d "${p}" ] && dp=${p};
  [ -d "GenLogs-${p}" ] && dp=GenLogs-${p};
  tar -cvzf prod-${p}.tgz ${dp} Generation_Sim09-Beam*_pid-${p}.json Prod_0*${p}_Generation_log.json; 
  if [ $? -eq 0 ]; then
    rm -rf ${dp};
    rm Generation_Sim09-Beam*_pid-${p}.json
    rm Prod_0*${p}_Generation_log.json
  fi
done
