#! /usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright CERN for the benefit of the LHCb Collaboration                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__version__ = '20211206'

import sys
from optparse import OptionParser
import logging
import argparse
# TODO: convert to using argparse and starting in main()


def main():
    opt = OptionParser("usage: %prog [options] <ProdIDs>\n\n"
                       "\t<ProdIDs> : list of ProdID(s), comma-separated, with no blank spaces.")
    opt.add_option("-k", "--keys", action="store", type="string", dest="keyFilter",
                   default='', metavar='<KEY_FILTER>',
                   help="filter and print from meta-information dictionary only keys in comma-separated list. "
                        "Special value '@RAW' instructs to output data as returned by "
                        "BookkeepingClient.getProductionInformations in LHCbDIRAC API")
    opt.add_option("-v", "--verb-level",
                   action='store', type='choice', dest='sLogLevel',
                   choices=['debug', 'DEBUG', 'info', 'INFO', 'warn', 'WARN', 'ERROR', 'error', 'CRIT', 'crit'],
                   default='info', metavar='<VERB_LEVEL>',
                   help='case insensitive verbosity level [CRIT, ERROR, WARN, INFO, DEBUG; default: %default]')

    (options, args) = opt.parse_args()
    logLevel = logging.INFO
    tt = options.sLogLevel.lower()
    if tt == 'info':
        logLevel = logging.INFO
    elif tt == 'debug':
        logLevel = logging.DEBUG
    elif tt == 'warn':
        logLevel = logging.WARN
    elif tt == 'error':
        logLevel = logging.ERROR
    elif tt == 'crit':
        logLevel = logging.CRITICAL
    logging.basicConfig(format='[%(asctime)s] %(name)s - %(levelname)s: %(message)s', 
                        datefmt='%a, %d.%m.%Y @ %H:%M:%S', level=logLevel)
    mlog = logging.getLogger('lbMcST')
    mlog.setLevel(logLevel)
    filters = []
    getRawData = False
    if len(options.keyFilter) > 0:
        filters = options.keyFilter.split(',')
        if len(filters) == 1 and filters[0] == '@RAW':
            getRawData = True
    import lbDirac_utils as lbDrk

    if len(args) != 1:
        opt.error("Incorrect number of arguments, should be one comma-separated list of ProdID(s).")
    mlog.debug("args= %s" % repr(args))
    if ',' not in args[0]:
        args[0] = args[0].replace(' ', ',')
    list_Dirac_id = map(lambda x: int(x), args[0].split(','))

    if lbDrk.cmdCheckProxy():
        mlog.error('No valid LHCbDIRAC proxy is available. Exiting...')
        sys.exit(1)

    for prod in list_Dirac_id:
        dd = lbDrk.dirac_getProdInfo2(prod, rawresponse=getRawData)
        dmsg = {}
        if not getRawData and len(filters) > 0:
            for i in range(len(filters)):
                fk = filters[i]
                if fk in dd.keys():
                    dmsg[fk] = dd[fk]
        else:
            dmsg = dd
        if len(dmsg) == 0:
            mlog.error('No meta-info for prod. \x1b[1m#%d\x1b[0m.' % prod)
            continue
        tmsg = ['For ProdID. #%d:' % prod, ]
        # check prod has XMLs on Log-SE
        lfnpath = lbDrk.prod_lpath % dmsg
        rr = lbDrk.ls_SE_path(lfnpath, getResponse=True)
        dmsg['XMLs on LOG-SE'] = 'ERR'
        if rr is not None:
            dmsg['XMLs on LOG-SE'] = 'YES'
        for (k, v) in dmsg.items():
            tmsg.append(' - %s : %s' % (k, repr(v)))
        tmsg.append(13*'=')
        mlog.info('\n'.join(tmsg))
    logging.shutdown()


if __name__ == "__main__":
    main()
