#!/bin/bash

# Rename all HTMLs for Sim08 productions to new filename format

for fn in $(ls -1 Generation_Sim08*.html); do
  oldifs=$IFS;
  IFS='-' read -r -a toks <<< "$fn"
  htok=${toks[0]}
  beam=${toks[1]}
  mstate=${toks[2]}
  tyear=${toks[3]}
  # when tyear not all digits may be parsing already converted file names
  [ $(expr "x${tyear}" : "x[0-9]\+$") -lt 2 ] && continue;
  etok=
  # try to capitalize first letter to obtain better compatibility w/ Sim09+ convention
  for((i=4; i<${#toks[@]}; ++i)); do
    ptok=${toks[$i]}
    ptok="$(tr '[:lower:]' '[:upper:]' <<< ${ptok:0:1})${ptok:1}"
    if [ $i -eq 4 ]; then
      etok="${ptok}"
    else
      etok="${etok}-${ptok}"
    fi
  done
  [ "$mstate" == "mu100" ] && mstate=MagUp;
  [ "$mstate" == "md100" ] && mstate=MagDown;
  mv "$fn" "$htok-$beam-$tyear-$mstate-$etok"
done
