#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright CERN for the benefit of the LHCb Collaboration                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Rename production report from new (Sim09+/Dirac) to old (Sim08) naming convention.
"""

import os
import sys
import re
import shutil
# TODO: convert to using argparse and starting in main()

tkReplace = {'MagUp': ('magstate', 'mu100'), 'MagDown': ('magstate', 'md100'), r'Nu([0-9\.]+)': ('pileup!R', r'nu\1')}
tkId = {r'Sim[0-9]+': 'sim', r'Beam[1-9][0-9]*GeV': 'beam', r'Mag(?:Up|Down)': 'magstate', r'[1-9][0-9]{3}': 'year',
        r'Nu[0-9\.]+': 'pileup', r'Pythia(?:6|8)': 'gen'}
oldFnTemplate = '%(sim)s-%(beam)s-%(magstate)s-%(year)s-%(pileup)s-%(gen)s'
newFnTemplate = '%(sim)s-%(beam)s-%(year)s-%(magstate)s-%(pileup)s-%(gen)s'


def showHelp():
    print('Script usage:')
    print('python %s <Generation_Sim....html>' % (os.path.split(__file__)[1]))


if __name__ != '__main__':
    showHelp()
    exit(200)

if len(sys.argv) < 2:
    showHelp()
    exit(1)

tgFile = sys.argv[1]

if not os.path.exists(tgFile) or not tgFile.startswith('Generation_') or not tgFile.endswith('.html'):
    showHelp()
    exit(2)

tgDir, tgFile = os.path.split(tgFile)

tkDict = {}
tkNames = []

tfn = os.path.splitext(tgFile)[0][11:]

tks = tfn.split('-')

if len(tks) < 1:
    showHelp()
    exit(3)

for i in range(len(tks)):
    tk = tks[i]
    for (rxp, name) in tkId.items():
        if name in tkDict:
            continue
        if re.match(rxp, tk) is not None:
            tkNames.append(name)
            tkDict[name] = tk
            break

tt = '-'.join([tkDict[n] for n in tkNames])
try:
    if tt != newFnTemplate % tkDict:
        print('Cannot rename file name with unknown naming convention...')
        showHelp()
        exit(4)
except Exception as exx:
    print('Error: %s' % str(exx))
    showHelp()
    exit(4)

for (nn, val) in tkDict.items():
    ret = [tx for tx in tkReplace.items() if tx[1][0].startswith(nn)]
    # [ (match, (name[!replace_type], replace_value) , ]
    if len(ret) == 0:
        continue
    if len(ret) > 1:
        ret2 = [tx for tx in ret if re.match(tx[0], val) is not None]
        ret = ret2
    if len(ret) != 1:
        print('Cannot perform replacement in tag \'%s\'. Multiple RE match: %s' % (nn, ', '.join([x[0] for x in ret])))
        continue
    mre = ret[0][0]
    rpltype = ret[0][1][0][-2:]
    rstr = ret[0][1][1]
    if rpltype.upper() == '!R':
        tkDict[nn] = re.sub(mre, rstr, val)
    else:
        tkDict[nn] = val.replace(mre, rstr)

tgPath = os.path.join(tgDir, tgFile)
tgnPath = os.path.join(tgDir, 'Generation_%s.html' % (oldFnTemplate % tkDict))

if tgPath == tgnPath:
    print('File names match in old and new naming convention. Aborting...')
    exit(5)

# copy file to file with new name (complex file system meta data is lost!)
shutil.copy2(tgPath, tgnPath)
# replace links in HTML
fp = open(tgnPath, 'r')
tt = fp.read()
fp.close()
repTxt = tt.replace(tfn, oldFnTemplate % tkDict)
fp = open(tgnPath, 'w')
fp.write(repTxt)
fp.flush()
fp.close()
