# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright CERN for the benefit of the LHCb Collaboration                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""Module implementing helper tools to handle and extract various structures in tar.gz compressed log files"""

import os
import shutil
import tarfile
import zlib
import logging
import re
import six

mlog = logging.getLogger('lbMcST.%s' % __name__)
mlog.setLevel(mlog.parent.level)
__version__ = '20211205'
if __name__ == '__main__':
    mlog.error('This module cannot be run as a script.')
    raise NotImplementedError('Aborting execution...')


def getUniqueFilename(dstPath, baseFilename, fileExt, jobId):
    """Returns -1 if cannot determine a valid unique filename."""
    fExists = True
    jid = jobId
    while fExists:
        jid += 1
        sjid = str(jid).zfill(8)
        fpath = os.path.join(dstPath, '%s_%s.%s' % (baseFilename, sjid, fileExt))
        if not os.path.exists(fpath):
            fExists = False
            break
        if jid - jobId > 100000:
            break
    if fExists:
        return -1
    return jid

# TODO: Multi-threading implementation - needs careful balance of existing 
#  disk space on local machine /tmp (network FS may be flooded!)


def untarProdFiles(prod, prod_zlogs, cwd_path,
                   nbMaxLogs=None, filesToExtract=('jobDescription.xml', 'GeneratorLog.xml')):
    mlog.debug('Called untarProdFiles with arguments\nprod_zlogs: %s\ncwd_path: %s\nfilesToExtract: %s' % 
               (repr(prod_zlogs), cwd_path, repr(filesToExtract)))
    if not isinstance(filesToExtract, list) or len(filesToExtract) < 1 or \
            not isinstance(filesToExtract[0], six.string_types):
        raise ValueError('Invalid argument type/value for filesToExtract.')
    cjobId = 10000000
    fcount = 0
    tcnt = int(0.05 * float(nbMaxLogs))
    prodstring = str(prod).zfill(8)
    tgzPatterns = [r'.*\.tar$', r'.*\.tar\.gz$', r'.*\.tgz$', r'.*\.targz$']
    for z_file in prod_zlogs:
        # ----------------------------------------------------------
        # copied from Vladimir Romanovsky's script Untar_Castor.py
        try:
            f = tarfile.open(z_file, "r:gz")
        except (tarfile.ReadError, IOError):
            mlog.warning('Problem in tarfile %s (open), skipping...' % z_file)
            continue
        try:
            names = f.getnames()
            mlog.info('Succesfully read %d names from %s ...' % (len(names), z_file))
        except (IOError, EOFError, zlib.error):
            mlog.warning('Problem in tarfile %s (unpack), skipping...' % z_file)
            f.close()
            continue
        logNames = [lname for lname in names if any([not re.match(fpatt, lname) is None for fpatt in filesToExtract])]
        tarNames = [tname for tname in names if
                    any([not re.match(fpatt, tname) is None and f.getmember(tname).isfile() for fpatt in tgzPatterns])]
        mlog.debug("Detected %d interesting files" % (len(tarNames)))
        # if large disk space available may opt to extract all files at once .extractall([ .getmember(...)])
        for name in logNames+tarNames:
            # try to keep job number from logs
            jobstring = None
            nxName = os.path.basename(name)
            # vetoed files of no interest
            if name.startswith(prodstring):
                tt = os.path.split(name)[0]
                rt, tt = os.path.split(tt)
                if tt is not None and len(tt) > 0:
                    jobstring = tt
                # in case jobstrings overlap
                tt = os.path.split(rt)[1]
                if tt is not None and len(tt.strip('0')) > 0:
                    jobstring = '%s_%s' % (tt.strip('0'), jobstring)
            if not f.getmember(name).isfile():  # ensure all extracted data are files
                continue
            f.extract(name, path=cwd_path)
            mlog.debug('Extracted successfully %s::%s' % (os.path.basename(z_file), name))
            if name in logNames:
                if nxName.find(prodstring) != -1:  # if production string already in file name assume unique!
                    continue
                nxName = os.path.splitext(nxName)[0]
                nxName = '%s_%s' % (nxName, prodstring)
                if jobstring is None:  # fallback for packed logs not respecting the rules!
                    tt = getUniqueFilename(cwd_path, nxName, 'xml', cjobId)
                    if tt == -1:
                        mlog.warning('Cannot determine a valid filename for \'%s\' from %s:%s. Skipping...' %
                                     (nxName, z_file, name))
                        # leave extracted file!
                        continue
                    else:
                        cjobId = tt
                    jobstring = str(cjobId).zfill(8)
                nxName = "%s_%s.xml" % (nxName, jobstring)
                shutil.copy(os.path.join(cwd_path, name), os.path.join(cwd_path, nxName))
                fcount += 1
                # and remove previous file; Paranoid to-do (on distributed FS): test copy succeeded!
                try:
                    os.remove(os.path.join(cwd_path, name))
                    os.removedirs(os.path.join(cwd_path, os.path.dirname(name)))
                except Exception as exr:
                    mlog.debug('For %s_%s: %s' % (prodstring, jobstring, str(exr)))
                    pass
            else:  # name.endswith('.tar') or name.endswith('.tgz')
                try:
                    f2 = tarfile.open(os.path.join(cwd_path, name), "r:gz")
                    f2.closed = False
                except tarfile.ReadError as tex:
                    mlog.warning("Bad tgz file: '%s'. Skipping..." % os.path.join(cwd_path, name))
                    if 'f2' not in locals():
                        continue
                    if hasattr(f2, 'closed') and not f2.closed:
                        f2.close()
                        f2.closed = True
                    # may need further cleaning here!
                    continue
                try:
                    logNames = [llname for llname in f2.getnames() if
                                any([re.match(fpatt, llname) is not None for fpatt in filesToExtract])]
                    finfos = [f2.getmember(xmlFile) for xmlFile in logNames if f2.getmember(xmlFile).isfile()]
                    if len(finfos) == 0:
                        mlog.warning('No log files found(/extractable) in %s:%s [0/%d]. Skipping...' %
                                     (z_file, name, len(logNames)))
                        f2.close()  # clean-up!
                        f2.closed = True
                        try:
                            os.remove(os.path.join(cwd_path, name))
                            os.removedirs(os.path.join(cwd_path, os.path.dirname(name)))
                        except OSError:
                            pass
                        continue
                    if len(finfos) != len(filesToExtract):
                        mlog.warning('Not all found log files were extractable in %s:%s. Proceed with %d out of %d.' %
                                     (z_file, name, len(finfos), len(filesToExtract)))
                    f2.extractall(cwd_path, finfos)
                    f2.close()
                    f2.closed = True
                    mlog.debug('Logs extracted successfully from %s:%s.' % 
                               (os.path.basename(z_file), os.path.basename(name)))
                    logNames = [os.path.basename(ff) for ff in logNames]
                    for name2 in logNames:
                        if name2.find(prodstring) != -1:
                            continue
                        nxName = os.path.splitext(name2)[0]
                        origPath = os.path.join(cwd_path, name2)
                        nxName = '%s_%s' % (nxName, prodstring)
                        # determine jobstring for GeneratorLog.xml (should be same for jobDescription.xml)
                        if jobstring is None:
                            tt = getUniqueFilename(cwd_path, nxName, 'xml', cjobId)
                            if tt == -1:
                                mlog.warning('Cannot determine a valid jobstring for logs in %s:%s. Skipping...' %
                                             (z_file, name))
                                # clean-up!
                                for fn in logNames:
                                    try:
                                        os.remove(os.path.join(cwd_path, fn))
                                    except OSError as xos:
                                        mlog.debug('Deleting %s: %s...' % (fn, str(xos)))
                                        continue
                                continue
                            else:
                                cjobId = tt
                            jobstring = str(cjobId).zfill(8)
                        nxName = '%s_%s.xml' % (nxName, jobstring)
                        shutil.copy(origPath, os.path.join(cwd_path, nxName))
                        if os.path.exists(origPath):
                            os.remove(origPath)
                    fcount += 1
                except (IOError, EOFError, zlib.error) as exp:
                    mlog.warning("Bad tgz file in %s:%s.\n%s" % (z_file, name, str(exp)))
                    for crumb in filesToExtract:
                        # remove crumbs if error during extraction!
                        try:
                            os.remove(os.path.join(cwd_path, crumb))
                        except OSError:
                            continue
                    continue
                except KeyError as exx:
                    mlog.warning('For %s::%s - %s' % (z_file, name, str(exx)))
                    f2.close()
                    f2.closed = True
                    try:
                        os.remove(os.path.join(cwd_path, name))
                        os.removedirs(os.path.join(cwd_path, os.path.dirname(name)))
                    except Exception:
                        pass
                    continue
                if not f2.closed:
                    f2.close()
            # clean-up!
            try:
                os.remove(os.path.join(cwd_path, name))
                os.removedirs(os.path.join(cwd_path, os.path.dirname(name)))
            except Exception:
                pass
        # end big if (all other files automatically ignored!)
            if nbMaxLogs is not None:
                if fcount > tcnt:
                    pct = float(fcount)/float(nbMaxLogs)*100.
                    mlog.info('Extracted %4.2f%% of requested logs (%d/%d).' % (pct, fcount, nbMaxLogs))
                    if pct < 10.:
                        tcnt = int(0.1*float(nbMaxLogs))
                    else:
                        tcnt += int(0.1*float(nbMaxLogs))
                if fcount > nbMaxLogs:
                    break
        f.close()  # close production tarball continue w/ next if any
        if fcount > nbMaxLogs:  # abort extraction if number of requested job
            break
