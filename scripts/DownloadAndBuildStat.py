#! /usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright CERN for the benefit of the LHCb Collaboration                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
Give a list of DIRAC prod IDs, and it creates folder and downloads the logs associated,
then it merges the generator statistics and creates the corresponding entry in the
HTML page (for simulation conditions as extracted from production metainfo).
"""

import os
import stat
import shutil
import logging
# see other imports after option parsing to correctly set verbosity
import xml
import sys
import math
from subprocess import Popen, PIPE, STDOUT
from glob import glob
from datetime import datetime
import time
import argparse

__version__ = '20220330'

# String and file name formats
import six

dt_format = '%a, %d.%m.%Y @ %H:%M:%S'
logDirTpl = 'GenLogs-%d'
ProdStatJSON_FnFormat = 'Prod_%08d_Generation_log.json'

mlog = logging.getLogger('lbMcST')
lbDrk = object()
XmlParser = object()

# dictionary caching for prodID the 'metadata' from LHCbDirac and/or 'stats' loaded from JSON
prodCache = {'family_map': {}}
early_jobDescription = {}
myOpts = argparse.Namespace(nb_logs=1000, bkkFile='', delta=0.07, isSamePhwg=False, useSHM=False, sLogLevel='info',
                            useLocalXMLs=False, sigList=[], extWgname='', useReqIDs=False)


def bootstrap():
    """
    Returns:
        Checks that prerequisite features and modules are available for the script to run and sets access protocols and
        methods in case of redundant mechanisms used in accessing needed external resources and services.
    """
    global prodCache
    if sys.version_info.major < 3:
        print("Python 2 and earlier are not longer supported by this package. See GIT page at "
              "https://gitlab.cern.ch/lhcb-datapkg/MCStatTools.")
        return False
    return True


def getProdsBKK(bkPathFile, log=mlog):
    if len(bkPathFile) == 0 or not os.path.isfile(bkPathFile):
        return []
    fp = open(bkPathFile, 'r')
    bkPaths = fp.readlines()
    fp.close()
    import lbDirac_utils as lDk
    retp = []
    for bkp in bkPaths:
        tks = bkp.strip('/').split('/')
        if tks[0] != 'MC':
            log.warning('Ignoring BKK path not starting w/ \'MC\': %s' % bkp)
            continue
        if not tks[1].isdigit() or len(tks[1]) != 4:
            log.warning('Ignoring apparently malformed BKK path: %s' % bkp)
            continue
        evtType = None
        if not tks[2].startswith('Beam') and tks[2].isdigit() and len(tks[2]) == 8:
            evtType = tks[2]
            bkp = bkp.replace('/%s/' % tks[2], '/')
        rcmd = ['dirac-bookkeeping-prod4path', ]
        if evtType is None:
            rcmd += ['-B', bkp]
        else:
            rcmd += ['--EventType=%s' % evtType, '-B', bkp]
        rc, ste, sto = lDk.runExternalCmd(rcmd, capture_streams=True, logger=log)
        if rc != 0:
            log.error('Failed to run external script:\n%s\n=====\n%s' % (ste, sto))
            continue
        simlines = [x for x in sto.split('\n') if x.lower().find('simulation):') > -1 and x.lower().find('(mc') > -1]
        if len(simlines) != 1:
            log.warning('Too many output lines matching:\n%s' % '\n'.join(simlines))
            continue
        vpid = simlines[0].split(' ')[-1].strip()
        if not vpid.isdigit():
            log.error('Error matching PID in parsed output:\n%s' % sto)
            continue
        retp.append(int(vpid))
    return retp


def opt_dir_exists(argval):
    if not os.path.isdir(argval):
        raise ValueError("Provided path is not a directory path or not accessible.")
    ret = argval
    if not os.path.isabs(argval):
        ret = os.path.join(os.path.abspath(os.curdir), argval)
    return ret


def opt_file_nonempty(argvar):
    if len(argvar) == 0:
        return argvar
    fp = open(argvar, 'r')
    lns = [ln for ln in fp.readlines() if len(ln.strip()) > 0]
    fp.close()
    if len(lns) == 0:
        raise ValueError("Provide file is empty. Nothing to do w/ it.")
    return argvar


def opt_check_int(argvar):
    return int(argvar)


def opt_valid_vlevel(argvar):
    verb_options = ['debug', 'verbose', 'verb', 'info', 'warn', 'warning', 'error', 'err', 'crit', 'critical']
    verb_options += [x.upper() for x in verb_options]
    if argvar not in verb_options:
        raise ValueError("Invalid verbosity level specified. Correct values are:\n%s" % verb_options)
    return argvar.lower()


def opt_valid_intlist(argvar):
    if isinstance(argvar, list):
        return argvar
    if not isinstance(argvar, str):
        raise TypeError("Argument value can only be a string.")
    spids = argvar.split(',')
    apids = [int(x.lstrip('0')) for x in spids if x.isdigit()]
    if len(spids) != len(apids):
        raise ValueError("Invalid production ID(s) in argument list. Please, specify only comma separated numbers.")
    return apids


def opt_valid_WG(argval):
    if not isinstance(argval, six.string_types):
        raise argparse.ArgumentTypeError('Please, provide a string as the WG name value.')
    if len(argval) == 0:
        return argval
    import GaussStat
    wgdirs = GaussStat.httpGetWGNames()
    lwgdirs = [x.lower() for x in wgdirs]
    if len(wgdirs) == 0:    # fail silently
        return argval
    if not argval.lower().endswith('-wg'):
        argval += '-WG'
    if argval.lower() not in lwgdirs:
        raise ValueError('Could not find provided WG name ("%s") in repository.' % argval)
    if argval not in wgdirs:
        argval = wgdirs[lwgdirs.index(argval.lower())]
    return argval


def parse_options():
    global myOpts, mlog

    optparser = argparse.ArgumentParser()
    optparser.add_argument("-n", "--number-of-logs", type=int, dest="nb_logs", default=1000,
                           help="number of logs to download [default : %(default)s]")
    ###
    optparser.add_argument("-f", "--file", type=opt_file_nonempty, dest="bkkFile", default="", metavar="BKK_PATHS_FILE",
                           help="path to text file containing BKK paths for productions. "
                                "The file must contain one BKK path per line.\n"
                                "(!) <PIDs> are not ignored if also present on command line.")
    ###
    optparser.add_argument("-r", "--req", action="store_true", dest="useReqIDs",
                           help="inform script to consider the <PIDs> as a list of Request IDs and extract "
                                "MC*Simulation production/transformation IDs automatically from LHCbDIRAC.")
    ###
    optparser.add_argument("-s", "--sig", action="append", default=[], type=opt_check_int, dest="sigList",
                           metavar="SIG_PIDS",
                           help="Signal process ID(s) for special generation tool. Use multiple times to give "
                                "a list of IDs. Process IDs are event generator specific and should be extracted "
                                "from event type definition specific file from Gen/DecFiles package. Specify signal "
                                "process IDs for all productions(event types) to process. Default: %(default)s.")
    ###
    optparser.add_argument("-i", "--ask-once", action="store_true", dest='isSamePhwg',
                           help="ask only once for physics working group name when generating statistics tables and "
                                "cache value for all productions [default : %(default)s].")
    ###
    optparser.add_argument("-w", "--phys-wg", action="store", dest="extWgname",
                           metavar='<WG-name>', default='', type=opt_valid_WG,
                           help="Provide physics WG name to use in processing productions. This implies that "
                                "the script is run non-interactively ('-i' is ignored if also present) and argument "
                                "value maps to a valid WG repository location. WG names should be w/o trailing '-WG' "
                                "and may be case insensitive (e.g. bandq for BandQ-WG). Default: \"\".")
    ###
    optparser.add_argument("--use-mem", action="store_true", dest="useSHM",
                           help="instruct new download algorithm to use memory as temporary buffer [%(default)s].")
    ###
    optparser.add_argument("--delta-size", action="store", type=float, dest="delta", default=0.07, metavar='DELTA',
                           help="XML files smaller by <DELTA> from largest file of sample will be deleted/ignored "
                                "[default : %(default)s]. It is used only when file size distribution is non-gaussian.")
    ###
    optparser.add_argument("-l", "--use-local-logs", action="store_true", dest="useLocalXMLs",
                           help="Use already downloaded XML logs (to recover processing after crash).")
    ###
    optparser.add_argument("-d", "--dir", type=opt_dir_exists, dest="workPath", metavar="WORKPATH",
                           default=os.path.abspath(os.curdir),
                           help="change work path to provided value. If path is not absolute, a relative path to "
                                "current directory is resolved [default: os.getcwd()].")
    ###
    gverb = optparser.add_mutually_exclusive_group(required=False)
    ###
    gverb.add_argument("-v", "--verbose", action="count", default=0, dest="logMore",
                       help="Increase logging verbosity by one level specifying multiple times [warn].")
    ###
    gverb.add_argument("-V", "--verb-level", action="store", type=opt_valid_vlevel, dest="sLogLevel",
                       default="warn", metavar="VERB_LEVEL",
                       help="case insensitive verbosity level (crit, err, warn, info, debug, verb) [%(default)s]. "
                            "Complete words are also accepted as valid values e.g. crit, critical. Verbosity "
                            "increase (-v flag) is ignored when level is expresively specified.")
    ###
    # accepts all remaining arguments as lists of PIDs - TODO: might consider ReqIDs to be also str!
    optparser.add_argument(metavar="PIDs", type=opt_valid_intlist, dest="argPIDs", default=[], nargs='*',
                           help="Comma separated (no blanks) list of production IDs to process. The script stops "
                                "execution if no MC*Simulation production IDs are provided/found for processing.\n"
                                "Mind the -r/--req flag which allows using Request IDs instead of Production IDs!")
    myOpts = optparser.parse_args()
    if myOpts.nb_logs < 1000:
        optparser.error(message='The number of logs parameter sets the upper limit on XML files retrieved '
                                'for processing, so it is not advisable to force it below default value.')
    if len(myOpts.extWgname) > 0:
        myOpts.isSamePhwg = True
    diracPIDs = [pid for sublst in myOpts.argPIDs for pid in sublst]
    diracPIDs += getProdsBKK(myOpts.bkkFile)
    myOpts.pidsDirac = list(set(diracPIDs))
    del myOpts.argPIDs      # no use for this list of lists any more!
    if len(myOpts.pidsDirac) == 0:
        optparser.error(message="No valid simulation production IDs provided. Nothing to do but abort.")
    base_path = myOpts.workPath
    if myOpts.useSHM:       # if instructed to use shared memory
        tt = os.path.abspath(os.curdir)
        if base_path.find(tt) == 0:
            myOpts.workPath = os.path.join('/dev/shm', base_path[len(tt):])
    myOpts.logLevel = logging.WARNING
    tt = myOpts.sLogLevel
    if tt == 'info':
        myOpts.logLevel = logging.INFO
    elif tt in ('debug', 'verb', 'verbose'):
        myOpts.logLevel = logging.DEBUG
    elif tt in ('warn', 'warning'):
        myOpts.logLevel = logging.WARN
    elif tt in ('err', 'error'):
        myOpts.logLevel = logging.ERROR
    elif tt in ('crit', 'critical'):
        myOpts.logLevel = logging.CRITICAL
    if myOpts.logMore > 0:
        tt = logging.WARNING - 10 * myOpts.logMore
        if tt < logging.DEBUG:
            tt = logging.DEBUG
        myOpts.logLevel = tt
    logging.basicConfig(format="[%(asctime)s] %(name)s - %(levelname)s: %(message)s",
                        datefmt=dt_format, level=myOpts.logLevel)
    mlog = logging.getLogger('lbMcST')
    mlog.setLevel(mlog.parent.level)
    mlog.info('Running DownloadAndBuildStat v. %s' % __version__)
    mlog.info("Verbosity level set to %s. Will be using UTC from here on." % (myOpts.sLogLevel.upper()))
    # switch to UTC is most likely done when importing LHCbDIRAC


def get_GenerationXml(cwd, nbmaxlogs, prodId):
    """
    returns GenStats sum object and number of XML log files available before merging.

    Args:
        cwd (str):  current working directory path
        nbmaxlogs (int): number of XML files to process
        prodId (int): production transformation ID to consider
    """
    Generation_files = glob(os.path.join(cwd, XmlParser.logBaseName + '*.xml'))
    Generation_files += glob(os.path.join(cwd, XmlParser.logBaseName + '*.xml.tgz'))
    max_ = min(nbmaxlogs, len(Generation_files))
    mstat = None
    nmerged = 0
    mlog.info('Will merge %d %s XML files from %s ...' % (max_, XmlParser.logBaseName, cwd))
    if len(Generation_files) == 0:
        raise ValueError('Could not find any log files for the production ID #%d.' % prodId)
    for Generation in Generation_files:
        smsg = '- merge %s' % (os.path.basename(Generation))
        try:
            stat_add = XmlParser.GenStat(filepath=Generation)
            if not isinstance(stat_add, XmlParser.GenStat):
                smsg += " : bad file, skipping it."
                mlog.error(smsg)
                continue
            if mstat is None:
                mstat = stat_add
                msig = mstat.xmodel.getMethodByType('sig')     # ref to signal genstat
            else:
                mstat += stat_add
            nmerged = msig.nb_jobs()    # contains correct number of signal genstats merged
            smsg += ' (@%d)' % nmerged
            mlog.info(smsg)
            if nmerged == max_:
                break
        except xml.parsers.expat.ExpatError:
            smsg += " : bad file, skipping it."
            mlog.error(smsg)
            mlog.debug('', exc_info=True)
            continue
        except IOError as exx:
            smsg = '{0}\n{1}{2}'.format(exx, smsg, " : corrupted/inaccesible file, skipping it.")
            mlog.error(smsg)
            mlog.debug('', exc_info=True)
            continue
        except ValueError as ex2:
            smsg += ' invalid XML data. Skipping file.'
            mlog.warning(smsg)
            mlog.debug(str(ex2), exc_info=True)
            continue
    return mstat, nmerged


def mkJoin_Html(pID, statDict, inb_jobs):
    # import LbMCStatTools.utils.GaussStat as GaussStat
    import GaussStat
    mlog.info('Get meta-information from DIRAC...')
    jobDescription = {}
    # retrieve production information from DIRAC - use early cached info if valid
    if pID in prodCache and 'metadata' in prodCache[pID] and \
            'diracOK' in prodCache[pID]['metadata'] and prodCache[pID]['metadata']['diracOK']:
        jobDescription = dict(prodCache[pID]['metadata'])
    if len(jobDescription) == 0:  # try get fresh metadata from LHCbDIRAC
        jobDescription = lbDrk.dirac_getProdInfo2(pID)
    # retrieve the evttype from the logs
    if not jobDescription['diracOK']:
        mlog.error('DIRAC BKK sub-system interogation failed. Cannot get metadata for Prod. ID %d. Skipping...' % pID)
        return True
    # table_name = GaussStat.statTableFnFmt % jobDescription
    # check event types match
    if all(['eventType' in x for x in (statDict, jobDescription)]):
        if statDict['eventType'] != jobDescription['eventType']:
            mlog.error('For Prod. ID %d event type in logs (%s) does not match event type from \
            LHCbDirac (%s). Skipping...' % (pID, statDict['eventType'], jobDescription['eventType']))
            return True
    else:
        if 'eventType' in statDict:
            mlog.error('Could not check event type for Prod. ID %d. Skipping...' % pID)
            return True
    jobDescription['prodID'] = pID
    for key, value in prodCache['family_map'].items():
        for item in value:
            if item['pid'] == pID:
                jobDescription["requestID"] = key
                break
    # in the unlike case this happens maybe we should interrogate again LHCbDirac
    if 'requestID' not in jobDescription:
        mlog.error("!!! Could not find Request Id for Production Id #{} !!!".format(pID))
    mlog.debug('Metadata before BuildStat: %s' % str(jobDescription))
    # TODO: The number of merged job logs may be eliminated as argument
    if 'nb_merged' in statDict:
        inb_jobs = statDict['nb_merged']
    # TODO: Wish it could be implemented more elegantly than setting wgName at module level
    if len(myOpts.extWgname) > 0:
        GaussStat.wgName = myOpts.extWgname
    return GaussStat.BuildStat(statDict, jobDescription, inb_jobs, isSamePhwg=myOpts.isSamePhwg) != 'OK'


def countLocalFiles(dirPath, fileMask=None, fileExt='xml'):
    import shlex
    ret = None
    cbf = 0
    useShell = False
    lpath = dirPath
    if fileMask is not None:
        lpath = os.path.join(dirPath, fileMask)
    if os.path.isfile(lpath):
        fileMask = lpath
        fileExt = os.path.splitext(lpath)[1]
    cmds = shlex.split('ls -1 "%s"' % lpath)
    if fileMask is not None:
        cmds = ' '.join(cmds)
        useShell = True
    proc = Popen(cmds, stdout=PIPE, stderr=STDOUT, stdin=PIPE, shell=useShell)  # avoid caching issue on network FS
    while True:
        line = proc.stdout.readline().strip()
        if line.find(b'No such') > -1:  # file or directory
            break
        if len(line) == 0:
            break
        if line.endswith(fileExt):
            cbf += 1
    if proc.wait() not in (0, 2):
        mlog.error("Invalid return code %d in 'ls' shell." % proc.returncode)
    else:
        ret = cbf
    return ret


def preValidateLocalXMLs(prod_dir, pid, logger=mlog):
    sizes, files = [], []
    pdir_files = os.listdir(prod_dir)
    for xfn in pdir_files:
        if os.path.isdir(os.path.join(prod_dir, xfn)):
            continue
        if not (xfn.lower().endswith('.xml') or xfn.lower().endswith('.xml.tgz')):
            continue
        files.append(xfn)
        sizes.append(os.stat(os.path.join(prod_dir, xfn))[stat.ST_SIZE])
    if len(sizes) < myOpts.nb_logs/4:
        logger.warning('Too little XML files selected. No further size filtering applied.')
        sizes = []  # only check for binary duplicates
    # removing log files with outlier file sizes (4-3 sigma) - use options.delta if not-gaussian
    if len(sizes) > 1:
        fnsizes = float(len(sizes))
        mean_size = float(sum(sizes))/fnsizes
        sig_size = sum([(float(x)-mean_size)*(float(x)-mean_size) for x in sizes])
        sig_size = math.sqrt(sig_size/(fnsizes-1.))
        sz_mean = int(mean_size)
        sz_deviation = int(4.*sig_size)
        if sz_mean - sz_deviation < 0 or sig_size > mean_size/3.:
            sz_deviation = 0    # non-gaussian?
        if sz_deviation < int(myOpts.delta * mean_size):
            sz_deviation = int(myOpts.delta * mean_size)
            
            def size_filter(x):
                return x < sz_deviation
            logger.debug('Deleting XML files with size below %d byte(s).' % sz_deviation)
        else:
            def size_filter(x):
                return x < (sz_mean-sz_deviation) or x > (sz_mean+sz_deviation)
            logger.debug('Deleting XML files with outlier size of %d +/- %d byte(s).' % (sz_mean, sz_deviation))
        logger.info('Prod. %d: get the sizes of the logs' % pid)
        smallFiles = []
        passfiles = []
        for ik in range(len(files)):
            if size_filter(sizes[ik]):
                smallFiles.append(files[ik])
            else:
                passfiles.append(files[ik])
        for ifn in smallFiles:
            os.unlink(os.path.join(prod_dir, ifn))

    else:
        passfiles = files
    safefiles = []
    dmap = lbDrk.findFileDups(prod_dir, passfiles)
    if any([len(x) > 1 for x in dmap.values()]):
        for (h, fls) in dmap.items():
            if len(fls) > 1:
                mlog.info('Removing XML duplicates: %s' % ', '.join(fls[0:-1]))
                for ii in range(len(fls)-1):
                    os.unlink(os.path.join(prod_dir, fls[ii]))
            safefiles.append(fls[-1])
    else:
        safefiles = passfiles
    return safefiles


# script starts here!
def main():
    global myOpts, lbDrk, XmlParser
    if not bootstrap():
        sys.exit(7)
    parse_options()
    online_pids = []
    pids_toget = []     # remaining pid to process

    # import LbMCStatTools.utils.lbDirac as lbDrk       # fails here if LHCbDirac environment not set
    # import LbMCStatTools.utils.GenStatParser as XmlParser
    import lbDirac_utils as lbDrk
    import GenStatParser as XmlParser
    if len(myOpts.sigList) > 0:
        XmlParser.specialSignalProcesses = list(myOpts.sigList)
    mlog.info('Checking valid proxy is initialized...')
    bHasValidProxy = True
    bAutoRespawn = False
    while lbDrk.cmdCheckProxy2():
        bAutoRespawn = True
        if lbDrk.cmdInitProxy2():
            bHasValidProxy = False
            break
    if not bHasValidProxy:
        mlog.error('No valid Dirac proxy could be initialized. Will continue using fall-backs !!!')
    if bHasValidProxy and bAutoRespawn:
        mlog.warning('Script auto-respawn to inherit correct environment ...')
        if 'MCSTATTOOLSSCRIPTS' not in os.environ:
            mcstatScriptPath = os.path.abspath(os.getcwd())
        else:
            mcstatScriptPath = os.path.abspath(os.path.expandvars(os.environ['MCSTATTOOLSSCRIPTS']))
        cmd = ['python', os.path.join(mcstatScriptPath, sys.argv[0])] + sys.argv[1:]
        mlog.debug('Respawn using command:\n\t%s' % (' '.join(cmd)))
        p = Popen(cmd)
        time.sleep(0.3)
        sys.exit(0)
    # prevent log output from LHCb DIRAC component
    dirac_logger = logging.getLogger('gfal2')
    dirac_logger.setLevel(logging.WARN)
    myOpts.ReqsDirac = None
    if myOpts.useReqIDs:
        myOpts.ReqsDirac = list(myOpts.pidsDirac)
        mlog.warning("Will attempt to map provided request IDs to production IDs using LHCbDIRAC API.")
        rr = lbDrk.getSimProdIdForRequest(myOpts.pidsDirac)
        if rr is None:
            mlog.error('Mapping of Request to Production IDs has failed. Aborting execution...')
            sys.exit(33)
        if len(rr) == 0:
            mlog.error('No MC*Simulation transformations for given request IDs. Aborting...')
            sys.exit(11)
        myOpts.pidsDirac = []
        if mlog.isEnabledFor(logging.INFO):
            msg = "Will process following Production/Transformation IDs associated to given Request IDs:\n"
        prodCache['family_map'].update(rr)
        for rid, dpids in rr.items():
            pids = [x['pid'] for x in dpids]
            myOpts.pidsDirac += pids
            if mlog.isEnabledFor(logging.INFO):
                msg += 'Request #{}: {}'.format(rid, ', '.join([str(x) for x in pids]))
        if mlog.isEnabledFor(logging.INFO):
            msg += '-'*80
            mlog.info(msg)
    import GaussStat
    GaussStat.logAlways(mlog, "Processing following MC*Simulation production IDs: %s" % str(myOpts.pidsDirac))
    mlog.info('Search for production logs stored in JSON and validate them...')
    # next get LHCbDIRAC meta-information for prodIDs and validate them as MC GEN productions
    ddpids = lbDrk.dirac_getProdInfo2(myOpts.pidsDirac)
    if not myOpts.useReqIDs:
        rr = lbDrk.getReqIdsForProds(list(ddpids.keys()))
        if rr is None or len(rr) == 0:
            sys.exit(33)
        prodCache['family_map'].update(rr)
    for (kp, dret) in ddpids.items():
        if not dret['diracOK']:
            # will retry automatically before constructing final genstat table
            mlog.warning('Could not retrieve production (#%d) metadata from LHCbDIRAC. Please, retry later '
                         'when Dirac can provide requested information or check production ID is valid.' % kp)
            continue
        else:
            simv = None
            if 'simVariant' in dret:
                simv = dret['simVariant']
            if 'Gauss_version' not in dret and simv is None:
                mlog.warning('Production ID #%d may not correspond to MC-Simulation step. Ignoring...' % kp)
                continue
            prodCache[kp] = {'metadata': dict(dret)}
    for pid in myOpts.pidsDirac:
        jsonFilePath = os.path.join(myOpts.workPath, ProdStatJSON_FnFormat % pid)
        existsfile = os.path.exists(jsonFilePath) and os.path.isfile(jsonFilePath)
        existsgzfile = os.path.exists(jsonFilePath + '.gz') and os.path.isfile(jsonFilePath + '.gz')
        if not (existsfile or existsgzfile):
            pids_toget.append(pid)
            continue
        tStat = XmlParser.GenStat()
        if existsgzfile:
            jsonFilePath += '.gz'
        if not tStat.loadJSON(jsonFilePath):
            mlog.warning('JSON file invalid for production #%d. Leaving it in list to get from server/ARCHIVE...' % pid)
            pids_toget.append(pid)
            del tStat
            continue
        else:
            mlog.info('Loaded Prod. ID %d from JSON(.gz). Found %d log(s) out of %d requested.' %
                      (pid, tStat.getAdditionCounter(), myOpts.nb_logs))
        online_pids.append(pid)     # to skip processing this production
        prodCache[pid]['stats'] = tStat
    myOpts.pidsDirac = list(pids_toget)
    if len(online_pids) == 0:
        mlog.info('No JSON file found for any of the given production IDs.')
        if len(myOpts.pidsDirac) == 0:
            mlog.error('No remaining valid production IDs to process. Exiting...')
            sys.exit(0)
    else:
        nloaded = len(online_pids)
        mlog.info('Valid JSON files found for %d/%d prod. IDs.' % (nloaded, nloaded + len(myOpts.pidsDirac)))
        if len(myOpts.pidsDirac) == 0:
            mlog.info('All production logs loaded from JSON. Will proceed to generation of HTML report table(s).')
        # should check if nb of logs is close to requested, but tricky issue (maybe max of nb of logs already parsed!)
    showSteps = len(myOpts.pidsDirac) > 0
    # TODO: Explore better alternatives for running w/o local disk
    workPath = myOpts.workPath
    mlog.debug('''Work path: %s
    Requested nb. XMLs: %d
    XML fragment veto level: %2.1f%%
    Prod IDs to process: %s''' % (workPath, myOpts.nb_logs, myOpts.delta * 100., str(myOpts.pidsDirac)))
    # 1. Get XMLs for productions either from local directory or download through LHCbDirac interface
    if showSteps:
        mlog.warning('\n' + '-'*72 + '\n1. Retrieving %s XML files...\n' % lbDrk.logBaseName + '-'*72)
    for prod in myOpts.pidsDirac:
        if prod not in prodCache:
            mlog.warning('Skip #%d as no metadata found in DIRAC.' % prod)
            continue
        prodWkPath = os.path.join(workPath, logDirTpl % prod)
        if not os.path.exists(prodWkPath) or not os.path.isdir(prodWkPath):
            os.mkdir(prodWkPath, 448)      # dec of 0o700
        if myOpts.useLocalXMLs:
            validFiles = preValidateLocalXMLs(prodWkPath, prod)
            if len(validFiles) > 1:
                online_pids.append(prod)
                mlog.debug('Will process XMLs for prod. #%d from %s...' % (prod, prodWkPath))
                continue
            else:
                mlog.info('Could not find enough local XMLs for prod. #%d. Fallback to normal execution.' % prod)
            del validFiles
        prodlfn = lbDrk.prod_lpath % prodCache[prod]['metadata']
        mlog.info('Extracting XMLs for prod. #%d from LHCbDIRAC SE...' % prod)
        lfns = lbDrk.get_LFNs_on_SE2(prodlfn, nbLimit=myOpts.nb_logs, localPath=prodWkPath)
        if len(lfns) == 0:
            mlog.warning('No valid xml LFNs detected on %s for production #%d. Skipping...' % (lbDrk.logSE_name, prod))
            continue
        if len(lfns) < myOpts.nb_logs:
            mlog.warning('Number of available logs smaller than requested. Will merge %d XML logs.' % len(lfns))
        # paths that are not LFNs but already downloaded XML file are prepended with file://
        mlFiles = [x for x in lfns if x.startswith('file://')]
        k = 0
        selfns = [x for x in lfns if not x.startswith('file://')]
        if len(mlFiles) > 0:
            k += len(mlFiles)
            mlog.info('Already downloaded %d XML logs while searching on LogSE...' % k)
        if len(selfns) > 0:
            # ordered getFile from LogSE - signature to possibly prevent protection mechanisms to kick in!
            selfns.sort()
            fcnt = len(selfns)
            mlog.debug('Downloading %d XML logs for prod #%d. Processing...' % (fcnt, prod))
            # sys.stdout.flush()
            pstart = datetime.now()
            tvar = pstart
            for i in range(fcnt):
                lfn = selfns[i]
                (head, xfname) = os.path.split(lfn)
                (head, jobid) = os.path.split(head)
                (head, runid) = os.path.split(head)
                suff = ''
                (fbase, fext) = os.path.splitext(xfname)
                if fext.endswith('gz'):
                    suff = '.tgz'
                # change local name on download to process correctly with tarfile
                fn = '%s-%s-%s.xml%s' % (lbDrk.logBaseName, runid, jobid, suff)
                if lbDrk.getSEfile(lfn, prodWkPath, localFilename=fn):
                    mlog.warning('Download of LFN %s from LogSE has failed. Skipping...' % lfn)
                    continue
                k += 1
                if (datetime.now() - tvar).seconds > 15:
                    tvar = datetime.now()
                    mlog.debug('Prod %d: %3.2f%% XMLs processed...' % (prod, float(k)/float(fcnt)*100.))
                if k >= myOpts.nb_logs:
                    break
        validFiles = preValidateLocalXMLs(prodWkPath, prod)
        if len(validFiles) == 0:
            mlog.warning('No downloaded/available log files for production. Skipping Prod ID %d ...' % prod)
            if myOpts.useSHM and os.path.exists(prodWkPath):
                shutil.rmtree(prodWkPath)
        else:
            online_pids.append(prod)
        del validFiles
    # 2. Merging XMLs and running GaussStat
    if showSteps:
        mlog.warning('\n' + '-'*72 + '\n2. Run GaussStats\n' + '-'*72)
    if myOpts.useSHM:
        mlog.warning('Algorithm will not clean-up memory in case of prod. XML processing error.')
    for prod in online_pids:
        mlog.info('for ProdID %d:' % prod)
        cwd_path = os.path.join(workPath, logDirTpl % prod)
        # merge the xmls
        # condition saving JSON number of actually merged XML logs
        try:
            if 'stats' not in prodCache[prod]:
                prod_stat, nb_files = get_GenerationXml(cwd_path, myOpts.nb_logs, prod)
                # decide whether to save JSON
                try:
                    # save json in parent not production dir - default action!
                    jsonFilename = ProdStatJSON_FnFormat % prod
                    jsonFilePath = os.path.join(myOpts.workPath, jsonFilename)  # always on disk!
                    cb_logs = prod_stat.getAdditionCounter()
                    if nb_files != cb_logs:
                        if nb_files < 1:
                            pct_fail = 1.
                            nbads = nb_files = cb_logs
                        else:
                            pct_fail = float(nb_files - cb_logs) / float(nb_files)
                            nbads = nb_files - cb_logs   # unrecoverable bad files below requested limit
                        res_msg = 'Detected %d corrupted logs out of %d available (see option -n).' % \
                                  (nbads, nb_files)
                        if pct_fail < 0.15 and nbads > 0:
                            mlog.info(res_msg)
                        elif 0.15 < pct_fail < 0.35:
                            mlog.warning(res_msg)
                        elif pct_fail > 0.35:
                            if len(myOpts.sigList) > 0:
                                mlog.error(res_msg)
                            else:
                                mlog.error('%s\n%s' % (res_msg,
                                                       'JSON/HTML file will not be saved for Prod. ID % d ...' % prod))
                                continue
                    prod_stat.saveFile(jsonFilePath, compress=True)
                    mlog.info('Prod. ID %d: Saved JSON to %s.' % (prod, jsonFilePath + '.gz'))
                except AttributeError:
                    mlog.error('Invalid GenStats sum for Prod. ID %d. Skipping...' % prod)
                    continue
            else:
                prod_stat = prodCache[prod]['stats']
            stat_dic = prod_stat.makeGenerationResults()
            nb_jobs = prod_stat.getAdditionCounter()    # should always correspond to nb of merged signal genstats
        except UnboundLocalError:   # maybe these exceptions would not arise anymore?!
            mlog.error("No good generator log, try with more (-n option). Skipping Prod ID %d" % prod,  exc_info=True)
            continue
        except AttributeError as ex:
            mlog.error("%s\nNo valid generator log found for Prod. ID %d "
                       "(open JIRA if you think it is a parsing/merging bug). Skipping..." % (str(ex), prod))
            continue
        if stat_dic is None:
            mlog.debug('!!! stat_dic === None')
            mlog.warning('Run the script with more jobs (-n option). Skipping Prod ID %d...' % prod)
            continue
    
        if nb_jobs < myOpts.nb_logs//2:
            mlog.warning('The number of logs actually used for the stats is %d (out of %d requested - see option -n).' %
                         (nb_jobs, myOpts.nb_logs))
    
        if mkJoin_Html(prod, stat_dic, nb_jobs):
            mlog.warning('Error occured while generating/joining statistics table. Skip production ID %d ...' % prod)
        # clean-up memory as it may not be done automatically
        if myOpts.useSHM and os.path.exists(cwd_path):
            shutil.rmtree(cwd_path)
    
    if len(myOpts.pidsDirac) > len(online_pids):
        mlog.warning('Generator XML logs for %d out of %d merged in JSON files.' %
                     (len(online_pids), len(myOpts.pidsDirac)))
    mlog.info('Run finished.')
    logging.shutdown()


if __name__ == '__main__':
    main()
