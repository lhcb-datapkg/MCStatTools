#! /usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright CERN for the benefit of the LHCb Collaboration                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""Module implementing functions required to create/merge generator statistics tables.
Contains also common code for JSON repository management."""

import math
import urllib.request as ulibrq
import six
import shutil
import time
import os
import sys
import logging
import re
import json
import glob
import datetime
import requests as rq

from subprocess import Popen, PIPE, STDOUT

__version__ = '20250123'

decfilesRepoPath = '/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/Gen/DecFiles'
webRootURL = "http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS"
simNameFmt = "SIM%s"
simStatFormat = '%sSTAT'
apiRootURL = webRootURL + '/' + simStatFormat + "/api/v1/"
resolverURL = webRootURL + '/' + simStatFormat + "/resolv.php"
simStatRepoPath = '/eos/project-l/lhcbwebsites/www/projects/STATISTICS'
simFmtRe = r'Sim(\d+).*'
statTableFnFmt = 'Generation_%(APPCONFIG_file)s.html'
statTableFnRe = r'^Generation_([^_]+)(?:_?.*)\.html$'
cacheWgName = False
wgName = None
decfilesCache = {}

mlog = logging.getLogger('lbMcST.%s' % __name__)
mlog.setLevel(mlog.parent.level)
mlog.debug('Module %s v. %s loaded.' % (__name__, __version__))

# global dict to hold data to write as JSON (alternative to HTML generation)
jsonDict = {}

# HTML fragments for parsing and page generation
thCounterFmt = '<div class="firstcell">%s</div>'
evtTHfmt = "<th colspan=\"2\"><font color=\"#0000FF\">%(evtType)s (%(evtTypeDesc)s)</font>" \
           " - Gauss <font color=\"#088A08\">%(Gauss_version)s</font><br/>" \
           "DecFiles <font color=\"#088A08\">%(DecFiles_version)s</font><br/>" \
           "DDDB <font color=\"#FF0000\">%(DDDB)s </font>" \
           "- SIMCOND <font color=\"#FF0000\">%(SIMCOND)s</font><br/>" \
           "APPCONFIG <font color=\"#088A08\">%(APPCONFIG_version)s</font> " \
           "<font color=\"#FF0000\">%(APPCONFIG_file)s</font><br/>%(genTimeStamp)s</th>\n"
statTableStart = "<a name=\""
statTableSep = "</p>\n" + statTableStart
evtTypeLinkAnchor = '<a name="%(evtType)s"> </a>\n'
evtTypeLink = "<a href=\"Generation_%(APPCONFIG_file)s.html#%(evtType)s\">%(evtType)s</a>"
evtTypeLinksContainer = "<p>Event Type : %s </p>\n"
evtTypeLinkHRef = "<p>Event Type : " + evtTypeLink + " </p>\n"
sTblHead = "<tr>\n<td align=\"center\">\n<table border=\"2\">\n<th colspan=\"%i\">%s</th>\n"
webpageHead = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" " \
              "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"\
              "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n"\
              "<link rel=\"stylesheet\" href=\"https://lhcbdoc.web.cern.ch/lhcbdoc/css/lhcb.css\" "\
              "type=\"text/css\" media=\"screen\" />\n"\
              "<link rel=\"stylesheet\" href=\"https://lhcbdoc.web.cern.ch/lhcbdoc/gauss/css/css.css\" "\
              "type=\"text/css\" media=\"screen\" />\n"\
              "<title>Table for Generation</title>\n"\
              "</head>\n<body>\n"
bodyEndTag = '</body>'
webPageEnd = bodyEndTag + "\n</html>\n"
xsUnits = ['mb', 'mub', 'nb', 'pb', 'fb']


class simVariant(object):
    """
    Implement Simulation stack versioning and contain logic for detecting and parsing version
    """
    simNameFmt = "SIM%02d"
    simStatFormat = "{}STAT"
    simFmtRe = r'Sim(\d{2,})([a-z]*)'
    
    def __init__(self, verstring=None):
        self.major = 0
        self.minor = ''
        if verstring is not None:
            self.parse(verstring)
        
    def reset(self):
        self.major = 0
        self.minor = ''
    
    def simName(self, titlecase=False):
        tt = simVariant.simNameFmt % self.major
        if titlecase:
            tt = tt[0].upper() + tt[1:].lower()
        return tt

    def simStat(self):
        return simVariant.simStatFormat.format(simVariant.simNameFmt % self.major)
    
    def __str__(self):
        t = simVariant.simNameFmt % self.major + self.minor
        t = t[0] + t[1:].lower()
        return t
   
    def parse(self, value):
        mm = re.match(simVariant.simFmtRe, value, re.I)
        if mm is None:
            return False
        if not mm.group(1).isdigit():
            return False
        if len(mm.group(2)) > 0:
            self.minor = mm.group(2).lower()
        self.major = int(mm.group(1).lstrip('0'))
        return True
    
    def isValid(self):
        ret = self.major > 0
        if len(self.minor) > 0:
            ret = self.minor.isalpha() and ret
        return ret


class StatRepo(object):
    eosRepoBasePath = '/eos/project-l/lhcbwebsites/www/projects/STATISTICS'
    fmtStatDirName = '%sSTAT'
    wapiURL = 'http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/%sSTAT/resolv.php'
    
    def __init__(self, simName, apiType='auto'):
        """
        Args:
            simName (int, string): Name of major SIM release to use when accesing repository
            apiType (string): 'auto' to determine automatically best API to use, 'disk' when repo is accessible
             on disk/distributed-FS or 'web' when code should use web API
        """
        self.simverstr = None
        if isinstance(simName, six.string_types) and simName.lower().startswith('sim'):
            mm = re.match(pattern=r'Sim0?(\d+)', flags=re.I, string=simName)
            if mm is not None:
                self.simverint = int(mm.group(1).lstrip('0'))
                self.simverstr = simName.upper()
        if isinstance(simName, int) and simName > 7:
            self.simverint = simName
            self.simverstr = 'SIM%02d' % simName
        self.method = apiType
        
    def getWGDirFromName(self, wgname):
        ret = None
        simStatPath = os.path.join(StatRepo.eosRepoBasePath, StatRepo.fmtStatDirName % self.simverstr)
        if not os.path.exists(simStatPath) or not os.path.isdir(simStatPath):
            return ret
        wgDirs = dict([(dn.lower().rstrip('-wg'), dn) for dn in os.listdir(simStatPath) if
                       os.path.isdir(os.path.join(simStatPath, dn)) and dn.lower().endswith('-wg')])
        if len(wgDirs) == 0:
            return ret
        if wgname.lower() not in wgDirs:
            return ret
        return wgDirs[wgname.lower()]


def formatCollPrint(scol, screenWidth=80, screenHeight=25, labelFmt='%d) ', itemSep='   ', EOL='\n'):
    lcol = len(scol)
    if lcol < screenHeight / 5:
        return EOL.join(scol)
    llabel = len(labelFmt % lcol)
    # if itemSep contains special chars consider correcting for displayed length
    maxisize = max([len(x) for x in scol])
    colsize = maxisize + llabel + len(itemSep)
    ncols = screenWidth // colsize
    if ncols > lcol * 2 / 3:
        ncols = lcol / 2
    if ncols < 2:
        return EOL.join(scol)
    centering = ' ' * int((screenWidth - (colsize * ncols)) // 2)
    nrows = lcol // ncols
    if lcol % ncols > 0:
        nrows += 1
    lines = []
    for j in range(nrows):
        line = []
        for i in range(ncols):
            ic = j * ncols + i
            if ic == lcol:
                break
            iLabel = labelFmt % (ic + 1)
            if len(iLabel) < llabel:
                iLabel = ' ' * (llabel - len(iLabel)) + iLabel
            line.append(iLabel + scol[ic] + ' ' * (maxisize - len(scol[ic])))
        lines.append(centering + itemSep.join(line))
    return EOL.join(lines)


def runConsole2(cmd):
    if not isinstance(cmd, list):
        return
    p = Popen(cmd)
    while p.returncode is None:
        p.poll()
        time.sleep(0.1)
    sys.stdout.flush()


def runConsole(cmd):
    if not isinstance(cmd, list):
        return
    p = Popen(cmd, stdout=PIPE, stderr=STDOUT)
    while True:
        line = p.stdout.readline()
        print(line),
        if not line or p.returncode is not None:
            break
        time.sleep(0.1)
    sys.stdout.flush()


def logAlways(logger, msg):
    if not isinstance(logger, logging.Logger):
        print(msg)
        return
    bkLevel = logging.NOTSET
    if not logger.isEnabledFor(logging.INFO):
        bkLevel = logger.level
        logger.setLevel(logging.INFO)
    logger.info(msg)
    if bkLevel > logging.NOTSET:
        logger.setLevel(bkLevel)
    return


def getRepoPawgName(jobdic):
    """
    Retrieve PAWG directory name for statistics storage. Tries EOS then uses web API.
    
    Args:
        jobdic (dict): Dictionary w/ all data related to currently processed generator statistics table.
        Must contain 'simName' of form SimNN to be able to identify major Simulation stack version/repository.
     
    Returns:
     PAWG directory name where genstats are stored as JSONs or None if error.
    """
    global wgName
    if wgName is None:
        if 'simName' not in jobdic:
            mlog.debug('Missing \'simName\' key:\n%s' % str(jobdic))
            return None
        hasEOS = True
        if not os.path.exists(simStatRepoPath) or not os.path.isdir(simStatRepoPath):
            mlog.warning('Could not access statistics repo on EOS: %s. Trying HTTP API...' % simStatRepoPath)
            hasEOS = False
        if not cacheWgName:
            wgName = None
        simStatPath = os.path.join(simStatRepoPath, simStatFormat % jobdic['simName'])
        if cacheWgName:
            mlog.warning('Will generated statistics tables for one Physics WG only.')
        if hasEOS:
            if not os.path.exists(simStatPath) or not os.path.isdir(simStatPath):
                mlog.debug('Could not find/access path %s.' % simStatPath)
                return None
            wgDirs = [dn for dn in os.listdir(simStatPath) if
                      os.path.isdir(os.path.join(simStatPath, dn)) and dn.lower().endswith('-wg')]
        else:
            wgDirs = httpGetWGNames(simver=jobdic['simName'])
        if len(wgDirs) == 0:
            mlog.debug('Could not locate any PAWG directories...')
            return None
        msg = 'Select your WG in order to get lastest HTML file for %s :\n\n' % jobdic['APPCONFIG_file']
        msg += formatCollPrint(wgDirs)
        selDir = None
        try:
            while True:
                mlog.warning(msg + '\n')
                mlog.disabled = True
                sys.stdout.write('Please, enter index corresponding to prodID #%d WG '
                                 '(non-numeric input -> ignore & continue): ' % jobdic['prodID'])
                sys.stdout.flush()
                ret = sys.stdin.readline()
                sys.stdout.write(ret)
                sys.stdout.flush()
                mlog.disabled = False
                ret = ret.strip()
                if len(ret) == 0 or not ret.isdigit():
                    break
                ret = ret.lstrip('0')
                if len(ret) == 0:
                    continue
                ii = int(ret) - 1
                if ii < len(wgDirs):
                    selDir = wgDirs[ii]
                    break
        except ValueError as exx:
            mlog.error('Exception occured: %s' % str(exx), exc_info=True)
            selDir = None
        if selDir is None:
            return None
        if cacheWgName:
            wgName = selDir
    else:
        if cacheWgName:
            mlog.debug('Generating statistics table for PAWG (cached) %s...' % wgName)
        selDir = wgName
    return selDir


def get_WG_Html_File(jobdic, wkpath=os.getcwd()):
    """
    Interactively retrieve path of latest HTML for given MC conditions. Store WG name in global wgName variable.
    Value in global may change between calls if module not forced to use same WG name repeatedly.
    
    Args:
        jobdic (dict):
        wkpath(str): Path where HTML file should be saved if present
    Returns:

    """
    ret = None
    if 'simName' not in jobdic:
        return ret
    simStatPath = os.path.join(simStatRepoPath, simStatFormat % jobdic['simName'])
    localFn = ('%(wgName)s_' + statTableFnFmt) % jobdic
    localPath = os.path.join(wkpath, localFn)
    if os.path.isfile(localPath):
        mlog.info('Using local newer version of file %s' % localFn)
        return localPath
    # TODO: Exception catching to be implemented
    if not os.path.exists(simStatPath) or not os.path.isdir(simStatPath):
        hurl = webRootURL + '/' + \
               os.path.join(simStatFormat % jobdic['simName'], jobdic['wgName'], localFn)
        r = rq.get(hurl)
        if not r.ok:
            mlog.debug('HTTP request failed: %s' % hurl)
        else:
            with open(localPath, 'w', encoding='utf-8') as tfp:
                tfp.write(r.content.decode('utf-8'))
                tfp.flush()
                tfp.close()
            ret = localPath
    else:
        fpath = os.path.join(simStatPath, jobdic['wgName'], statTableFnFmt % jobdic)
        if not os.path.exists(fpath):
            mlog.debug('Not found on EOS: %s' % fpath)
        else:
            mlog.info('Copying locally file from \'%s\' ...' % fpath)
            shutil.copy(fpath, localPath)
            time.sleep(1.1)
            ret = localPath
    return ret


# ========================================================================
# Functions relative to smart rounding
# ========================================================================


def nb_digit(val, nb_sig_digit=2):
    """ compute the number of digit after the point that needs to be
    considered to get 2 significant digits.
    """
    val = abs(val)
    ndig = nb_sig_digit - 3
    # if "%3.2f" % val == "0.00":
    if val < 1.e-18:
        return 2
    k = 1
    while True:
        ndig += 1
        if val / 10 >= 1.:
            break
        val *= 10.
        k += 1
        if k > 20:
            mlog.error('Failed to compute signification digits for %g. Aborting...' % val)
            ndig = -999
            break
    return ndig


def nb_digit_PDG(val):
    """ compute the number of digit after the point that needs to be
    considered according to PDG rules.
    """
    n_digit = nb_digit(val, 3)
    # print n_digit
    val_3dig = int(val * (pow(10, n_digit))) / float(pow(10, n_digit))
    # print val_3dig
    val_3dig = val_3dig * pow(10, n_digit - 3) % 1
    # print val_3dig
    if 0.100 < val_3dig <= 0.354:
        return 2
    elif 0.354 < val_3dig <= 0.949:
        return 1
    else:
        return 2


def get_significant_digit(value, err_pos, err_neg, method='2digit'):
    """ return the value and errors and the number of digits
        method can be '2digit' : take 2 significant digits
                      'PDG'    : along the PDG recommendations
    """
    # print '+++', value, err_pos, err_neg
    digit_val, digit_pos, digit_neg = -999, -999, -999
    if method == '2digit':
        digit_val = nb_digit(value)
        digit_pos = nb_digit(err_pos)
        digit_neg = nb_digit(err_neg)
    if method == 'PDG':
        digit_val = nb_digit_PDG(value)
        digit_pos = nb_digit_PDG(err_pos)
        digit_neg = nb_digit_PDG(err_neg)
    nb_digit_all = max(digit_val, digit_pos, digit_neg)
    if any([x == -999 for x in (digit_val, digit_pos, digit_neg)]):
        mlog.error('Error determining significant digits for: %g + %g - %g' % (value, err_pos, err_neg))
        return None
    value = round(value, nb_digit_all)
    err_pos = round(err_pos, nb_digit_all)
    err_neg = round(err_neg, nb_digit_all)
    return value, err_pos, err_neg, nb_digit_all


def signif_digit(value, err, method='2digit'):
    """wrapper to 'get_significant_digit' in case of symetrical uncertainty.
    """
    return get_significant_digit(value, err, err, method=method)


def format_to_unit(val, err, unitlst=(), prec=2):
    ui = 0
    rounded = signif_digit(val, err)
    if rounded is None:
        mlog.error('Failed signif_digit for %g +/- %g' % (val, err))
        return None
    value = val
    error = err
    valtxt = '%.*f' % (rounded[3], rounded[0])
    while valtxt.startswith('0.'):
        nzeros = len(valtxt) - 2 - len(valtxt[2:].lstrip('0'))
        if nzeros < prec:  # uniformity in units, number of zeroes after decimal point
            break
        ui += 1
        if 0 < len(unitlst) == ui:
            ui -= 1
            break
        value = value * 1000.
        error = error * 1000.
        rounded = signif_digit(value, error)
        valtxt = '%.*f' % (rounded[3], rounded[0])
    if len(unitlst) == 0:
        unit = ui
    else:
        unit = unitlst[ui]
    return '%.*f' % (rounded[3], rounded[0]), '%.*f' % (rounded[3], rounded[1]), unit


def format_statval(cnt, includeEOL=True):
    """- format numbers in stat quantity for web output; take stat type into account
    - at debug logging level print as text as well """
    debug = mlog.isEnabledFor(logging.DEBUG)
    xs_units = [' [%s]' % xu for xu in xsUnits]
    msg = ''
    ret = None
    rounded_new = ('', '')
    # #mlog.debug('Formatting %s.' % (str(cnt)))
    name = cnt['descr']
    if cnt['type'] == 'counter':
        ui = 0
        unit = ''
        isXScnt = name.lower().find('cross-section') > -1
        rvalue = cnt['value']
        rerror = cnt['error']
        value = float(rvalue)
        error = float(rerror)
        if (isinstance(rvalue, int) and isinstance(rerror, int) and rvalue == 0 and rerror == 0) or \
                (value < 1.e-18 and error < 1.e-18):
            tval = '<td>0</td>'
            terr = '<td>&plusmn; 0</td>'
            rounded_new = ('0.00', '0.00')
        else:
            rounded = signif_digit(value, error)
            if rounded is None:
                mlog.error('Failed to format counter: %s' % str(cnt))
                return ''
            valtxt = '%.*f' % (rounded[3], rounded[0])
            old_rounded = None
            while valtxt.startswith('0.'):
                nzeros = len(valtxt) - 2 - len(valtxt[2:].lstrip('0'))
                if nzeros < 2:  # uniformity in units
                    break
                ui += 1
                if isXScnt and ui == len(xs_units):
                    ui -= 1
                    break
                if not isXScnt and ui > 5:
                    ui -= 1
                    break
                value = value * 1000.
                error = error * 1000.
                rounded = signif_digit(value, error)
                if rounded is None:
                    mlog.error('Failed to format %s level %d.' % (valtxt, ui))
                    rounded = old_rounded
                    if rounded is None:
                        return ''
                old_rounded = rounded
                valtxt = '%.*f' % (rounded[3], rounded[0])
            rounded_new = ('%.*f' % (rounded[3], rounded[0]), '%.*f' % (rounded[3], rounded[1]))
            tval = '<td>%s</td>' % rounded_new[0]
            terr = '<td>&plusmn; %s</td>' % rounded_new[1]
        if not isXScnt and ui > 0:
            unit = ' x 10<sup>-%d</sup>' % (3*ui)
        if isXScnt:
            unit = xs_units[ui]
        if debug:
            msg = '%s%s: %s +- %s' % (name.strip(), unit, rounded_new[0], rounded_new[1])
        thead = '<td><font size="2">%s%s</font></td>' % (name.strip(), unit)
        if includeEOL:
            thead += '\n'
            tval += '\n'
            terr += '\n'
        ret = (thead, tval, terr)
    elif cnt['type'] == 'fraction':
        fval = float(cnt['numer']) / float(cnt['denom'])
        nbd = nb_digit(fval, 4)  # require 4 non-zero last digits
        ret = '%s: %i / %i = %.*f' % (name.strip(), cnt['numer'], cnt['denom'], nbd, round(fval, nbd))
    elif cnt['type'] == 'sig_brfrac':  # never output in Sim09!
        ret = '<font color="#0000FF">%s:</font> %s' % (name.strip(), str(cnt['value']))
    if len(msg) > 0:
        mlog.debug(msg)
    return ret


def format_text_entry(string_, value, error, method='2digit'):
    """
    Format a value and its simmetrical error for final output by default with 2 significant digits.
    
    Args:
        string_ (str):  String to concatenate value representation to
        value (float):  The value to represent w/ same precision as its error
        error (float):  Simmetrical error to represent at same precision as the value
        method (str):   The level of precision to consider (2digit or PDG rules)

    Returns:
        The string to which the new value and its error string representation is appended.
    """
    if not string_.endswith(':'):
        string_ += ':'
    if isinstance(value, int) and isinstance(error, int) and value == 0 and error == 0:
        return '%s 0 +- 0' % string_
    rounded = signif_digit(value, error)
    rounded_new = '%.*f +- %.*f' % (rounded[3], rounded[0], rounded[3], rounded[1])
    return string_ + rounded_new


def test_fraction(num, dnm):
    # if any of the operands is NaN
    if (isinstance(num, six.string_types) and num.lower().find('nan') > -1) or \
            (isinstance(dnm, six.string_types) and dnm.lower().find('nan') > -1):
        return 0, 0
    if not ((isinstance(num, int) or isinstance(num, float)) and
            (isinstance(dnm, int) or isinstance(dnm, float))):
        mlog.error("Bad data type. Arguments can be only int or float.")
        return None
    if (isinstance(dnm, int) and dnm == 0) or \
            (isinstance(dnm, float) and abs(dnm) < 1.e-8):
        return 0, 0
    if isinstance(num, float):
        fnum = num
    else:
        fnum = float(num)
    if isinstance(dnm, float):
        fdnom = dnm
    else:
        fdnom = float(dnm)
    return fnum, fdnom


def calc_fraction_ints(counter, csum):
    r = test_fraction(counter, csum)
    if r is None:
        return r
    if isinstance(r[0], int) and r[0] == 0:
        return 0, 0
    (fcnt, fsum) = r
    # error is Sqrt( counter * (sum - counter) / sum**3 )
    return fcnt / fsum, math.sqrt(fcnt * (fsum - fcnt) / fsum / fsum / fsum)


def calc_ratio_ints(numer, denom):
    r = test_fraction(numer, denom)
    if r is None:
        return r
    (fnum, fdnom) = r
    if isinstance(fdnom, int) and fdnom == 0:
        val, err = 0, 0
    else:
        val = fnum / fdnom
        err = (fnum / fdnom) * math.sqrt((1. / fnum) + (1. / fdnom))
    return val, err


# ========================================================================
# Operations for the generation process
# ========================================================================


def operations(result, bGetDict=False):
    # It takes a dictionary with the counters and makes the operations
    global jsonDict
    for k in jsonDict['CounterTableHeads']:
        jsonDict[k] = list()
    assert isinstance(result, dict)
    tdict = jsonDict['Interaction Counters']
    # Computation of Pile-Up interactions
    if 'N2' in result:
        if 'L1' in result:
            (val, err) = calc_ratio_ints(result['N2'], result['L1'])
            tdict.append({'descr': 'Mean Number of generated Pile-Up interactions', 'value': val,
                          'error': err, 'type': 'counter'})
        if 'N1' in result:
            (val, err) = calc_ratio_ints(result['N2'], result['N1'])
            tdict.append({'descr': 'Mean Number of non-empty generated Pile-Up interactions', 'value': val,
                          'error': err, 'type': 'counter'})
    if 'N3' in result and 'N4' in result:
        (val, err) = calc_ratio_ints(result['N4'], result['N3'])
        tdict.append({'descr': 'Mean Number of accepted Pile-Up interactions', 'value': val,
                      'error': err, 'type': 'counter'})
    # Computation of cut efficiencies
    if 'I2' in result and 'I1' in result:
        (val, err) = calc_fraction_ints(result['I2'], result['I1'])
        jsonDict['Hadron Counters'].append({'descr': 'Generator level cut efficiency', 'value': val,
                                            'error': err, 'type': 'counter'})
    if 'N20' in result and 'N19' in result:
        (val, err) = calc_fraction_ints(result['N20'], result['N19'])
        tdict.append({'descr': 'Full event cut efficiency', 'value': val, 'error': err, 'type': 'counter'})
    
    # Computation of cross-sections
    if 'C1N2' in result and 'N2' in result and result['C1N2'] > 0.:
        C1 = result['C1N2'] / result['N2']
        if 'N5' in result:
            fr = test_fraction(C1, result['N5'])
            if fr is None or (isinstance(fr[1], int) and fr[1] == 0):
                mlog.error('Counter \'N5\' (generated interaction w/ >=1b) is NaN or 0. Defaulting xsections to 0.0.')
                err = 0.0
            else:
                err = C1 / float(math.sqrt(result['N5']))
            tdict.append({'descr': 'total cross-section', 'value': C1, 'error': err, 'type': 'counter'})
            (val, err) = calc_fraction_ints(result['N5'], result['N2'])
            tdict.append({'descr': 'b cross-section', 'value': val * C1, 'error': err * C1, 'type': 'counter'})
        if 'N6' in result:
            (val, err) = calc_fraction_ints(result['N6'], result['N2'])
            tdict.append({'descr': 'Double b cross-section', 'value': val * C1, 'error': err * C1, 'type': 'counter'})
        if 'N7' in result:
            (val, err) = calc_fraction_ints(result['N7'], result['N2'])
            tdict.append({'descr': 'Prompt B cross-section', 'value': val * C1, 'error': err * C1, 'type': 'counter'})
        if 'N8' in result:
            (val, err) = calc_fraction_ints(result['N8'], result['N2'])
            tdict.append({'descr': 'c cross-section', 'value': val * C1, 'error': err * C1, 'type': 'counter'})
        if 'N9' in result:
            (val, err) = calc_fraction_ints(result['N9'], result['N2'])
            tdict.append({'descr': 'Double c cross-section', 'value': val * C1, 'error': err * C1, 'type': 'counter'})
        if 'N10' in result:
            (val, err) = calc_fraction_ints(result['N10'], result['N2'])
            tdict.append({'descr': 'Prompt D cross-section', 'value': val * C1, 'error': err * C1, 'type': 'counter'})
        if 'N11' in result:
            (frac, errfrac) = calc_fraction_ints(result['N11'], result['N2'])
            tdict.append(
                {'descr': 'b and c cross-section', 'value': frac * C1, 'error': errfrac * C1, 'type': 'counter'})
            del frac, errfrac
        # Computation of B Hadron fraction
        if all([x in result for x in ['I3', 'I5', 'I7', 'I9']]):
            fsum = result['I3'] + result['I5'] + result['I7'] + result['I9']
            (frac, errfrac) = calc_fraction_ints(result['I3'], fsum)
            tdict.append({'descr': 'Fraction of generated B0', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I5'], fsum)
            tdict.append({'descr': 'Fraction of generated B+', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I7'], fsum)
            tdict.append({'descr': 'Fraction of generated Bs0', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I9'], fsum)
            tdict.append({'descr': 'Fraction of generated b-Baryon', 'value': frac,
                          'error': errfrac, 'type': 'counter'})
            del frac, errfrac
        # beginning hadron counters
        tdict = jsonDict['Hadron Counters']
        if all([x in result for x in ['I14', 'I16', 'I18', 'I20']]):
            isum = result['I14'] + result['I16'] + result['I18'] + result['I20']
            (frac, errfrac) = calc_fraction_ints(result['I14'], isum)
            tdict.append({'descr': 'Fraction of accepted B0', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I16'], isum)
            tdict.append({'descr': 'Fraction of accepted B+', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I18'], isum)
            tdict.append({'descr': 'Fraction of accepted Bs0', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I20'], isum)
            tdict.append({'descr': 'Fraction of accepted b-Baryon', 'value': frac, 'error': errfrac, 'type': 'counter'})
            del frac, errfrac
        # Computation of D Hadron fraction
        if all([x in result for x in ['I25', 'I27', 'I29', 'I31']]):
            fsum = result['I25'] + result['I27'] + result['I29'] + result['I31']
            (frac, errfrac) = calc_fraction_ints(result['I25'], fsum)
            tdict.append({'descr': 'Fraction of generated D0', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I27'], fsum)
            tdict.append({'descr': 'Fraction of generated D+', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I29'], fsum)
            tdict.append({'descr': 'Fraction of generated Ds+', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I31'], fsum)
            tdict.append({'descr': 'Fraction of generated c-Baryon', 'value': frac,
                          'error': errfrac, 'type': 'counter'})
            del frac, errfrac
        if all([x in result for x in ['I34', 'I36', 'I38', 'I40']]):
            fsum = result['I34'] + result['I36'] + result['I38'] + result['I40']
            (frac, errfrac) = calc_fraction_ints(result['I34'], fsum)
            tdict.append({'descr': 'Fraction of accepted D0', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I36'], fsum)
            tdict.append({'descr': 'Fraction of accepted D+', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I38'], fsum)
            tdict.append({'descr': 'Fraction of accepted Ds+', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I40'], fsum)
            tdict.append({'descr': 'Fraction of accepted c-Baryon', 'value': frac,
                          'error': errfrac, 'type': 'counter'})
            del frac, errfrac
        # Computation of B** fraction
        if all([x in result for x in ['I43', 'I44', 'I45']]):
            csum = result['I43'] + result['I44'] + result['I45']
            (frac, errfrac) = calc_fraction_ints(result['I43'], csum)
            tdict.append({'descr': 'Fraction of generated B', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I44'], csum)
            tdict.append({'descr': 'Fraction of generated B*', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I45'], csum)
            tdict.append({'descr': 'Fraction of generated B**', 'value': frac, 'error': errfrac, 'type': 'counter'})
            del frac, errfrac
        # Computation of accepted B fractions
        if all([x in result for x in ['I46', 'I47', 'I48']]):
            csum = result['I46'] + result['I47'] + result['I48']
            (frac, errfrac) = calc_fraction_ints(result['I46'], csum)
            tdict.append({'descr': 'Fraction of accepted B', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I47'], csum)
            tdict.append({'descr': 'Fraction of accepted B*', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I48'], csum)
            tdict.append({'descr': 'Fraction of accepted B**', 'value': frac, 'error': errfrac, 'type': 'counter'})
            del frac, errfrac
        # Computation of D** fraction
        if all([x in result for x in ['I49', 'I50', 'I51']]):
            csum = result['I49'] + result['I50'] + result['I51']
            (frac, errfrac) = calc_fraction_ints(result['I49'], csum)
            tdict.append({'descr': 'Fraction of generated D', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I50'], csum)
            tdict.append({'descr': 'Fraction of generated D*', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I51'], csum)
            tdict.append({'descr': 'Fraction of generated D**', 'value': frac, 'error': errfrac, 'type': 'counter'})
            del frac, errfrac
        # Computation of accepted D fractions
        if all([x in result for x in ['I52', 'I53', 'I54']]):
            csum = result['I52'] + result['I53'] + result['I54']
            (frac, errfrac) = calc_fraction_ints(result['I52'], csum)
            tdict.append({'descr': 'Fraction of accepted D', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I53'], csum)
            tdict.append({'descr': 'Fraction of accepted D*', 'value': frac, 'error': errfrac, 'type': 'counter'})
            (frac, errfrac) = calc_fraction_ints(result['I54'], csum)
            tdict.append({'descr': 'Fraction of accepted D**', 'value': frac, 'error': errfrac, 'type': 'counter'})
            del frac, errfrac
        # calculate signal counters
        tdict = jsonDict['Signal Counters']
        # include entry for signal XS if present
        if 'C2' in result and 'N5' in result:
            err = result['C2']/math.sqrt(result['N5'])
            tdict.append({'descr': 'signal cross-section', 'value': result['C2'], 'error': err, 'type': 'counter'})
        # Computation of signal level cut efficiencies
        if 'S1' in result and 'S2' in result:
            (val, err) = calc_fraction_ints(result['S2'], result['S1'])
            tdict.append({'descr': 'particle cut efficiency', 'value': val, 'error': err, 'type': 'counter'})
        if 'S3' in result and 'S4' in result:
            (val, err) = calc_fraction_ints(result['S4'], result['S3'])
            tdict.append({'descr': 'anti-particle cut efficiency', 'value': val, 'error': err, 'type': 'counter'})
        # (New) List fraction of signal and anti-signal particles in sample
        if all([x in result for x in ['signal', 'anti-signal', 'I90', 'I99']]):
            csum = result['I90'] + result['I99']
            (val, err) = calc_fraction_ints(result['I90'], csum)
            tdict.append({'descr': 'signal %s in sample' % result['signal'], 'value': val,
                          'error': err, 'type': 'counter'})
            (val, err) = calc_fraction_ints(result['I99'], csum)
            tdict.append({'descr': 'signal %s in sample' % result['anti-signal'], 'value': val,
                          'error': err, 'type': 'counter'})
    calcdict = dict([(d['descr'], (d['value'], d['error'])) for d in
                     jsonDict['Interaction Counters'] + jsonDict['Hadron Counters'] + jsonDict['Signal Counters']])
    del val, err
    if bGetDict:
        return calcdict
    # #mlog.debug('Calculations dictionary:\n%s' % (str(#calcdict)))
    return '\n'.join([format_text_entry(dit[0], *dit[1]) for dit in calcdict.items()])


def BuildStat(stat_dic, job_dic, nb_jobs, isSamePhwg=False):
    """
    Entry point for generating statistics table for new production(s).
    Args:
        stat_dic ():
        job_dic ():
        nb_jobs ():
        isSamePhwg (bool): flag to specify that tables must be generated for one PAWG only

    Returns:
        return string KO or OK depending on how data interaction left the code
    """
    global cacheWgName, jsonDict
    ## jsonDict = {'CounterTableHeads': ('Counters Interaction', 'Hadron Counters', 'Signal Counters'), }
    jsonDict = {'CounterTableHeads': ('Interaction Counters', 'Hadron Counters', 'Signal Counters'), }
    cacheWgName = isSamePhwg
    # modify stat_dic according to values in job_dic
    eventtype = job_dic['eventType']
    gaussversion = job_dic['Gauss_version']
    # change dictionary values to take into account bugs in older Gauss versions
    if gaussversion in ['v46r4', 'v46r3', 'v46r2p1', 'v46r1', 'v46r0p1',
                        'v45r4', 'v45r3', 'v45r2', 'v45r1', 'v45r0']:
        if eventtype[0:2] in ['15', '18', '21', '22', '23', '24', '25', '26', '27', '28',
                              '31', '32', '33', '34', '35', '36']:
            stat_dic['I2'] = stat_dic['I2'] - stat_dic['N21']
    # these are all very old version, but they are still available on CVMFS!
    return gen_store_results_dict2(stat_dic, job_dic, nb_jobs)


def upgradeGenReport2(grep, **additems):
    """
    Temporary function to update format of report dict if necessary. Does not check working group name!
    """
    grep['CounterTableHeads'] = ('Interaction Counters', 'Hadron Counters', 'Signal Counters')
    if 'HadronCounters' in grep:
        grep['Hadron Counters'] = grep.pop('HadronCounters')
    if 'SignalCounters' in grep:
        grep['Signal Counters'] = grep.pop('SignalCounters')
    if 'InteractionCounters' in grep:
        grep['Interaction Counters'] = grep.pop('InteractionCounters')
    if 'Counters Interaction' in grep:
        grep['Interaction Counters'] = grep.pop('Counters Interaction')
    if 'infoProd' in grep and isinstance(grep['infoProd'], dict):
        grep.update(grep['infoProd'])
        del grep['infoProd']
    if grep['evtTypeDesc'] == 'dummy':
        etdescr = readEvtDesc4(grep)
        if etdescr is not None:
            grep['evtTypeDesc'] = etdescr[1]
    # added due to API excluding '-WG' suffix
    if 'wgName' in grep and not grep['wgName'].lower().endswith('-wg'):
        grep['wgName'] += '-WG'
    if len(additems.items()) > 0:
        grep.update(additems)


def diffCounters(nc, oc):
    """ Return a string with message regarding differences between two counter 'objects'."""
    if nc['type'] != oc['type']:
        return '   - Type has changed for statistics "%s": %s (new) vs. %s (old)' % \
               (nc['descr'], nc['type'], oc['type'])
    msg = ''
    # tuples: (value, err_pos, err_neg, nb_digit_all)
    if nc['type'] == 'counter':
        nmt = signif_digit(nc['value'], nc['error'])
        omt = signif_digit(oc['value'], oc['error'])
        errprec = max(nmt[3], omt[3])
        if nmt[3] != omt[3] or abs(nmt[1] - omt[1]) > math.pow(10., -errprec) or \
                abs(nmt[0] - omt[0]) > min(nmt[1], omt[1]):
            Zval = abs(nmt[0] - omt[0]) / math.sqrt(nmt[1] * nmt[1] + omt[1] * omt[1])
            msg = '%.*f +/- %.*f (new) vs. %.*f +/- %.*f (old) Z=%3.2f' % \
                  (nmt[3], nmt[0], nmt[3], nmt[1], omt[3], omt[0], omt[3], omt[1], Zval)
    elif nc['type'] == 'fraction':
        nrv = float(nc['numer']) / float(nc['denom'])
        orv = float(oc['numer']) / float(oc['denom'])
        if abs(nrv - orv)/orv > 1.e-3:   # diff > 0.1%
            pdelta = 100. * abs(nrv - orv)/orv
            msg = '%.1f/%.1f = %.6f (new) vs. %.1f/%.1f = %.6f (old) Diff %3.2f%%' % \
                  (nc['numer'], nc['denom'], nrv, oc['numer'], oc['denom'], orv, pdelta)
    if len(msg) > 0:
        msg = '   - Change in "%s": %s' % (nc['descr'], msg)
    return msg


def diffCounterLists(cln, clo, joinChar='\n', countdiffs=False):
    """
    Returns differences between two lists of counters with same descriptions.
    If requested count differences and return as second tuple element.
    Parameters:
        cln (list): new counter list
        clo (list): older counter list
        joinChar (str,None): string to be used in joining results into one big string. If None just return the list.
        countdiffs (bool): when True return the number of differences  as second result tuple element.
    """
    replns = []
    ndfs = 0
    # created dicts using `descr` as keys
    ldn = dict([(x['descr'], x) for x in cln])
    ldo = dict([(x['descr'], x) for x in clo])
    for (nk, nv) in ldo.items():
        if nk not in ldn:
            replns.append('   - %(type)s "%(descr)s" missing in new statistics' % nv)
            ndfs += 1
    for (nk, nv) in ldn.items():
        if nk not in ldo:
            replns.append('   - %(type)s "%(descr)s" missing in older statistics' % nv)
            ndfs += 1
            continue
        if nv['type'] != ldo[nk]['type']:
            replns.append('   - Type has changed for statistics "%(descr)s": %(type)s (new)' % nv +
                          ' vs. %(type)s (old)' % ldo[nk])
            ndfs += 1
            continue
        tt = diffCounters(nv, ldo[nk])
        if len(tt):
            replns.append(tt)
            ndfs += 1
    ret = replns
    if joinChar is not None:
        ret = joinChar.join(replns)
    if countdiffs:
        ret = ret, ndfs
    return ret


def mkGenCmpReport(nrep, orep, savePath=os.getcwd(), nosave=False, countdiffs=False):
    """
    Returns difference between two dictionaries or saves it to disk.
    If error is encountered return just message and error state.
    When nosave is True does not save the report to disk.
    If countdiffs, a score indicating the number of non-trivial ones is returned.
    """
    if orep['evtType'] != nrep['evtType']:
        msg = 'Will not create diff-report for different event type productions (%s vs. %s).' % \
              (nrep['evtType'], orep['evtType'])
        return msg, True
    if 'wgName' in orep and 'wgName' in nrep and orep['wgName'] != nrep['wgName']:
        msg = 'Will not create diff-report for productions requested by different PAWG (%s vs. %s).' % \
              (nrep['wgName'], orep['wgName'])
        return msg, True
    if orep['APPCONFIG_file'] != nrep['APPCONFIG_file']:
        msg = 'Cannot create diff-report for different simulation conditions (%s vs. %s).' % \
              (nrep['APPCONFIG_file'], orep['APPCONFIG_file'])
        return msg, True
    nn = 0
    repFileName = 'GenCounterDiff-P%d-v-P%d.txt' % (nrep['prodID'], orep['prodID'])
    head = 'Comparing generator statistics for prod. #%d (new) and prod. #%d (old)' % \
           (nrep['prodID'], orep['prodID'])
    if 'wgName' in nrep:
        head += ' for %s PAWG' % nrep['wgName'].split('-')[0]
    head += ' :'
    replines = [head, ' - for MC config. %(APPCONFIG_file)s, event type %(evtType)s (%(evtTypeDesc)s)' % orep,
                      ' - generated on %s (new) vs. %s (old)' % (nrep['genTimeStamp'], orep['genTimeStamp'])]
    if nrep['script_version'] != orep['script_version']:
        nn += 1
        replines.append(' - generated by GaussStat script version %s (new) vs. %s (old)' %
                        (nrep['script_version'], orep['script_version']))
    else:
        replines.append(' - generated by GaussStat version %(script_version)s' % orep)
    if nrep['nb_jobs'] != orep['nb_jobs']:
        nn += 1
        replines.append(' - from %d (new) vs. %d (old) XML generator logs' %
                        (nrep['nb_jobs'], orep['nb_jobs']))
    else:
        replines.append(' - from %(nb_jobs)d XML generator logs' % orep)
    if 'simVersion' not in orep:
        orep['simVersion'] = 'missing'
    if nrep['simVersion'] != orep['simVersion']:
        nn += 1
        replines.append(' - using Sim version %s (new) vs. %s (old)' %
                        (nrep['simVersion'], orep['simVersion']))
    else:
        replines.append(' - generated w/ Sim version %(simVersion)s' % orep)
    if nrep['Gauss_version'] != orep['Gauss_version']:
        nn += 1
        replines.append(' - Gauss version: %s (new) vs. %s (old)' %
                        (nrep['Gauss_version'], orep['Gauss_version']))
    else:
        replines.append(' - Gauss version: %s' % orep['Gauss_version'])
    if nrep['DecFiles_version'] != orep['DecFiles_version']:
        nn += 1
        replines.append(' - DecFiles version: %s (new) vs. %s (old)' %
                        (nrep['DecFiles_version'], orep['DecFiles_version']))
    else:
        replines.append(' - DecFiles version: %s' % orep['DecFiles_version'])
    if nrep['APPCONFIG_version'] != orep['APPCONFIG_version']:
        nn += 1
        replines.append(' - APPCONFIG version: %s (new) vs. %s (old)' %
                        (nrep['APPCONFIG_version'], orep['APPCONFIG_version']))
    else:
        replines.append(' - APPCONFIG version: %s' % orep['APPCONFIG_version'])
    if nrep['DDDB'] != orep['DDDB']:
        nn += 1
        replines.append(' - DDDB tag: %s (new) vs. %s (old)' %
                        (nrep['DDDB'], orep['DDDB']))
    else:
        replines.append(' - DDDB tag: %s' % orep['DDDB'])
    if nrep['SIMCOND'] != orep['SIMCOND']:
        nn += 1
        replines.append(' - SIMCOND tag: %s (new) vs. %s (old)' %
                        (nrep['SIMCOND'], orep['SIMCOND']))
    else:
        replines.append(' - SIMCOND tag: %s' % orep['SIMCOND'])
    ckey = 'Interaction Counters'
    if ckey in nrep:
        m = len(nrep[ckey])
        n = len(orep[ckey])
        replines.append(' - Interaction counters (%d - new) vs. (%d - old):' % (m, n))
        rr, nd = diffCounterLists(nrep[ckey], orep[ckey], joinChar=None, countdiffs=True)
        replines += rr
        nn += nd
    ckey = 'Hadron Counters'
    if ckey in nrep:
        replines.append(' - Hadron counters (%d - new) vs. (%d - old):' % (len(nrep[ckey]), len(orep[ckey])))
        rr, nd = diffCounterLists(nrep[ckey], orep[ckey], joinChar=None, countdiffs=True)
        replines += rr
        nn += nd
    ckey = 'Signal Counters'
    if ckey in nrep:
        replines.append(' - Signal counters (%d - new) vs. (%d - old):' % (len(nrep[ckey]), len(orep[ckey])))
        rr, nd = diffCounterLists(nrep[ckey], orep[ckey], joinChar=None, countdiffs=True)
        replines += rr
        nn += nd
    ckey = 'globStat'
    if ckey in nrep:
        replines.append(' - Other counters (%d - new) vs. (%d - old):' % (len(nrep[ckey]), len(orep[ckey])))
        rr, nd = diffCounterLists(nrep[ckey], orep[ckey], joinChar=None, countdiffs=True)
        replines += rr
        nn += nd
    if not nosave:
        fp = open(os.path.join(savePath, repFileName), 'w')
        fp.write("\n".join(replines))
        fp.flush()
        fp.close()
    if countdiffs:
        return replines, nn
    return replines


def pickTagContents(haystack, smrk, emrk, tracing=False, strip=True):
    """Extracts text within provided HTML tag fragments. Start tag should not contain closing >.
    Text is by default stripped of blank spaces.
    """
    if len(haystack) < len(emrk) + len(smrk):
        return None
    if len(smrk) == 0 and len(emrk) == 0:
        return haystack, 0
    if len(smrk) == 0:
        p = haystack.find(emrk)
        if p > -1:
            if tracing:
                mlog.debug('Match start of line: %s' % (' :: '.join([haystack[0:p], emrk])))
            return haystack[0:p], p + len(emrk)
    p = haystack.find(smrk)
    m = haystack[p + len(smrk):].find('>')
    if p == -1 or m == -1:
        mlog.debug('Cannot find start of tag %s:\n%s' % (smrk, haystack))
        return None
    m += p + len(smrk)
    if len(emrk) == 0:
        if tracing:
            mlog.debug('Match to end of line: %s' % (' :: '.join([smrk, '>', haystack[m + 1:]])))
        return haystack[m + 1:], len(haystack)
    if m + len(emrk) > len(haystack):
        mlog.debug('EOL before end tag %s.' % emrk)
        return None
    p = haystack[m + 1:].find(emrk)
    if p > -1:
        p = p + m + 1
        if tracing:
            mlog.debug(' :: '.join([smrk, '>', haystack[m + 1:p], emrk]))
        ret = haystack[m + 1:p]
        if strip:
            ret = ret.strip()
        return ret, p + len(emrk)
    mlog.debug('Failed to find end of tag %s:\n%s' % (emrk, haystack))
    return None


def readStatValMultiplier(desc):
    ff = 1.
    sdesc = desc
    xsunits = {'mub': 1.e-3, 'nb': 1.e-6, 'pb': 1.e-9, 'fb': 1.e-12}
    if desc.find('cross-section') > -1:
        mm = re.match(r'(.*?) \[([munfp]b)]$', desc)
        if mm is not None:
            sdesc = mm.group(1)
            unit = mm.group(2)
            if unit in xsunits:
                ff = xsunits[unit]
    else:
        mm = re.match(r'(.*?) x 10<sup>-([0-9]+)</sup>$', desc)
        if mm is not None:
            sdesc = mm.group(1)
            funit = float(mm.group(2))
            ff = math.pow(10, -funit)
    return ff, sdesc


def parseHtmlStats(htmlCode):
    lns = list()
    if isinstance(htmlCode, list) or isinstance(htmlCode, tuple):
        lns = htmlCode
    if isinstance(htmlCode, six.string_types):
        lns = htmlCode.split('\n')
    assert len(lns) > 0
    enableTrace = False
    # enableTrace = True
    oldData = {'CounterTableHeads': tuple(jsonDict['CounterTableHeads'])}
    if 'evtType' in jsonDict:
        oldData['evtType'] = jsonDict['evtType']
    else:
        mm = re.match(r'.*<a name="([0-9]+)">', lns[0])
        if mm is None:
            return None
        oldData['evtType'] = mm.group(1)
        mlog.debug('Found table for event type: %(evtType)s. Parsing...' % oldData)
        # hack to avoid changing existing code using the same function
        # TODO: make user code similar in behaviour
        lns = lns[2:]
    for k in oldData['CounterTableHeads']:
        oldData[k] = list()
    oldData['globStat'] = list()
    j = 0
    searchHead = True
    searchCounters = True
    searchComments = True
    while j < len(lns):
        ln = lns[j].strip()
        if len(ln) == 0:  # skip empty lines
            if enableTrace:
                mlog.debug('Skip line %d: %s' % (j + 1, lns[j]))
            j += 1
            continue
        if searchHead:
            if 'evtTypeDesc' not in oldData:
                # evtHead = #evtTHfmt[0:51] % oldData
                mm = re.match(
                    r'^<th\s+colspan[^>]+><font\s+color[^>]+>%s\s*\((?:<font [^>]+>)?\s*(.+?)\s*(?:</font>)?\)</font>'
                    % oldData['evtType'], ln)
                if mm is None:
                    mlog.warning('Parsing event type description from HTML has failed:\n%s\n'
                                 'Skipping line %d ...' %
                                 (ln, j + 1))
                    j += 1
                    continue
                else:
                    oldData['evtTypeDesc'] = mm.group(1)
            if 'Gauss_version' not in oldData:
                vv = pickTagContents(ln, ' - Gauss <font ', '</font>', tracing=enableTrace)
                if isinstance(vv, tuple):
                    oldData['Gauss_version'] = vv[0]
                    ln = ln[vv[1]:]
            if 'DecFiles_version' not in oldData:
                vv = pickTagContents(ln, 'DecFiles <font', '</font>', tracing=enableTrace)
                if isinstance(vv, tuple):
                    oldData['DecFiles_version'] = vv[0]
                    if 'evtTypeDesc' in oldData and oldData['evtTypeDesc'] == 'dummy':
                        etdescr = readEvtDesc3(oldData['evtType'], vv[0], 'Sim09')
                        if etdescr is not None:
                            oldData['evtTypeDesc'] = etdescr[1]
                        mlog.debug('Retrieved event type description (%(evtTypeDesc)s) '
                                   'for event type (%(evtType)s) from DecFiles %(DecFiles_version)s.' % oldData)
                    ln = ln[vv[1]:]
            if 'DDDB' not in oldData:
                vv = pickTagContents(ln, 'DDDB <font', '</font>', tracing=enableTrace)
                if isinstance(vv, tuple):
                    oldData['DDDB'] = vv[0]
                    ln = ln[vv[1]:]
            if 'SIMCOND' not in oldData:
                vv = pickTagContents(ln, '- SIMCOND <font', '</font>', tracing=enableTrace)
                if isinstance(vv, tuple):
                    oldData['SIMCOND'] = vv[0]
                    ln = ln[vv[1]:]
            if 'APPCONFIG_version' not in oldData:
                vv = pickTagContents(ln, 'APPCONFIG <font', '</font>', tracing=enableTrace)
                if isinstance(vv, tuple):
                    oldData['APPCONFIG_version'] = vv[0]
                    vv = pickTagContents(ln[vv[1]:], '<font color="#FF0000"', '</font>', tracing=enableTrace)
                    if isinstance(vv, tuple):
                        oldData['APPCONFIG_file'] = vv[0]
                        vv = pickTagContents(ln[vv[1]:], '<br', '</th>', tracing=enableTrace)
                        if vv is None:
                            vv = pickTagContents(lns[j], '', '</th>', tracing=enableTrace)
                            if isinstance(vv, tuple):
                                oldData['genTimeStamp'] = vv[0]
                                j += 1
                        else:
                            oldData['genTimeStamp'] = vv[0]
            searchHead = not all([kk in oldData for kk in
                                  ['Gauss_version', 'DecFiles_version', 'DDDB', 'SIMCOND', 'APPCONFIG_version']])
            j += 1
            continue
        if searchCounters:
            ck = None
            for x in oldData['CounterTableHeads']:
                if re.search(r'<div class="?firstcell"?>%s</div>' % x, ln) is not None:
                    ck = x
                    break
            if ck is not None:
                if enableTrace:
                    mlog.debug('Parsing counters in table \'%s\'.' % ck)
                vv = None
                while vv is None:
                    if enableTrace:
                        mlog.debug('Skip line %d: %s' % (j + 1, lns[j]))
                    j += 1
                    vv = pickTagContents(lns[j], '<td><font size=', '</font></td>', tracing=enableTrace)
                while isinstance(vv, tuple):
                    v = vv[0]
                    ff = 1.
                    if v.find('cross-section') > -1:
                        v = v.rstrip(' (mb)')
                        if v == vv[0] and v.endswith(']'):
                            ff, v = readStatValMultiplier(v)
                    if v.find(' x 10<sup>') > -1:
                        ff, v = readStatValMultiplier(v)
                    if v.find('in sample') > -1 and not v.lower().startswith('fraction'):
                        v = 'Fraction of ' + v
                        if enableTrace:
                            mlog.debug('Line %d: %s' % (j + 1, lns[j]))
                    oldData[ck].append({'descr': v, 'value': 0, 'error': 0, 'type': 'counter', 'factor': ff})
                    j += 1
                    # ln = lns[j]
                    vv = pickTagContents(lns[j], '<td><font size=', '</font></td>', tracing=enableTrace)
                # j += 1
                # ln = lns[j]
                n = 0
                vv = None
                while vv is None:
                    if enableTrace:
                        mlog.debug('Skip line %d: %s' % (j + 1, lns[j]))
                    j += 1
                    vv = pickTagContents(lns[j], '<td', '</td>', tracing=enableTrace)
                
                def isfloat(msg):
                    return re.match(r'^-?[0-9]+\.[0-9]+', msg) is not None
                
                while isinstance(vv, tuple):
                    if isfloat(vv[0]):
                        oldData[ck][n]['value'] = float(vv[0]) * oldData[ck][n]['factor']
                    n += 1
                    j += 1
                    vv = pickTagContents(lns[j], '<td', '</td>', tracing=enableTrace)
                # j += 1
                n = 0
                while vv is None:
                    if enableTrace:
                        mlog.debug('Skip line %d: %s' % (j + 1, lns[j]))
                    j += 1
                    vv = pickTagContents(lns[j], '<td', '</td>', tracing=enableTrace)
                while isinstance(vv, tuple):
                    v = vv[0].lstrip('&plusmn; ')
                    if isfloat(v):
                        oldData[ck][n]['error'] = float(v) * oldData[ck][n]['factor']
                    # del oldData[ck][n]['factor']
                    n += 1
                    j += 1
                    vv = pickTagContents(lns[j], '<td', '</td>', tracing=enableTrace)
            else:
                mms = [re.match(r'.*?No (%s) lines found.*' % x, lns[j]) for x in oldData['CounterTableHeads']
                       if re.match(r'.*?No (%s) lines found.*' % x, lns[j]) is not None]
                if len(mms) > 0:
                    oldData[mms[0].group(1)].append(None)
            searchCounters = not all([len(oldData[tk]) > 0 for tk in oldData['CounterTableHeads']])
            if enableTrace:
                mlog.debug('Skip line %d: %s' % (j + 1, lns[j]))
            if not searchCounters:
                for tk in oldData['CounterTableHeads']:
                    oldData[tk] = [oo for oo in oldData[tk] if oo is not None]
            j += 1
            continue
        if searchComments:
            # ln = lns[j].strip()     # !!!
            if not ln.startswith('<p>'):
                if enableTrace:
                    mlog.debug('Skip line %d: %s' % (j + 1, lns[j]))
                j += 1
                continue
            if ln.startswith('<p><font color="#FF0000">Warning: No') and ln.find(' lines found</font>') > -1:
                if enableTrace:
                    mlog.debug('Skip line %d: %s' % (j + 1, lns[j]))
                j += 1
                continue
            if not ln.endswith('</p>'):
                k = j
                tks = [ln.strip(), ]
                while k < len(lns):
                    tks.append(lns[k].strip())
                    if tks[-1].endswith('</p>'):
                        break
                    k += 1
                j = k
                ln = ''.join(tks)
                del tks
            if enableTrace:
                mlog.debug('Parsing production meta-information from\n%s' % ln)
            mm = re.search(r'(Number of accepted events[^:]+): ([0-9]+) / ([0-9]+) = [0-9.]+', ln)
            if mm is not None:
                oldData['globStat'].append({'descr': mm.group(1), 'numer': int(mm.group(2)),
                                            'denom': int(mm.group(3)), 'type': 'fraction'})
            mm = re.search(r'(Number of interactions[^:]+): ([0-9]+) / ([0-9]+) = [0-9.]+', ln)
            if mm is not None:
                oldData['globStat'].append({'descr': mm.group(1), 'numer': int(mm.group(2)),
                                            'denom': int(mm.group(3)), 'type': 'fraction'})
            mm = re.search(r'\(script version ([0-9]+)\)', ln)
            if mm is not None:
                oldData['script_version'] = mm.group(1)
            mm = re.search(r'with ([0-9]+) jobs from', ln)
            if mm is not None:
                oldData['nb_jobs'] = int(mm.group(1).lstrip('0'))
            mm = re.search(r'\(<strong>(Sim[0-9]+[a-z])(?:</strong>)?\)', ln)
            if mm is not None:
                oldData['simVersion'] = mm.group(1)
                if mm.end(1) + 1 == len(ln):
                    continue
            mm = re.search(r'ProdID:<font [^>]+>\s*([0-9]+)</font>', ln)
            if mm is not None:
                oldData['prodID'] = int(mm.group(1).lstrip('0'))
            searchComments = any([x not in oldData for x in ['script_version', 'nb_jobs', 'prodID', 'simVersion']]) or \
                             len(oldData['globStat']) == 0
            j += 1
        if not searchHead and not searchCounters and not searchComments:
            break
    if enableTrace:
        mlog.debug('Parsed dictionary:  %s' % (str(oldData)))
    return oldData


def getGenerationDictJSON(gend, compact=False):
    if compact:
        return json.dumps(gend)
    else:
        return json.dumps(gend, sort_keys=True, indent=2, separators=(',', ': '))


def getSchemaDict(genrep):
    ndic = dict()
    vetoedKeys = ('jsonFN', )
    for (k, v) in genrep.items():
        if k in vetoedKeys:
            continue
        ndic[k] = v
    return ndic


def saveGenerationDict(dirPath, genrep, newFilename=None, compress=False, overwrite=False):
    ret = True
    jsonFileName = '%s_%s_pid-%d.json' % \
                   (genrep['wgName'], genrep['APPCONFIG_file'], genrep['prodID'])
    if newFilename is not None:
        jsonFileName = newFilename
    if compress:
        jsonFileName += '.gz'
    jsonRepFileName = os.path.join(dirPath, jsonFileName)
    # preserve extra meta-information in original dict but not save it
    genrepf = getSchemaDict(genrep)
    # refusing to overwrite existing file
    if os.path.exists(jsonRepFileName) and not overwrite:
        mlog.warning('Refusing to overwrite existing JSON %s.' % jsonFileName)
        return ret
    try:
        if compress:
            import gzip
            fp = gzip.open(jsonRepFileName, 'wb')
        else:
            fp = open(jsonRepFileName, 'wb')
        fp.write(getGenerationDictJSON(genrepf).encode('utf-8'))
        fp.flush()
        fp.close()
    except (OSError, IOError):
        ret = False
    return ret


def loadGenerationDict(floc):
    """
    Get contents of a JSON file automatically detecting retrieval method and compression.
    Args:
        floc (str,dict): As str it represent the filesystem location fo .json(.gz) file; If it is a dict
        the keys wgName, APPCONFIG_file, prodID, simVersion and jsonFn should be used to download
        the file using the web API

    Returns:
        None if error occured or a valid GenerationDict dictionary if successful.
    """
    # TODO: Use code and integrate into findActiveTablesForConds
    pass


def parseHTMLPage(filePath, wgname=None):
    """Returns an ordered list of 'valid' JSON dicts parsed from tables, a sketchy layout of HTML page
     to display for parsing failure analysis and the number of parsing failures + invalid tables detected."""
    global jsonDict
    if not os.path.exists(filePath) or not os.path.isfile(filePath):
        return None, ["Bad file path provided or not a file.", ], 1
    # extract WG dir name from file path if on EOS repository location; otherwise allow external value
    wgn = 'undetermined'
    if filePath.startswith(simStatRepoPath):
        wgn = os.path.split(os.path.dirname(filePath))[1]
    else:
        if wgname is not None:
            wgn = wgname
    # further validation of file name
    fn = os.path.split(filePath)[1]
    mm = re.match(statTableFnRe, fn)
    if mm is None:
        return None
    appconfig = mm.group(1)
    jsonDict = {'CounterTableHeads': ('Interaction Counters', 'Hadron Counters', 'Signal Counters'), }
    ldics = []
    pagelayout = ["Parsing layout for file %s (MC config: %s)" % (fn, appconfig), ]
    bads = 0
    
    def getNextTable(pfile):
        lin = pfile.readline()
        lskip = []
        while len(lin) > 0:
            if lin.find(statTableStart) > -1:
                ret = [lin, ]
                lin2 = pfile.readline()
                while len(lin2) > 0:
                    ret.append(lin2)
                    if lin2.find(statTableStart) > -1:
                        break
                    # ret.append(lin2)
                    lin2 = pfile.readline()
                yield lskip, ret
                lin = lin2
                lskip = []
            else:
                lskip.append(lin)
                lin = pfile.readline()
    
    fp = open(filePath, 'rb')
    for (slines, hlines) in getNextTable(fp):
        dd = parseHtmlStats(hlines)
        pagelayout += slines
        if dd is None:
            mlog.warning('Skipping invalid stat table lines [#%d] in list [#%d].' % (len(hlines), len(ldics) + 1))
            pagelayout += hlines
            bads += 1
            continue
        if 'APPCONFIG_file' not in dd or appconfig != dd['APPCONFIG_file']:
            mlog.warning('Statistics table for different MC conditions detected in page (%s vs. %s). Skipping...' %
                         (dd['APPCONFIG_file'], appconfig))
            pagelayout.append('\n! Skipped invalid table for %s MC conditions !\n' % dd['APPCONFIG_file'])
            bads += 1
            continue
        dd['wgName'] = wgn.strip('/')
        # avoid use of deep-copy
        ldics.append(dict(dd))
        pagelayout.append('\n! Table #%d for event type \'%s\'. !' % (len(ldics), dd['evtType']))
    fp.close()
    return ldics, pagelayout, bads


# ========================================================================
# Store results for the generation process
# ========================================================================


def get_prod_html_table(prod_json, logger=mlog):
    """ Generate the main statistics tables and trailing comments for a given production JSON dict.
    """
    assert isinstance(prod_json, dict)
    if 'CounterTableHeads' not in prod_json:
        logger.error("Invalid Generation dict provided as argument.")
        return None
    warning = list()  # Prompt B cross-section control
    # count = 0
    table = '<table>\n'
    table += evtTHfmt % prod_json
    table += '<tr>\n'
    # weave the HTML table using a matrix of arrays
    tbls = list()
    for i in range(0, 4):
        # each table has three rows (counter name, value, error)
        tbls.append(list([list(), list(), list()]))
    for j in range(0, 3):
        tbls[3][j].append('<td>&nbsp;</td>')
    t1name = prod_json['CounterTableHeads'][0]
    t2name = prod_json['CounterTableHeads'][1]
    t3name = prod_json['CounterTableHeads'][2]
    # Table2: Hadron Counters
    for cnt in prod_json[t2name]:
        name = cnt['descr']
        fields = format_statval(cnt, includeEOL=False)
        if name.find('Fraction') > -1 or name.find('Generator level cut efficiency') > -1:
            if name.find('generated') > -1:
                # generated counters
                for i in range(0, 3):
                    tbls[3][i].append(fields[i])
            else:
                # accepted counters
                for i in range(0, 3):
                    tbls[1][i].append(fields[i])
    # Table3: Signal Counters
    for cnt in prod_json[t3name]:
        fields = format_statval(cnt, includeEOL=False)
        for i in range(0, 3):
            tbls[2][i].append(fields[i])
    # Table1: Counters Interaction
    for cnt in prod_json[t1name]:
        name = cnt['descr']
        fields = format_statval(cnt, includeEOL=False)
        if name.find('Prompt B cross-section') > -1:
            pass
        else:
            for i in range(0, 3):
                tbls[0][i].append(fields[i])
    table1, table2, table3 = '', '', ''
    count1 = len(tbls[0][0])
    count2 = len(tbls[1][0])
    count3 = len(tbls[2][0])
    count4 = len(tbls[3][0]) - 1
    if count1 > 0:
        # Counters Interaction
        table1 = sTblHead % (count1, thCounterFmt % t1name)
        for i in range(0, 3):
            table1 += '<tr>\n' + '\n'.join(tbls[0][i]) + '\n</tr>\n'
        table1 += '</table>\n</td>\n</tr>'
    else:
        warning.append('<p><font color="#FF0000">Warning: No Interaction Counter lines found</font></p>')
        logger.warning("No Interaction Counter lines found for %s/%s:%d." %
                       (prod_json['evtType'], prod_json['simVersion'], prod_json['prodID']))
    if count2 > 0:
        table2 = sTblHead % (count2, thCounterFmt % t2name)
        for i in range(0, 3):
            table2 += '<tr>\n' + '\n'.join(tbls[1][i]) + '\n</tr>\n'
        if count4 != 0:
            for i in range(0, 3):
                table2 += '<tr>\n' + '\n'.join(tbls[3][i]) + '\n</tr>\n'
        table2 += '</table>\n</td>\n</tr>'
    else:
        warning.append('<p><font color="#FF0000">Warning: No Hadron Counters lines found</font></p>')
        logger.warning("No Hadron Counters lines found")
    if count3 != 0:
        table3 = sTblHead % (count3, thCounterFmt % t3name)
        for i in range(0, 3):
            table3 += '<tr>\n' + '\n'.join(tbls[2][i]) + '\n</tr>\n'
        table3 += '</table>\n</td>\n</tr>'
    else:
        warning.append('<p><font color="#FF0000">Warning: No Signal Counters lines found</font></p>')
        logger.warning("No Signal Counters lines found for %s/%s:%d" %
                       (prod_json['evtType'], prod_json['simVersion'], prod_json['prodID']))
    table += table1 + table2 + table3 + '</table>\n'
    web_comments = '\n'.join(warning) + '\n<p>'
    wcmts = []
    if 'globStat' in prod_json:
        wcmts += [format_statval(cnt) for cnt in prod_json['globStat']]
    prod_json['tmpSim'] = ''
    if 'simVersion' in prod_json:
        prod_json['tmpSim'] = ' (<strong>%s</strong>)' % prod_json['simVersion']
    if 'sigPIds' in prod_json:
        wcmts.append('Signal cross-section summed up for process IDs: {}'.format(prod_json['sigPIds']))
    wcmts.append('Statistics done (script version %(script_version)s) with %(nb_jobs)i jobs from the '
                 'following%(tmpSim)s ProdID:<font color="#0000FF"> %(prodID)i' % prod_json)
    del prod_json['tmpSim']
    if len(wcmts) > 0:
        web_comments += '<br/>'.join(wcmts)
    del wcmts
    web_comments += '</font></p>\n'
    return table, web_comments


def rebuild_html_page(dlist, multiples=False):
    if len(dlist) < 1:
        return
    if not isinstance(dlist, list) or not isinstance(dlist[0], dict):
        raise TypeError('First argument must be a list of table dictionaries.')
    appconfig = dlist[0]['APPCONFIG_file']
    htmlinner = ''
    htmlevtlinks = []
    elast = ''
    for i in range(len(dlist)):
        dtable = dlist[i]
        if "APPCONFIG_file" not in dtable:
            mlog.error("Ignoring possible malformed dictionary @%d" % (i + 1))
            continue
        if dtable["APPCONFIG_file"] != appconfig:
            mlog.error("Ignoring table w/ different MC config: %s vs. %s" % (dtable["APPCONFIG_file"], appconfig))
            continue
        # ensure internal links appear only once to the group of tables for an event type
        # assume tables are ordered by event type
        if dtable['evtType'] != elast:
            htmlevtlinks.append(evtTypeLink % dtable)
        htable, hcomments = get_prod_html_table(dtable)
        if dtable['evtType'] != elast:
            htmlinner += evtTypeLinkAnchor % dtable
        htmlinner += htable + hcomments
        elast = dtable['evtType']
    pagetstamp = "<div style=\"text-align:right;fonts-size:x-small;\">Generated: %s</div>\n" % \
                 datetime.datetime.utcnow().strftime("%a, %d %b %Y %H:%M:%S +0000")  # RFC 2822
    htmlpage = webpageHead + pagetstamp + evtTypeLinksContainer % " ".join(htmlevtlinks) + htmlinner + webPageEnd
    return htmlpage


def repoDiskGenerator(path, fmask='Evt*.json'):
    """
    Generator for JSON objects loaded from a specified repository path.
    Args:
        path (str): Location of JSON repository
        fmask (str): File name mask for JSON files. Use '*.json' to load even unactive files.
    """
    if not os.path.exists(path):
        yield None
    flist = glob.glob(os.path.join(path, fmask))
    for fn in flist:
        with open(fn, 'r') as fp:
            yield json.load(fp)


def getRepoJSONs(pawg, mc_conds, attrFilter={}, includeHist=False):
    """
    Load a filtered series of JSONs from the database using EOS access or HTTP requests as fallback.
    Args:
        pawg (str): Physics working group acronym in the database
        mc_conds (str): MC conditions string starting w/ "SimNN-..."
        attrFilter (dict): a dictionary to match JSONs which to load and return
        includeHist (bool): controls whether disabled/replaced tables should be selected/loaded

    Returns:
        Returns the list of dictionaries parsed from the matched JSONs in database (None if error).
    """
    retv = None
    hasEOS = os.path.exists(simStatRepoPath)
    repoObjGenerator = None
    if not hasEOS:
        raise NotImplementedError('The HTTPS request branch will be implemented in the near future.')
    else:
        repoObjGenerator = repoDiskGenerator
        kwargs = {'path': os.path.join(simStatRepoPath, pawg, mc_conds)}
        if includeHist:
            kwargs['fmaks'] = '*.json'
    if repoObjGenerator is None:
        return retv
    retv = []
    
    for rdic in repoObjGenerator(**kwargs):
        pass
        # check if dicts correspond to attrFilter
    pass
    return retv


def loadRepoJSONs(path, includeHist=False, keys=()):
    """
    Just loads the json files found in a specified path.
    Adds extra jsonFN key with file name which encodes status.
    """
    fmask = 'Evt*.json'
    if includeHist:
        fmask = '*.json'
    flist = glob.glob(os.path.join(path, fmask))
    dlist = []
    for fn in flist:
        lrepo = None
        with open(fn, 'r') as fp:
            try:
                lrepo = json.load(fp)
            except json.JSONDecodeError as exx:
                mlog.error('Failed to load {}\n{}'.format(fn, str(exx)))
            finally:
                fp.close()
        if lrepo is None:
            continue
        if len(keys) > 0:   # keep JSON dict as flat as possible
            flrepo = dict([(k, lrepo[k]) for k in keys if k in lrepo])
            lrepo = flrepo
        if includeHist:
            jfn = fn
            if len(os.path.dirname(fn)) > 0:
                jfn = os.path.split(fn)[1]
            lrepo['jsonFN'] = jfn
        dlist.append(lrepo)
    return dlist


def checkRepoConsistency(bpath, wgn, mcconf, rdicts=None):
    """Check all dictionaries are consistent with repository rules."""
    assert os.path.isdir(os.path.join(bpath, wgn)) and os.path.isdir(os.path.join(bpath, wgn, mcconf)), \
        "Invalid path elements provided."
    if rdicts is None:
        rdicts = loadRepoJSONs(os.path.join(bpath, wgn, mcconf), includeHist=True)
    assert len(rdicts) > 0 and 'jsonFN' in rdicts[0], "Checks only dicts loaded from disk w/ loadRepoJSONs."
    errs = []
    evtwa = []  # event types with known active tables
    # Production conditions defined uniquely by MC conditions, event type and sim variant/version
    # 0. check json filename corresponds to dict data, and dictionaries are self-consistent
    # 1. check for multiple tables active for same production conditions (or same production - see 2)
    # 2. check for same production ID table corresponding to multiple event types
    # 3. check for disabled/replaced tables with no active correspondent
    # 4. check and warn if older production ID statistics overwrite newer production IDs for same conditions
    for nd in rdicts:
        pid = nd['prodID']
        simv = nd['simVersion']
        evt = nd['evtType']
        devt = nd['evtTypeDesc']
        isActive = nd['jsonFN'].startswith('Evt')
        dfn = nd['jsonFN']
        if mcconf != nd['APPCONFIG_file']:
            errs.append('Bad MC config %s in %s. Abort processing dict.' % (nd['APPCONFIG_file'], dfn))
            continue
        if 'wgName' not in nd:
            errs.append('%s has no WG name key.' % dfn)
        else:
            if nd['wgName'] != wgn:
                errs.append('Different WG name detected in %s (%s vs. %s)' % (dfn, nd['wgName'], wgn))
        mm = re.match(r'.*?Evt(\d+)-P(\d+)\.json$', nd['jsonFN'])
        if mm is None:
            errs.append('JSON file name does not conform to rules: %s' % dfn)
            continue
        if mm.group(1) != evt or mm.group(2) != str(pid):
            errs.append('Data in file %s is corrupted or illegal file name for prod. #%d (evt. type %s - %s).'
                        % (dfn, pid, evt, devt))
            continue
        tt = [x for x in rdicts if x['jsonFN'].endswith('-P%d.json' % pid) and x['jsonFN'].startswith('Evt')]
        if len(tt) > 1:
            errs.append('Multiple active tables for same production #%d: %s' %
                        (pid, ', '.join([x['jsonFN'] for x in tt])))
            for td in tt:
                if td['evtType'] != evt:
                    errs.append('Data in file %s (PID: #%d) corresponds to different event type %s - %s '
                                '( vs. %s - %s).' %
                                (td['jsonFN'], td['prodID'], td['evtType'], td['evtTypeDesc'], evt, devt))
        tt = [x for x in rdicts if x['evtType'] == evt and x['jsonFN'].startswith('Evt')]
        if len(tt) > 1 and evt not in evtwa:
            tdic = {}
            for xt in tt:
                if 'simVersion' not in xt:
                    errs.append('Missing key simVersion in dict from file %s.' % xt['jsonFN'])
                    tdic = {}
                    continue
                simk = xt['simVersion']
                if simk not in tdic:
                    tdic[simk] = [xt, ]
                else:
                    tdic[simk].append(xt)
            for simk, xtl in tdic.items():
                if len(xtl) > 1:
                    tfns = '\n'.join([tx['jsonFN'] for tx in xtl])
                    errs.append('Multiple active tables for exactly same conditions %s/%s/%s in files:\n%s'
                                '\nOnly one active table allowed.' % (mcconf, evt, simk, tfns))
        if not isActive and evt not in evtwa:
            if len(tt) == 0:
                errs.append('For event type %s (%s) there are no active tables available.' % (evt, devt))
            else:  # keep track of processed event types
                if evt not in evtwa:
                    evtwa.append(evt)
        if isActive:
            # take only non active, not ignored tables
            tt = [x for x in rdicts if x['evtType'] == evt and not x['jsonFN'].startswith(('Evt', 'ign')) and
                  x['simVersion'] == simv and x['prodID'] != pid]
            for td in tt:
                if td['prodID'] > pid:
                    print('WARNING: Statistics for prod. #%d overwritten by earlier prod. #%d for %s' %
                          (td['prodID'], pid, simv))
    return errs


def loadCheckRepoJSON(basepath, wgname=None, slog=mlog):
    """
    Returns a list of dictionaries containing basic identification data for JSONs in repository when
    appconfig is last path component in basepath. Optionally a wg name can be provided for consistency
    checks. Optional argument wgname when set triggers consistency checks (loading every json).
    """
    ret = []
    appconf = os.path.split(basepath)[1]
    if not appconf.startswith('Sim'):
        return ret
    wgn = None
    if wgname is not None:
        wgn = wgname
    ejfiles = glob.glob(os.path.join(basepath, '*.json'))
    for tf in ejfiles:
        if not os.path.isfile(tf):
            slog.warning('JSON path not a file: %s' % tf)
            continue
        tfn = os.path.splitext(os.path.split(tf)[1])[0]
        mm = re.match(r'(.*)?Evt(\d+)-P(\d+)$', tfn)
        if mm is None:
            continue
        if len(mm.group(1)) > 0 and not mm.group(1).startswith('owr-'):
            continue
        if wgn is not None:  # start consistency checks
            fp = open(tf, 'r')
            pj = json.load(fp)
            fp.close()
            if 'wgName' not in pj or pj['wgName'] != wgn:
                slog.warning('Skipping record with different (%s) PAWG than considered default.' % pj['wgName'])
                continue
            if 'APPCONFIG_file' not in pj or pj['APPCONFIG_file'] != appconf:
                slog.warning('Skipping record with different (%s) APPCONFIG than considered default.' %
                             pj['APPCONFIG_file'])
                continue
        isProdDisabled = tfn.startswith('owr-')
        ret.append({'evtType': mm.group(2), 'fn': tfn, 'prodID': int(mm.group(3)), 'disabled': isProdDisabled})
    return ret


def findActiveTablesForConds(minfo):
    """
    Extract JSON dict for active statistics tables in repo corresponding to given production conditions
    Args:
        minfo (dict): Dictionary containing all information about production conditions

    Returns:

    """
    # TODO: Should search also already saved tables and load them from .json.gz
    sv = simVariant(minfo['simVersion'])
    eospath = os.path.join(simStatRepoPath, sv.simStat(), minfo['wgName'], minfo['APPCONFIG_file'])
    wserv = resolverURL % sv.simName()
    active_files = []
    rdata = {}
    wapi = False
    if os.path.isdir(eospath):
        fmask = 'Evt%(evtType)s-P*.json' % minfo
        active_files = glob.glob(os.path.join(eospath, fmask))
    else:
        wapi = True
        rdata = {'wg': minfo['wgName'], 'appconf': minfo['APPCONFIG_file'], 'evtype': minfo['evtType']}
        rr = rq.post(wserv, rdata)
        if rr.ok:
            rd = rr.json()
            if 'error' in rd:
                mlog.error('Could not get active prods for event type %s: %s' % (minfo['evtType'], rd['error']))
            else:
                active_files = rd['files']
    for fn in active_files:
        ostat = None
        if wapi:
            rdata['jfn'] = fn
            rf = rq.post(wserv, rdata)
            if rf.ok:
                ostat = rf.json()
                if 'error' in ostat:
                    mlog.error('Failed to retrieve %s/%s/%s' % (minfo['wgName'], minfo['APPCONFIG_file'], fn))
                    continue
        else:   # TODO: add some exception catching to be able to continue?!
            with open(os.path.join(eospath, fn), 'r') as fp:
                ostat = json.load(fp)
                fp.close()
        yield ostat


def storePageJson(filePath, wgname=None, hintDate=None, dryrun=False, altrepobase=None):
    """
    Create/update the JSON structure parsing a given HTML file, if file ends with date newly generated JSON files
    are named as disabled/replaced. Naming convention is to create/access directory with name given by MC config
    (APPCONFIG_file key) and store within active production stats with name Evt<eeeeeeee>-P<pppppp>.json. Disabled
    production stats have name owr-yyyymmdd-Evt<eeeeeeee>-P<pppppp>.json.
    """
    dpath = os.path.dirname(filePath)
    wgp = os.path.split(dpath)[1]
    fn = os.path.split(filePath)[1]
    print('Processing %s ...' % filePath)
    wgn = None
    if wgp.lower().endswith('-wg'):
        wgn = wgp
    if wgname is not None:
        wgn = wgname
    if wgn is None:
        print('Unknown WG and cannot continue.')
        return
    mm = re.match(r'Generation_[^_]*\.html_(\d{8})$', fn)
    if mm is None:
        mm = re.match(r'Generation_[^_]*_(\d{8})\.html$', fn)
    isCurrent = True
    rdate = 'unknown'
    if mm is not None:
        isCurrent = False
        rdate = mm.group(1)
        # rename file to keep proper html extension
        if not dryrun:
            tfn = os.path.splitext(fn)[0]
            tfn += '_%s.html' % rdate
            print('copying %s to %s before processing.' % (fn, tfn))
            shutil.copy2(filePath, os.path.join(dpath, tfn))
        else:
            tfn = fn
        filePath = os.path.join(dpath, tfn)
    if hintDate is not None:
        rdate = hintDate
    dlist, ptpl, nerrs = parseHTMLPage(filePath, wgn)
    if nerrs > 0:
        print('Error while parsing file:\n%s\nAborting...' % '\n'.join(ptpl))
        return
    if len(dlist) < 1:
        print('Parsing failed in unclear way. Will not continue:%s\n%s\n%d' % (str(dlist), str(ptpl), nerrs))
        return
    if altrepobase is not None and os.path.isdir(altrepobase):
        dpath = altrepobase
    jpath = os.path.join(dpath, dlist[0]['APPCONFIG_file'])
    if not os.path.isdir(jpath):
        os.makedirs(jpath)
    ejps = loadCheckRepoJSON(jpath, wgname=wgn)  # slow run as all JSON are parsed!
    ejpids = [ii['prodID'] for ii in ejps]
    ejevts = [ii['evtType'] for ii in ejps]
    # #print('Pids: %s' % ','.join(map (lambda x: str(x), ejpids)))
    # #print('Evts: %s' % ','.join(ejevts))
    # #print(str(ejps))
    cjFnTpl = 'Evt{}-P{:d}.json'
    djFnTpl = 'owr-{}-Evt{}-P{:d}.json'
    for dd in dlist:
        ij = -1
        try:
            ij = ejpids.index(dd['prodID'])
        except ValueError:
            mlog.debug('Error occured:', exc_info=True)
        if ij >= 0:
            if dd['evtType'] != ejps[ij]['evtType']:
                print('Ignoring MC stats for prodID #%d: %s (new) vs. %s (old)' %
                      (dd['prodID'], dd['evtType'], ejps[ij]['evtType']))
                continue
            # do not allow transitions from current to replaced or vice-versa
            if isCurrent and ejps[ij]['fn'].startswith('owr'):
                print('Transitions from replaced to active not allowed for prod. #%(prodID)d, '
                      'event type id %(evtType)s. Skipping...' % dd)
                continue
            if not isCurrent and ejps[ij]['fn'].startswith('owr'):
                print('Will not replace at %s already substituted file (%s) for same prodID #%d' %
                      (rdate, ejps[ij]['fn'], ejps[ij]['prodID']))
                continue
            if not isCurrent and ejps[ij]['fn'].startswith('Evt'):
                print('Active prod. #%(prodID)d cannot be replaced by same prod. Skipping...' % dd)
                continue
        if ij < 0:
            if dd['evtType'] in ejevts:
                ij = ejevts.index(dd['evtType'])
                if not rdate.isdigit() and isCurrent:
                    stoday = datetime.datetime.now().strftime('%Y%m%d')
                    cfn = cjFnTpl.format(ejps[ij]['evtType'], ejps[ij]['prodID'])
                    dfn = djFnTpl.format(stoday, ejps[ij]['evtType'], ejps[ij]['prodID'])
                    print('Replacing prod #%d with prod #%d for event type %s.' %
                          (ejps[ij]['prodID'], dd['prodID'], dd['evtType']))
                    if not dryrun:
                        shutil.move(os.path.join(jpath, cfn), os.path.join(jpath, dfn))
                else:
                    print('Will not replace evtType %s prod #%d in %s w/ disabled stats from prod #%d.' %
                          (dd['evtType'], ejps[ij]['prodID'], ejps[ij]['fn'], dd['prodID']))
                    # continue
        # test if overwriting any evt type and disable old then save new!
        if isCurrent:
            jfn = cjFnTpl.format(dd['evtType'], dd['prodID'])
        else:
            jfn = djFnTpl.format(rdate, dd['evtType'], dd['prodID'])
        print('Saving stats for prod #%d in file %s...' % (dd['prodID'], jfn))
        if not dryrun:
            saveGenerationDict(jpath, dd, newFilename=jfn)


def HtmlTableMerge(htmlFn, jdic, htbl, webcmt, logger=mlog):
    """
    Implement the old statistics table merging algorithm overwritting already existing
    table for given MC conditions and event type. This is still compatible w/ Sim08.

    Args:
        htmlFn (str):   File path of static HTML for merging
        jdic (dict):    dictionary w/ new production meta-data
        htbl (str):     HTML fragment containing new statistics table
        webcmt (str):   HTML fragment containing extra text to appear after table
        logger (logging.Logger):    logger to use for error reporting

    Returns:
        Writes the updated HTML to disk and return 'OK' if successful or 'KO' when error occurs.
    """
    retv = 'OK'
    tsFmt = "<div style=\"text-align:right;fonts-size:x-small;\">Generated: %s</div>\n"
    owebn = ''
    today_ts = datetime.date.today().strftime("%a, %d %b %Y %H:%M:%S +0000")
    webn = ''
    if os.path.exists(htmlFn):
        logger.info('Will attempt merging to existing file %s ...' % htmlFn)
        f = open(htmlFn, 'r')
        ftext = f.read()
        f.close()
        # Looking for production event type
        # if we find it, it means we used already this production for another generation
        # it has been tested before anyway
        if ftext.find(jdic['evtType']) == -1:
            # if there is not yet a simulation for this production
            pos_list = ftext.find(statTableSep)
            pos_table = ftext.find(bodyEndTag)
            owebn = ftext[:pos_list] + " " + evtTypeLink % jdic
            owebn += ftext[pos_list:pos_table] + evtTypeLinkAnchor % jdic + htbl + webcmt + ftext[pos_table:]
            logger.info("Appended as new event type for given simulation conditions.")
        else:
            # There is already at least a table for this production event type
            pos_table = ftext.find(evtTypeLinkAnchor % jdic)
            # get active tables for event type and conditions
            cpid = jdic['prodID']
            for edic in findActiveTablesForConds(jdic):
                if 'prodID' in edic and cpid == edic['prodID']:
                    logger.warning('Existing statistics tables for same production ID (%d). '
                                   'Please, explain in JIRA task.' % cpid)
                    rr = mkGenCmpReport(jdic, edic)
                    if isinstance(rr[1], bool) and rr[1]:
                        logger.error(rr[0])
                    continue
                if 'simVersion' in edic and jdic['simVersion'] == edic['simVersion']:
                    logger.warning('New statistics table will replace existing one for prod ID %(prodID)d.' % edic)
                    rr = mkGenCmpReport(jdic, edic)
                    if isinstance(rr[1], bool) and rr[1]:
                        logger.error(rr[0])
            # insert new table before first table w/ same event type
            owebn = ftext[:pos_table] + evtTypeLinkAnchor % jdic + htbl + webcmt + ftext[pos_table:]
        # update time stamp
        ph = tsFmt.rfind(':')
        tsHead = tsFmt[:ph+1]
        tsp_start = owebn.find(tsHead)
        if tsp_start > -1:
            tsp_end = owebn.find('</div>', tsp_start)
            webn = owebn[:tsp_start+len(tsHead)] + today_ts + owebn[tsp_end:]
        else:
            tsp_start = len(webpageHead)
            webn = owebn[:tsp_start] + today_ts + owebn[tsp_start:]
    else:
        # the file does not exist, create it
        logger.info('%s does not exist, creating it now.' % htmlFn)
        webn = webpageHead + tsFmt % today_ts
        webn += evtTypeLinkHRef % jdic + evtTypeLinkAnchor % jdic
        webn += htbl + webcmt + webPageEnd
    with open(htmlFn, 'w', encoding='utf-8') as f:
        f.write(webn)
        f.flush()
        f.close()
    return retv


def gen_store_results_dict2(stat_dic, job_dic, nb_jobs, logger=mlog):
    """New approach to building the HTML file w/ the results using JSON dict.
    (!) add 'wgName' to job_dic

    Args:
        stat_dic (dict):
        job_dic (dict):
        nb_jobs (int):
        logger (logging stream):

    Returns:
        string : 'OK' if successful or 'KO' if error occured
    
    """
    global jsonDict     # TODO: made global because changed in operations; why not make it argument?
    # TODO: Use of job_dic and jsonDict may be confusing after jsonDict is fully generated!
    logger.debug('In gen_store_results_dict2:\nnb_jobs: %d\n%s' % (nb_jobs, str(stat_dic)))
    
    job_dic['evtType'] = str(job_dic['eventType'])
    jsonDict['evtType'] = job_dic['evtType']
    jsonDict['requestID'] = job_dic['requestID']
    
    # use 'simVariant' else fallback to previous mode of determining simVersion
    if 'simVariant' in job_dic:
        jsonDict['simVersion'] = job_dic['simVariant']
    else:
        if 'BKKPath' in job_dic and len(job_dic['BKKPath']) > 8:
            simparts = [x for x in job_dic['BKKPath'].split('/').reverse() if x.lower().startswith('sim')]
            if len(simparts) > 0:
                job_dic['simVariant'] = simparts[0]
                jsonDict['simVersion'] = simparts[0]
    sv = simVariant(job_dic['simVariant'])
    job_dic['simName'] = sv.simName()
    # perform statistics calculations on the results - fills jsonDict!
    if 'C1' in stat_dic:
        xsval, xserr, xsunit = format_to_unit(stat_dic['C1'], 0, unitlst=xsUnits)
        logger.debug('Total signal production XS is %s %s.' % (xsval, xsunit))
    calcs = operations(stat_dic)
    logger.info('\x1b[1mResults:\x1b[0m\n%s' % calcs)
    
    rc = 'OK'
    if calcs == '':
        rc = 'KO'
        return rc

    for k in ['Gauss_version', 'DecFiles_version', 'DDDB', 'SIMCOND', 'APPCONFIG_version', 'APPCONFIG_file']:
        jsonDict[k] = job_dic[k]
    evtdesc = readEvtDesc4(jsonDict)
    job_dic['evtTypeDesc'] = 'dummy'
    if evtdesc is not None:
        job_dic['evtTypeDesc'] = evtdesc[1]
    else:
        logger.error('Failed to retrieve event type description for %s' % str(jsonDict['evtType']))
        logger.debug('Prod dict for unavailable event type description:\n%s' % str(jsonDict))
    jsonDict['evtTypeDesc'] = job_dic['evtTypeDesc']
    job_dic['genTimeStamp'] = time.ctime()
    jsonDict['genTimeStamp'] = job_dic['genTimeStamp']
    jsonDict['globStat'] = [{'descr': 'Number of accepted events/generated events', 'numer': stat_dic['N3'],
                             'denom': stat_dic['N1'], 'type': 'fraction'},
                            {'descr': 'Number of interactions in accepted events/generated interactions',
                             'numer': stat_dic['N4'], 'denom': stat_dic['N2'], 'type': 'fraction'}]
    jsonDict.update(prodID=job_dic['prodID'], nb_jobs=nb_jobs, script_version=__version__)
    if 'C2-pids' in stat_dic:
        jsonDict.update(sigPIds=stat_dic['C2-pids'])

    if 'L0' in stat_dic:
        jsonDict['globStat'].append({'descr': 'Branching fraction of the signal decay mode', 'value': stat_dic['L0'],
                                     'type':  'sig_brfrac'})
    rtt = get_prod_html_table(jsonDict)
    table, web_comments = '', ''
    if isinstance(rtt, tuple):
        (table, web_comments) = rtt
    LocationPath = os.getcwd()
    hfile = None
    # Try to find interactively the latest HTML file if available and copy to work directory
    # Table merging in HTML is done regardless whether the new table would overwrite an older one
    #   since HTML is only used for quick display and checks by human operator
    if 'wgName' not in job_dic:
        if cacheWgName and wgName is not None:
            job_dic['wgName'] = wgName
            # logger.debug('Using same PAWG as provided externally on command line: %s' % wgName)
        else:
            logger.warning('PAWG not set for %(prodID)d. Trying to get latest version of '
                           '%(APPCONFIG_file)s HTML tables from WG repo ...' %
                           job_dic)
            selDir = getRepoPawgName(job_dic)
            if selDir is None:
                logger.error('Cannot continue since no physics WG could be specified. Please, re-run script on lxplus.')
                return 'KO'
            else:
                job_dic['wgName'] = selDir
    hfile = get_WG_Html_File(job_dic)
    if hfile is None:
        logger.debug('No file HTML found or PAWG HTML file retrieval failed. Continue...')
        # ensure HTML not overwritten if working w/ multiple PAWG prods
        hfile = os.path.join(LocationPath, ('%(wgName)s_' + statTableFnFmt) % job_dic)
    # save dict here to be able to include phys wg name - final JSON saved here!
    jsonDict['wgName'] = job_dic['wgName']
    saveGenerationDict(LocationPath, jsonDict, compress=True)
    HtmlTableMerge(hfile, jsonDict, table, web_comments, logger=logger)
    return rc


def getHTML(url):
    mlog.debug(">>> URL -> %s" % url)
    usock = ulibrq.urlopen(url)
    htmlSource = usock.read()
    usock.close()
    return htmlSource


def getHTMLlines(url):
    # --- get url content (line by line) ---
    usock = ulibrq.urlopen(url)
    htmlSource = usock.readlines()
    usock.close()
    return htmlSource


def apiGetCall(get_data={}, simver='SIM09', timeout=3):
    rd = None
    try:
        r = rq.get(resolverURL % simver.upper(), params=get_data)
        if not r.ok:
            return None
        rd = r.json()
        if 'error' in rd:
            mlog.error('Error using web API: %s' % rd['error'])
            return None
    except rq.Timeout as ex:
        mlog.warning('Timeout occurred.', exc_info=True)
        time.sleep(timeout)
        timeout += 4
        if timeout <= 15:
            return apiGetCall(get_data, simver, timeout)
    return rd


def httpGetWGNames(simver='SIM09', timeout=5):
    ret = []
    try:
        r = rq.post(resolverURL % simver.upper(), data={'pawg': 'list'})
        if not r.ok:
            return ret
        rd = r.json()
        if 'error' in rd:
            mlog.error('Error using web API: %s' % rd['error'])
            return ret
        if 'WG_names' in rd:
            ret = [x + '-WG' for x in rd['WG_names']]
    except rq.Timeout as ex:
        mlog.warning('Timeout occured.', exc_info=True)
        time.sleep(5)
        if timeout <= 20:
            return httpGetWGNames(timeout=timeout+5)
    return ret


def getHTTPFile(url, splitLines=True, reqTimeout=15, testASCII=False):
    global mlog
    ret = ''
    try:
        fp = ulibrq.urlopen(url, timeout=reqTimeout)
        if fp is None:
            raise Exception('Unknown handler for given location protocol.')
        nbytes = None
        if 'content-length' in fp.info():
            nbytes = int(fp.info().getheader('content-length'))
            if nbytes < 0:
                nbytes = None
        mlog.debug('Will read %d bytes from %s...' % (nbytes, url))
        ret = fp.read(nbytes)
        fp.close()
        if 0 < nbytes != len(ret):
            mlog.warning('Less data read from server %d/%d' % (len(ret), nbytes))
        mlog.debug('Read %d chars from server.' % (len(ret)))
    except Exception as exx:
        mlog.error('Exception: %s' % str(exx), exc_info=True)
        time.sleep(10)
        if reqTimeout < 500:  # re-entrant at least 5 times
            ret = getHTTPFile(url, splitLines=False, reqTimeout=reqTimeout * 2)
    enc = {}
    if testASCII and len(ret) > 0:
        import chardet
        tret = None
        while tret is None:
            try:
                tret = len(enc) == 0 and str(ret) or str(ret, encoding=enc['encoding'])
            except UnicodeDecodeError:
                try:  # try UTF-8 before forcing lengthy detection
                    tret = str(ret, 'utf-8')
                    enc['encoding'] = 'utf-8'
                    enc['confidence'] = 1.0
                    enc['language'] = 'guess'
                except UnicodeDecodeError:
                    enc = chardet.detect(ret)
                    mlog.warning('Converted URL text contents from %s to Unicode.' % enc['encoding'])
        if len(enc) > 0:
            enc['isUnicode'] = True
            ret = tret
        else:
            enc['isUnicode'] = False
    if splitLines:
        ret = ret.split('\n')
    if testASCII:
        return ret, enc
    return ret


def readEvtDesc4(jdic):
    if any([x not in jdic for x in ['evtType', 'DecFiles_version', 'simVersion']]):
        return None
    sv = simVariant(jdic['simVersion'])
    return readEvtDesc3(jdic['evtType'], jdic['DecFiles_version'], sv.simName())


def readEvtDesc3(evtnb, decfilesVer, simver):
    """
        Retrieves the nickname and decay descriptor associated w/ an event type number of e given DecFiles version.
        Uses CVMFS files if available or web API call if required data base is not available on CVMFS (locally).
    Args:
        evtnb (int,str): DecFiles event type number (all digits); may be also int
        decfilesVer (str): the version of DecFiles package for which information is read.

    Returns:
        None if error or tuple with fields: evtType, nickname, decay descriptor
    """
    if not evtnb.isdigit():
        return None
    ret = None
    # local CVMFS solution
    if os.path.isdir(decfilesRepoPath):
        dfPath = os.path.join(decfilesRepoPath, decfilesVer, 'doc/table_event.txt')
        if os.path.isfile(dfPath):
            import GenStatParser as mcp
            etable = mcp.getFileContents(dfPath)
            if etable is not None:
                etable = etable.split("\n")
                evtrec = [x for x in etable if x.startswith(str(evtnb))]
                if len(evtrec) == 1:
                    ret = tuple([x.strip() for x in evtrec[0].split('|')])
    if ret is not None:
        return ret
    # web API
    resp = apiGetCall({'decfiles': decfilesVer, 'evttype': str(evtnb)}, simver=simver)
    if resp is not None and resp['evttype'] == str(evtnb):
        ret = (evtnb, resp['nick'], resp['decay'])
    return ret


def readEvtDesc(evntp, decfilesVer):
    """Try to get event type from CVMFS using EOS backdoor. Fails when EOS is
    not readable."""
    lpath = os.path.abspath(os.path.join(simStatRepoPath, '../decfiles/releases/%s/options' % decfilesVer,
                                         '%s.py' % evntp))
    ret = None
    if os.path.isfile(lpath):
        fp = open(lpath, 'r')
        ret = fp.read()
        fp.close()
        ret = ret.split('\n')
    return ret, lpath


def readErrorDesc2(evntp, DecFilesVer='latest'):
    """Retrieve from DecFiles web the event type description for a given event type number.
    Uses the associated Python file to extract the event type description as name of the
    .DEC file. Event types may be available in only in specific version of DecFiles package."""
    # TODO: Should use a multi-line re match for getting .py URL
    newurl_base = "http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/releases/"
    newurl = "%s%s/table_evttype.php" % (newurl_base, DecFilesVer)
    etUrlRe = r'<?\s*a?\s*.*(?:href=)?"?\s*(%s[^\/]+/options/%s\.py)"?\s*>.*' % (newurl_base, evntp)
    ret = 'dummy'
    pyHtml, pyUrl = readEvtDesc(evntp, DecFilesVer)
    if pyHtml is None:
        if DecFilesVer not in decfilesCache:
            tblHtml, encState = getHTTPFile(newurl, testASCII=True)
            decfilesCache[DecFilesVer] = (tblHtml, encState)
        else:
            tblHtml, encState = decfilesCache[DecFilesVer]
            mlog.debug('Using cached catalog for DecFiles %s...' % DecFilesVer)
        # import chardet
        # enc = chardet.detect('\n'.join(tblHtml))
        # if enc['encoding'].lower() != 'ascii' and enc['encoding'].lower() != 'utf-8':
        #    tblHtml = map (lambda x: unicode(x, enc['encoding']), tblHtml)
        #    mlog.warn('Converted HTML contents from %s to Unicode' % str(enc))
        if len(tblHtml) == 0:
            mlog.warning('Failed to retrieve event type table. Please, retry later...')
            return ret
        pyUrl = None
        evtHtml = [ln for ln in tblHtml if ln.find(evntp) > -1]
        if len(evtHtml) == 0:
            mlog.warning(
                'Could not locate event type in official table. Please, check event type \'%s\' is valid.' % evntp)
            return ret
        for line in evtHtml:
            mm = re.match(etUrlRe, line.strip())
            if mm is not None:
                pyUrl = mm.group(1)
                break
        if pyUrl is None:
            mlog.error('Could not locate Python options file URL for event type \'%s\'.' % evntp)
            return ret
        # so far it is safe to assume everything is ASCII in Python files
        pyHtml = getHTTPFile(pyUrl)
    else:
        mlog.debug('Loaded event type definition from %s' % pyUrl)
    head = [ln for ln in pyHtml if ln.startswith('#')]
    code = []
    for line in head:
        p = line.find('Event Type:')
        if p > -1:
            evt = line[p + 11:].strip()
            if not evt.isdigit():  # fail if event type format changes
                mlog.error('Detected event type does not match LHCb format. Please, update code!')
                return ret
            if evt != evntp:  # bad options file!
                mlog.error('Event type does not match in Python options file \'%s\'.' % pyUrl)
                return ret
            code = [ln for ln in pyHtml if ln.find('UserDecayFile') > -1]
            break
    if len(code) != 1:  # should not have multiple decay files in event type
        mlog.error('Invalid format in Python options file \'%s\'.' % pyUrl)
        return ret
    # next RE matches last path element of decay file path (could also contain . in file name!)//
    mm = re.match(r'.*"\$DECFILESROOT/dkfiles/([^/]+)\.dec"$', code[0].strip())
    if mm is None:
        mlog.error('Failed decay match:\n%s' % code[0])
        return ret
    return mm.group(1)
