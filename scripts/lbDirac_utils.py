#! /usr/bin/env python3.8
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright CERN for the benefit of the LHCb Collaboration                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""Module implementing helper tools to access information from LHCb DIRAC system."""

# see info provided by dirac-bookkeeping-production-information
__version__ = '20210625'

import logging
import subprocess as sp
from time import sleep
from datetime import datetime
import sys
import os
import stat
import re
import zipfile

from threading import Timer
import six
from urllib.request import urlopen
from io import BytesIO as zipBytes

import tarfile
import zlib

mlog = logging.getLogger('lbMcST.%s' % __name__)
mlog.setLevel(mlog.parent.level)
mlog.debug('Module %s v. %s loaded.' % (__name__, __version__))

if __name__ == '__main__':
    mlog.error('This module cannot be run as a script.')
    raise NotImplementedError('Aborting execution...')


# mlog = logging.getLogger()
logSE_name = 'LogSE-EOS'
prod_lpath = '/lhcb/MC/%(simYear)s/LOG/%(prodID)08d'
logBaseName = 'GeneratorLog'

# detect early if LHCbDirac environment is present:
try:
    from DIRAC import S_OK
    if S_OK()['OK']:
        mlog.debug('DIRAC Python interface available.')
    from LHCbDIRAC.Interfaces.API.DiracLHCb import DiracLHCb
    from DIRAC.Core.Utilities.Adler import fileAdler
    if DiracLHCb().checkSEAccess(logSE_name, access='Read'):
        mlog.debug('LHCbDIRAC Python interface working and Log SE readable.')
except ImportError as exx:
    mlog.error('Please, set up \x1b[1mLHCbDirac\x1b[0m environment before running this script.', exc_info=True)
    sys.exit(1)


def loginit(logname, modver, parent='lbMcST', glevel=None):
    # TODO: Consider exporting this from package config module
    global mlog
    loggerName = logname
    if parent is not None and len(parent) > 0:
        loggerName = "{}.{}".format(parent, logname)
    mlog = logging.getLogger(loggerName)
    llevel = logging.INFO
    if glevel is not None:
        llevel = glevel
    if mlog.parent is not None:
        llevel = mlog.parent.level
    else:
        logging.basicConfig(format="[%(asctime)s] %(name)s - %(levelname)s: %(message)s",
                            datefmt='%a, %d.%m.%Y @ %H:%M:%S', level=llevel)
    mlog.setLevel(llevel)
    mlog.debug('Module %s v. %s loaded.' % (logname, modver))


def findFileDups(wkdir, flist):
    """
    Find duplicates using DIRAC swift Adler hashing.
    Args:
        wkdir (str): Path to local directory where files in list are stored
        flist (str): List of files to be checked for duplicates

    Returns:
    Dictionary mapping Adler hashes to list of files w/ same check sum. If length of list > 1 duplicates exist.
    """
    dupMap = {}
    for ifn in flist:
        fpath = os.path.join(wkdir, ifn)
        if not os.path.isfile(fpath):
            continue
        fhash = fileAdler(fpath)
        if fhash in dupMap:
            dupMap[fhash].append(ifn)
        else:
            dupMap[fhash] = [ifn, ]
    return dupMap


def processResponse(rdict, logger=mlog):
    ret = None
    msg = ''
    if rdict is None or not isinstance(rdict, dict):
        logger.error('Invalid argument \'rdict\' value: %s' % (str(rdict)))
        return ret
    if 'OK' not in rdict or 'Value' not in rdict:
        logger.warning('Invalid DIRAC response received:\n%s\n' % (str(rdict)))
        return ret
    if rdict['OK']:  # OK refers to success of DIRAC call not really operation!
        ret = rdict['Value']
    else:
        msg += 'DIRAC call has failed'
        if 'Message' in rdict:
            msg += '%s: %s' % (msg, rdict['Message'])
        logger.error(msg)
        return ret
    if 'Successful' not in ret and 'Failed' not in ret:
        return ret
    if len(ret['Failed']) > 0:
        msg += 'DIRAC subsystem failure:\n%s\n' % (str(ret['Failed']))
        logger.warning(msg)
        return None
    if len(ret['Successful'].items()) < 1 or \
            len(tuple(ret['Successful'].items())[0]) < 2:
        logger.warning('DIRAC empty response:\n%s' % str(ret))
        return None
    return ret


def getAccessibleLFNs(dslice, getSubDirs=False):
    # dslice is a dict with 'Files' and 'SubDirs' keys
    # mlog.debug(str(dslice))
    if getSubDirs:
        if 'SubDirs' not in dslice:
            return []
        if len(dslice['SubDirs']) == 0:
            return []
        td = dslice['SubDirs']
        tvlfns = [ll for ll in td.keys() if all([x in td[ll] and td[ll][x] for x in ('Accessible', 'Directory')])]
        vlfns = []
        for lfp in tvlfns:
            if lfp.endswith('/.'):
                lfp = lfp.rstrip('/.')
            vlfns.append(lfp)
        return vlfns
    if 'Files' not in dslice:
        return []
    if len(dslice['Files']) == 0:
        return []
    td = dslice['Files']
    vlfns = [ll for ll in td.keys() if all([x in td[ll] and td[ll][x] for x in ['Accessible', 'File']])]
    return vlfns


def runExternalCmd(cmdList, exectimeout=300, capture_streams=False, logger=mlog):
    """
    Runs a shell command.
    Args:
        cmdList (list): command line broken in tokens
        exectimeout (integer): time-out for command execution (triggers KILL)
        capture_streams (bool): controls whether to copy output and error streams in return
        logger (logging.logger): default logging object to output log messages to

    Returns: Tuple of return code, output stream and error stream

    """
    assert isinstance(cmdList, tuple) or isinstance(cmdList, list)
    if len(cmdList) < 1:
        logger.warning('Cannot run empty command.')
        return 999, None, None
    logger.debug('Running external command: %s' % ' '.join(cmdList))
    logger.disabled = True
    
    def killer(proc):
        print('External process #%d will be killed as execution timeout exceeded.' % proc.pid)
        proc.kill()
        sys.stdout.flush()
    retc = 999
    stdout, stderr = None, None
    if capture_streams:
        p = sp.Popen(cmdList, stdout=sp.PIPE, stderr=sp.PIPE)
    else:
        p = sp.Popen(cmdList)
    wtimer = Timer(exectimeout, killer, [p])
    try:
        wtimer.start()
        if capture_streams:
            stdout, stderr = p.communicate()
        else:
            while p.returncode is None:
                p.poll()
                sleep(0.5)
    except OSError as ose:
        logger.disabled = False
        logger.error('External command failed:', exc_info=True)
    finally:
        logger.disabled = False
        wtimer.cancel()
        if p is not None:
            retc = p.returncode
    return retc, stdout, stderr


def cmdCheckProxy2():
    rc = runExternalCmd(['dirac-proxy-info', '-v'], exectimeout=180)
    return rc[0] != 0


def cmdCheckProxy(logger=mlog):
    dirac_cmd = ['dirac-proxy-info', '-v']
    p = sp.Popen(dirac_cmd)
    sts = datetime.now()
    bAbort = False
    try:
        while p.returncode is None:
            p.poll()
            sleep(1.5)
            if (datetime.now() - sts).seconds > 120:
                logger.warning('Proxy query exceeded limit of 2 minutes. Aborting...')
                if p.returncode is not None:
                    p.kill()
                bAbort = True
    except OSError as ex:
        logger.error('Failed to communicate to LHCb DIRAC sub-system. STOP', exc_info=True)
        sys.exit(1)
    return bAbort or p.returncode != 0


def cmdInitProxy2():
    rc = runExternalCmd(['dirac-proxy-init', ], exectimeout=420)
    return rc[0] != 0


def cmdInitProxy(logger=mlog):
    dirac_cmd = ['dirac-proxy-init', ]
    p = sp.Popen(dirac_cmd)
    sts = datetime.now()
    k = 0
    bAbort = False
    while p.returncode is None:
        p.poll()
        sleep(1.5)
        if k == 2:
            logger.warning('Something goes wrong with Dirac proxy initialization. Aborting...')
            p.kill()
            bAbort = True
            continue
        if (datetime.now() - sts).seconds > 300:
            logger.warning('Proxy init exceeded limit of 5 minutes. Retry...')
            p.kill()
            k += 1
            p = sp.Popen(dirac_cmd)
    return bAbort or p.returncode != 0


def getReqIdsForProds(
        prod_ids: (int, [int])
) -> {str: [{'pid': int, 'type': str}]}:
    """
    Args:
        prod_ids (): List of production IDs for which request IDs are required

    Returns:
        A dict mapping RequestIDs to list of dict describing associated productions
    """
    from DIRAC.Core.Base import Script
    from DIRAC.TransformationSystem.Client.TransformationClient import TransformationClient
    Script.initialize(ignoreErrors=True, enableCommandLine=False)
    if isinstance(prod_ids, int):
        prod_ids = [prod_ids, ]
    trc = TransformationClient()
    rr = trc.getTransformations({"TransformationID": prod_ids, 'Status': ['Archived', 'Completed', 'Idle', 'Cleaned']})
    rqdic = {}
    if not rr["OK"]:
        mlog.error(rr["Message"])
        return None
    ts = rr['Value']
    if len(ts) == 0:
        mlog.error('None of the production IDs was matched in the LHCbDIRAC system.')
        return []
    for tr in ts:
        pid = tr['TransformationID']
        if pid not in prod_ids:
            continue
        nrq = tr["TransformationFamily"]
        if nrq not in rqdic:
            rqdic[nrq] = []
        rqdic[nrq].append({"pid": pid, "type": tr["Type"]})
    return rqdic


def getSimProdIdForRequest(
        rqIds: (int, [int])
) -> {str: [{'pid': int, 'type': str}]}:
    """
    Args:
        rqIds (): List of request IDs (also known as production IDs in DIRAC jargon)

    Returns:
        A dict of Production(a.k.a. Transformation) IDs to the MC*Simulation steps in the given production(s).
        Transformation ID is None if error occured for a given request ID. Empty list means no Simulation step
        was found for given request ID.
    """
    from DIRAC.Core.Base import Script
    from DIRAC.TransformationSystem.Client.TransformationClient import TransformationClient

    Script.initialize(ignoreErrors=True, enableCommandLine=False)

    # variants of Type from LHCbDirac: MCSimulation, MCFastSimulation so r'MC[a-z]*Simulation' w/ re.I
    if isinstance(rqIds, int):
        rqIds = [rqIds, ]
    trc = TransformationClient()
    rr = trc.getTransformations({"TransformationFamily": rqIds, 'Status': ['Archived', 'Completed', 'Idle', 'Cleaned']})
    matchedReq = {}
    if not rr["OK"]:
        mlog.error(rr["Message"])
        return None
    ts = rr['Value']
    if len(ts) == 0:
        mlog.error('None of the request/production IDs was matched in the LHCbDIRAC system.')
        return []
    for tr in ts:
        irq = tr['TransformationFamily']
        if irq not in matchedReq:
            matchedReq[irq] = []
        if re.match(r'MC[a-z]*Simulation', tr['Type'], re.I) is None:
            continue
        matchedReq[irq].append({'pid': tr['TransformationID'], 'type': tr['Type']})
    for ir in rqIds:
        sir = str(ir)  # LbDIRAC returns TransformationFamily ids as str
        if sir not in matchedReq:
            mlog.warning('Request ID {} not found in the LHCbDIRAC system.'.format(sir))
            continue
        if len(matchedReq[sir]) == 0:
            mlog.warning('No MC*Simulation production found for request {}. Maybe it is a WG production?!'.format(sir))
            del matchedReq[sir]
    return matchedReq


# noinspection PyUnresolvedReferences
def ls_SE_path(prodLFN, seName=logSE_name, logger=mlog, getResponse=False):
    if len(prodLFN) == 0:
        logger.error('LFN location cannot be empty string.')
        return False
    # from DIRAC.Core.Base.Script import parseCommandLine
    from DIRAC.Core.Base import Script
    # parseCommandLine()
    Script.initialize(ignoreErrors=True, enableCommandLine=False)
    from DIRAC.Resources.Storage.StorageElement import StorageElement
    logSE = StorageElement(seName)
    # find run directory LFNs
    dd = processResponse(logSE.listDirectory(prodLFN), logger=logger)
    if getResponse:
        return dd
    else:
        print(dd)
        sys.stdout.flush()
    return True


def getUniqueLocalFilename(cLFN, lPath, zfname, runid, prodid):
    jobid, ext = os.path.splitext(zfname)
    if runid is None or jobid is None:
        mlog.debug('LFN: %s\nProdID: %s, RunId: %s, JobId: %s' % (cLFN, prodid, runid, jobid))
    if runid is None:
        runid = "000a"
    if jobid is None:
        jobid = "000x"
    # if archive file is missing extension put ZIP as default
    if len(ext) == 0 or ext == '.':
        mlog.debug('No extension for archive %s. Assuming a ZIP file.' % cLFN)
        ext = '-miss.zip'
    lzfname = '_'.join([prodid, runid, jobid]) + ext
    if not os.path.exists(os.path.join(lPath, lzfname)):
        return lzfname
    j = 1
    lzfname = '_'.join([prodid, runid, jobid, "%03d" % j]) + ext
    while os.path.exists(os.path.join(lPath, lzfname)):
        j += 1
        lzfname = '_'.join([prodid, runid, jobid, "%03d" % j]) + ext
    return lzfname


def findDownloadLogLFN(containerLFN, localPath, targetFiles=(logBaseName + '.xml', logBaseName + '.xml.gz'),
                       seName=logSE_name, logger=mlog):
    ret = []
    p = containerLFN.find('/LOG/')
    if p == -1:
        logger.debug('Not a LFN for LOGs: %s' % containerLFN)
        return ret
    rpath = containerLFN[p+5:]
    ptoks = rpath.split(os.path.sep)
    prodid, runid, jobid = None, "000a", None
    if ptoks[0].isdigit() and len(ptoks[0]) == 8:
        prodid = ptoks[0]
    if ptoks[1].isdigit() and len(ptoks[1]) == 4 and ptoks[1].startswith('0'):
        runid = ptoks[1]
    if len(ptoks) == 3:
        zfname = ptoks[2]
    elif len(ptoks) > 3:
        zfname = '_'.join(ptoks[2:])
    else:
        zfname = os.path.split(rpath)[1]
    if len(zfname) == 0:
        logger.debug('Not a file LFN received as argument: %s' % containerLFN)
        return ret
    jobid, ext = os.path.splitext(zfname)
    # Try filter mass collected LFNs
    if all([lambda ztx: zfname.find(ztx) == -1 for ztx in targetFiles]) and \
            len(re.sub(r'[0-9_]*', r'', jobid)) > 0:
        logger.debug('Ignoring LFN detected as archive: %s' % containerLFN)    # should comment in release!
        return ret
    lzfname = getUniqueLocalFilename(containerLFN, localPath, zfname, runid, prodid)
    if getSEfile(containerLFN, localPath, seName=seName, localFilename=lzfname):
        logger.debug('Failed to retrieve LFN: %s' % containerLFN)
        return []
    lzPath = os.path.join(localPath, lzfname)
    lzbPath = ''
    try:        # treat all archives as ZIP regardless of extension
        zf = zipfile.ZipFile(lzPath, 'r')
        for fi in zf.filelist:
            epath, fbase = os.path.split(fi.filename)
            if any([fbase.find(xname) > -1 for xname in targetFiles]):
                zf.extract(fi, localPath)
                # local XML log name
                xmlPath = os.path.join(localPath, fi.filename)
                suff = ''
                (gfbase, fext) = os.path.splitext(fbase)
                if fext.endswith('gz'):
                    suff = '.tgz'
                # make sure log name starts always w/ logBaseName
                if not gfbase.startswith(logBaseName) or not gfbase.endswith(logBaseName):
                    ip = gfbase.find(logBaseName)
                    npfx = gfbase[0:ip].strip('\\/*- ')
                    ip += len(logBaseName)
                    nsfx = ''
                    if ip < len(gfbase):
                        nsfx = gfbase[ip:].strip('\\/*- ')
                    gfbase = '-'.join((logBaseName, npfx, nsfx))
                    gfbase = gfbase.strip('-').replace('--', '-')
                newxfname = '-'.join([gfbase, runid, jobid]) + '.xml' + suff
                newXmlPath = os.path.join(localPath, newxfname)
                os.rename(xmlPath, newXmlPath)
                ret = ['file://' + newXmlPath, ]
                # get the first path element in XML file path to use in removedirs
                ptoks = fi.filename.strip(os.path.sep).split(os.path.sep)
                if len(ptoks) > 0:
                    lzbPath = os.path.join(localPath, ptoks[0])
                break
            else:
                if fbase.find('GeneratorLog') > -1:
                    logger.warning('New XML log file name extension: %s::%s.\nPlease, ask developer to update targets.'
                                   % (containerLFN, fi.filename))
        zf.close()
    except Exception as exa:
        logger.error('{0}:'.format(sys.exc_info()[0]), exc_info=True)
    # clean up
    sleep(0.1)        # wait for sluggish file system ops
    if os.path.exists(lzPath):
        os.unlink(lzPath)
    if os.path.exists(lzbPath):
        # logger.debug('Removing archive unzip tree at %s...' % lzbPath)
        try:
            os.removedirs(lzbPath)
        except OSError as ose:
            logger.error('Could not remove directory tree.', exc_info=True)
    if len(ret) == 0:
        logger.debug('No GeneratorLog.xml* in LFN %s' % containerLFN)
    return ret


def isArchivePath(pth, getFilename=False):
    """
    Function primarily used to veto file LFNs w/ some simple file name pattern matching for archives.
    """
    ret = False
    fn = os.path.split(pth)[1]
    if len(fn) < 5:
        return False
    if any([fn.lower().endswith(x) for x in ['.zip', '.tgz', '.tar', '.tar.gz', '.gz']]):
        ret = True
    fbn, fex = os.path.splitext(fn)
    # deal with ZIPs w/o extension for web server auto-unpacking
    if fbn.isdigit() and len(fbn) == 8 and len(fex.strip('.')) == 0:
        ret = True
    if getFilename:
        ret = (ret, fn)
    return ret


# noinspection PyUnresolvedReferences
def get_LFNs_on_SE2(prodLFN, seName=logSE_name, targetFiles=('GeneratorLog.xml.gz', 'GeneratorLog.xml'), logger=mlog,
                    nbLimit=1000, localPath=None):
    if len(prodLFN) == 0 or not isinstance(nbLimit, int) or nbLimit < 100:
        return []
    if targetFiles is None or len(targetFiles) == 0:
        logger.error('Cannot validate LFNs with an empty list of target files.')
        return []
    from DIRAC.Core.Base import Script
    Script.initialize(ignoreErrors=True, enableCommandLine=False)
    from DIRAC.Resources.Storage.StorageElement import StorageElement
    logSE = StorageElement(seName)
    NBLIMIT_TOLLERANCE = 1.15
    NBLFN_MAX = int(float(nbLimit) * NBLIMIT_TOLLERANCE)
    NBLFN_LIMIT = int(float(NBLFN_MAX)*1.1)
    prodName = os.path.split(prodLFN)[1]
    xLFNs = []              # xml(.gz) LFNs
    dLFNs = [prodLFN, ]     # directory LFNs
    aLFNs = []              # archived logs LFNs
    j = 0
    logger.info('Discovering XML files on Dirac SE...')
    tsop = datetime.now()
    tsv = tsop
    while j < len(dLFNs):
        tlfn = dLFNs[j]
        j += 1
        logger.debug('Listing directory LFN: %s' % tlfn)
        dd = processResponse(logSE.listDirectory(tlfn), logger=logger)
        if dd is None:
            continue
        # if 'Successful' not in dd or len(dd['Successful'].items()) < 1 or len(dd['Successful'].items()[0]) < 2:
        #    logger.error('Unsatisfactory response at %s:\n%s' % (tlfn, str(dd)))
        #    continue
        dpl = tuple(dd['Successful'].items())[0][1]
        dLFNs += getAccessibleLFNs(dpl, getSubDirs=True)
        fLFNs = getAccessibleLFNs(dpl)
        if len(fLFNs) > 0:
            xLFNs += [xfn for xfn in fLFNs if any([xfn.endswith(target) for target in targetFiles])]
            # all other files are considered archives
            aLFNs += [xfn for xfn in fLFNs if xfn not in xLFNs and isArchivePath(xfn)]
        if len(aLFNs) + len(xLFNs) > NBLFN_LIMIT:
            break
        if (datetime.now()-tsv).seconds > 15:
            tsv = datetime.now()
            logger.info('Found %d XML LFNs, %d archives. Queued %d directories...' %
                        (len(xLFNs), len(aLFNs), len(dLFNs)-j))
    if len(aLFNs) > 0 and len(xLFNs) < NBLFN_MAX:
        logger.info('Search and extract XMLs from %d archive LFNs...' % (len(aLFNs)))
        if localPath is None or not os.path.isdir(localPath):
            logger.error('Invalid or no disk path given for XML extraction.')
        else:
            for zlfn in aLFNs:
                xLFNs += findDownloadLogLFN(zlfn, localPath)   # pname=prodName)
                ff = float(len(xLFNs))/float(NBLFN_MAX)*100.
                if (datetime.now() - tsv).seconds > 15:
                    tsv = datetime.now()
                    logger.info('Extraction progress: %3.1f %%' % ff)
                if len(xLFNs) > NBLFN_MAX:
                    break
    logger.debug('%d valid LFNs found under %s:%s' % (len(xLFNs), seName, prodLFN))
    return xLFNs


def get_LFNs_on_SE(prodLFN, seName=logSE_name, targetFiles=('GeneratorLog.xml.gz', 'GeneratorLog.xml'), logger=mlog,
                   nbLimit=1000, localPath=None):
    if len(prodLFN) == 0 or not isinstance(nbLimit, int) or nbLimit < 100:
        return []
    if targetFiles is None or len(targetFiles) == 0:
        logger.error('Cannot validate LFNs with an empty list of target files.')
        return []
    # from DIRAC.Core.Base.Script import parseCommandLine
    from DIRAC.Core.Base import Script
    # parseCommandLine()
    Script.initialize(ignoreErrors=True, enableCommandLine=False)
    from DIRAC.Resources.Storage.StorageElement import StorageElement
    logSE = StorageElement(seName)
    NBLIMIT_TOLLERANCE = 1.15
    # find run directory LFNs
    prodName = os.path.split(prodLFN)[1]
    logger.debug('Listing directory LFN: %s' % prodLFN)
    dd = processResponse(logSE.listDirectory(prodLFN), logger=logger)
    if dd is None:
        return []
    if 'Successful' not in dd or len(dd['Successful'].items()) < 1 or len(dd['Successful'].items()[0]) < 2:
        logger.error('Unsatisfactory response at %s:\n%s' % (prodLFN, str(dd)))
        return []
    runLFNs = getAccessibleLFNs(dd['Successful'].items()[0][1], getSubDirs=True)
    if len(runLFNs) == 0:
        logger.error('Did not find any valid production run LFNs.')
        return []
    jobLFNs = []
    if len(runLFNs) > 1:
        # get latest XMLs first to use as template for statistics merging
        # a 'knob' may be needed to be able to avoid picking up 'BAD' XMLs as template
        runLFNs.sort(reverse=True)
    logger.debug('Found %d run dirs under %s...' % (len(runLFNs), prodLFN))
    # find accessible job LFNs with LOGs
    for prun in runLFNs:
        logger.debug('Getting job dirs and zip files in %s. This may take a long time ...' % prun)
        dd = processResponse(logSE.listDirectory(prun), logger=logger)
        if dd is None:
            continue
        if 'Successful' not in dd or len(dd['Successful'].items()) < 1 or len(dd['Successful'].items()[0]) < 2:
            logger.warning('Unsatisfactory response in %s:\n%s' % (prun, str(dd)))
            continue
        dval = dd['Successful'].items()[0][1]
        tlfn = getAccessibleLFNs(dval, getSubDirs=True)
        if len(tlfn) == 0:
            logger.warning('Could not find any job LFNs under %s' % prun)
        else:
            jobLFNs += tlfn
        tlfn = getAccessibleLFNs(dval)
        logger.debug('Found %d file LFNs under %s' % (len(tlfn), prun))
        for lfn in tlfn:
            r = isArchivePath(lfn, True)
            if not r[0]:
                continue
            (fn, ext) = os.path.splitext(r[1])
            if not fn.isdigit():  # 0-padded jobid as filename
                logger.debug('Ignoring file w/ LFN %s' % lfn)
                continue
            jobLFNs.append(lfn)
        # break if found job LFNs exceeds satisfactorily the limit on number of XMLs to merge
        # as one does not know apriori how many XMLs will be valid for merging take a large tolerance factor
        if len(jobLFNs) >= int(float(nbLimit)*1.1*NBLIMIT_TOLLERANCE):
            break
    jcount = len(jobLFNs)
    logger.debug('Found %d job LFNs for production LFN %s' % (jcount, prodLFN))
    if nbLimit is not None:
        if jcount > nbLimit:
            logger.info('Will validate %d out of %d possible XML-log LFNs...' %
                        (int(NBLIMIT_TOLLERANCE*nbLimit), jcount))
        else:
            logger.info('Will validate all %d possible XML-log LFNs...' % jcount)
    pstart = datetime.now()
    tvar = pstart
    # find valid LFNs for XML logs
    validLFNs = []
    max_nbLimit = int(float(nbLimit)*NBLIMIT_TOLLERANCE)
    for j in range(jcount):
        # 30% margin for erroneous transfers!
        if len(validLFNs) >= max_nbLimit:
            break
        lfn = jobLFNs[j]
        # logger.debug('Processing LFN path: %s' % (lfn))
        jobName = os.path.split(lfn)[1]
        if (datetime.now()-tvar).seconds > 15:
            tvar = datetime.now()
            logger.info('Validated %d/%d LFNs...' % (j, jcount))
        if isArchivePath(lfn):  # .../ProdID/runID/jobID.zip !
            if localPath is not None:
                validLFNs += findDownloadLogLFN(lfn, localPath)
            else:
                logger.debug('Local path not specified to download archived LFN %s' % lfn)
            continue
        # assuming LFN is job directory
        dd = processResponse(logSE.listDirectory(lfn), logger=logger)
        if dd is None:
            logger.debug('No LFNs under %s ...' % lfn)
            continue
        if 'Successful' not in dd or len(dd['Successful'].items()) < 1 or len(dd['Successful'].items()[0]) < 2:
            logger.error('Unsatisfactory response for LFN %s:\n%s' % (lfn, str(dd)))
            continue
        jfLFNs = getAccessibleLFNs(dd['Successful'].items()[0][1])
        flfns = [xfn for xfn in jfLFNs if any([xfn.endswith(target) for target in targetFiles])]
        zlfns = [tfn for tfn in jfLFNs if tfn not in flfns and isArchivePath(tfn)]
        if len(flfns) + len(zlfns) != len(jfLFNs):
            logger.debug('In LFN directory %s located %d XMLs(.gz) and %d archived LOGs out of %d listed.' %
                         (lfn, len(flfns), len(zlfns), len(jfLFNs)))
        if len(flfns) > 0:
            validLFNs += flfns
        else:
            # here we actually need localPath to pre-download XML files if found
            if localPath is not None:
                #  process all archive files in /ProdID/runID/jobID/*.{tar,zip,tgz,...}
                for i in range(len(zlfns)):
                    validLFNs += findDownloadLogLFN(zlfns[i],  localPath)
                    if (datetime.now() - tvar).seconds > 15:
                        tvar = datetime.now()
                        logger.info('Validated and downloaded archived LFNs: %d/%d' % (len(validLFNs), jcount))
                    if len(validLFNs) >= max_nbLimit:
                        break
            else:
                logger.debug('Don\'t know where to download locally LFNs:\n%s' % '\n'.join(zlfns))
    logger.debug('%d valid LFNs found under %s:%s' % (len(validLFNs), seName, prodLFN))
    return validLFNs


def parseProdInfo(prodID, piRet):
    # parsing metadata from API calls in dirac_getProdInfo2
    mdict = {}
    try:
        mdict['diracOK'] = piRet['OK']
        mdict['prodID'] = prodID
        if piRet['OK']:
            val = piRet['Value']
            steps = val['Steps']  # could be a basestring only - parsing not implemented in this case
            if 'Number of jobs' in val and len(val['Number of jobs']) > 0:
                nbjd = val['Number of jobs'][0]
                if len(nbjd) > 0:
                    mdict['NbJobsStep1'] = nbjd[0]
            if 'Path' in val:
                mdict['BKKPath'] = val['Path'].strip('/')
            infs = None
            if 'Production informations' in val:
                infs = val['Production informations']
            if 'Production information' in val:
                infs = val['Production information']
            if infs is not None:
                for inf in infs:
                    if inf[0] == 'MC' or inf[2] is not None:
                        # [0] ConfigurationName; [1] ConfigurationVersion; [2] EventType
                        mdict['eventType'] = str(inf[2])
                        mdict['simYear'] = str(inf[1])
                        break
            for st in steps:
                # [0] StepName; [1] ApplicationName; [2] ApplicationVersion; [3] OptionFiles
                # [4] DDDB; [5] CONDDB; [6] ExtraPackages; [7] StepId; [8] Visible (Y|N)
                if st[1] == 'Gauss':
                    mdict['Gauss_version'] = st[2]
                    for vvv in [x for x in st if isinstance(x, six.string_types) and x.lower().startswith('sim')]:
                        if re.match(r'sim\d*[a-z]?-.+', vvv, re.I) is not None:
                            mdict['SIMCOND'] = vvv
                            break
                    ddbtags = [x for x in st if isinstance(x, six.string_types) and x.lower().startswith('dddb-')]
                    if len(ddbtags) == 1:
                        mdict['DDDB'] = ddbtags[0]
                    else:
                        mlog.error("Multiple/No DDDB tag candidates found in LHCbDIRAC metadata. "
                                   "Please, contact developers to patch extraction criteria.")
                    for extra_package in st[6].split(';'):
                        packToks = extra_package.split('.')
                        if len(packToks) < 2:
                            continue
                        if packToks[0].find('AppConfig') > -1:
                            mdict['APPCONFIG_version'] = packToks[1]
                        if packToks[0].find('DecFiles') > -1:
                            mdict['DecFiles_version'] = packToks[1]
                    break
            tks = val['Path'][1:].split('/')
            if tks[0] != 'MC':
                mlog.error('Production #%d does not appear to be MC*Simulation !!!' % prodID)
            if 'simYear' in mdict and tks[1] != mdict['simYear']:
                mlog.warning('BKK path for prod. #%d under different year: %s(mdata) vs. %s(bkk)' %
                          (prodID, mdict['simYear'], tks[1]))
            if 'simYear' not in mdict:
                mdict['simYear'] = tks[1]
            appcfg = ['', '']
            for tk in tks:
                if tk.startswith('Beam'):
                    appcfg[1] = tk
                if tk.startswith('Sim'):
                    mm = re.match(r'(Sim[0-9]+).*', tk)
                    if mm is None:
                        raise ValueError('Invalid Sim id: %s.' % tk)
                    appcfg[0] = mm.group(1)
                    mdict['simVariant'] = mm.group(0)
                if all([len(ss) != 0 for ss in appcfg]):
                    break
            mdict['APPCONFIG_file'] = '-'.join(appcfg)
        else:
            mlog.error("Prod. #%d: %s" % (prodID, piRet['Message']))
    except Exception as aex:
        mlog.error("ERROR: ", exc_info=True)
    mlog.debug("Parsed prod. info dict: " + str(mdict))
    return mdict


# noinspection PyUnresolvedReferences
def dirac_getProdInfo2(prodIDs, logger=mlog, rawresponse=False):
    """
    Based on code in dirac-production-information
    """
    from DIRAC.Core.Base import Script
    ret = {}
    singleId = isinstance(prodIDs, int)
    if singleId:
        pids = (prodIDs, )
    else:
        pids = tuple(prodIDs)
    try:
        # from dirac-bookkeeping-production-information.py
        Script.initialize(ignoreErrors=True, enableCommandLine=False)
        from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
        bk = BookkeepingClient()
        sleep(0.5)
        for pid in pids:
            rr = bk.getProductionInformation(pid)
            if rawresponse:
                rdd = processResponse(rr, logger=logger)
            else:
                rdd = parseProdInfo(pid, rr)
            if singleId:
                ret = rdd
            else:
                ret[pid] = rdd
            sleep(0.1)
    except Exception as erx:
        logger.error("{0}:".format(str(erx.__class__)), exc_info=True)
    return ret


def parseTrWkflow(midic, issim=True):
    import xml.dom.minidom as mxdom
    xdoc = mxdom.parseString(midic['Body'])
    bkNames = ['configName', 'configVersion', 'conditions', 'groupDescription']
    altSimKeys = ['StepProcPass', 'ProcessingType']
    pNames = ['extraPackages', 'DDDBTag', 'CondDBTag', 'applicationName', 'applicationVersion',
              'eventType'] + bkNames
    pNames += altSimKeys
    pid = midic['prodID']
    for xe in filter(lambda xe: xe.hasAttribute(u"name"), xdoc.getElementsByTagName("Parameter")):
        xname = str(xe.getAttribute(u'name'))
        if xname not in pNames:
            continue
        if xe.firstChild is None or xe.firstChild.tagName != u'value':
            mlog.warning('Parameter %s w/o value child.' % xname)
            continue
        if xe.firstChild.firstChild is None:
            xval = xe.firstChild.nodeValue
        else:
            if xe.firstChild.firstChild.nodeType not in (mxdom.Element.CDATA_SECTION_NODE, mxdom.Element.TEXT_NODE):
                mlog.warning('PID %d: Value of parameter %s not in CDATA section/ TEXT node (%d).' %
                             (pid, xname, xe.firstChild.firstChild.nodeType))
                mlog.debug('Data:\n%s' % xe.toprettyxml(indent='  '))
                continue
            xval = xe.firstChild.firstChild.nodeValue
        if not isinstance(xval, six.string_types) or len(xval) == 0:
            mlog.warning('PID %d: Empty node value for parameter \'%s\'' % (pid, xname))
            mlog.debug('Data:\n%s' % xe.toprettyxml(indent='  '))
            continue
        if xname not in midic:
            midic[xname] = str(xval)
        else:
            mlog.info('PID %d: Will not overwrite existing parameter %s w/ new value %s (vs. old: %s)' %
                      (pid, xname, xval, midic[xname]))
    nullpnames = [kx for kx in pNames if kx not in midic]
    if len(nullpnames) > 0:
        mlog.error('For prod. #%(prodID)d following keys are missing in metadata: ' % midic +
                   ', '.join(nullpnames))
        return
    midic['NbJobsStep1'] = -1
    if midic['groupDescription'].find('/') > -1:
        midic['groupDescription'] = midic['groupDescription'].strip('/').split('/')[0]
    bkpath = [midic[xn].strip('/') for xn in bkNames]
    if bkpath[0].lower() != u'mc':
        mlog.warning('Cannot reconstruct BKPath from JDL for prod. #%(prodID)d' % midic)
    else:
        midic['BKKPath'] = '/'.join(bkpath)
        midic['simYear'] = midic['configVersion']
    if 'applicationName' not in midic or midic['applicationName'] != u'Gauss':
        mlog.warning('Application name not GAUSS for prod. #%(prodID)d' % midic)
    else:
        midic['Gauss_version'] = midic['applicationVersion']
    midic['DDDB'] = midic['DDDBTag']
    midic['SIMCOND'] = midic['CondDBTag']
    del midic['Body']
    # mlog.debug('DEBUG data: %s' % str(midic))
    resimv = r'(Sim\d{2,}).*'
    mm = re.match(resimv, midic['groupDescription'].strip('/'))
    if mm is None:
        for ik in range(len(altSimKeys)):
            altsimk = altSimKeys[ik]
            if altsimk not in midic:
                continue
            if not midic[altsimk].lower().startswith('sim'):
                continue
            altsimval = midic[altsimk].strip('/').split('/')[0]
            mm = re.match(resimv, altsimval)
            if mm is not None:
                mlog.debug('PID #%d: Match Sim variant: %s.' % (pid, str(mm.groups())))
                break
    if mm is None:
        raise ValueError('Invalid Sim id: %(groupDescription)s.' % midic)
    midic['simVariant'] = mm.group(0)
    midic['APPCONFIG_file'] = '%s-%s' % (mm.group(1), midic['conditions'].strip('/'))
    epNames = {'AppConfig': 'APPCONFIG_version', 'DecFiles': 'DecFiles_version'}
    for extrapkg in midic['extraPackages'].split(';'):
        if extrapkg.startswith('ProdConf'):
            continue
        pnv = extrapkg.split('.')
        if pnv[0] in epNames:
            midic[epNames[pnv[0]]] = pnv[1]
    # clean up dictionary
    for nkey in pNames:
        if nkey == 'eventType':
            continue
        del midic[nkey]
    midic['diracOK'] = True


def dirac_getTransformationInfo(prodIDs, logger=mlog):
    """
    Based on code in dirac-transformation-information
    """
    from DIRAC.Core.Base import Script
    from LHCbDIRAC.TransformationSystem.Client.TransformationClient import TransformationClient
    
    ret = {}
    singleId = isinstance(prodIDs, int)
    if singleId:
        pids = (prodIDs, )
    else:
        pids = tuple(prodIDs)
    try:
        # from dirac-bookkeeping-production-information.py
        Script.initialize(ignoreErrors=True, enableCommandLine=False)
        trc = TransformationClient()
        for pid in pids:
            try:
                sleep(0.5)
                res = trc.getTransformation(pid)
                idetails = ['Status', 'Type', 'TransformationFamily', 'Body']
                rdd = {'prodID': pid}
                isSim = True
                for det in idetails:
                    param = res.get('Value', {}).get(det, 'Unknown')
                    # returns detail attribute or default "Unknown" in script when detail key is missing
                    rdd[det] = str(param.strip())
                    # if det == "Type" and not param.lower().endswith('simulation'):
                    #    break
                if 'Type' in rdd and not rdd['Type'].lower().endswith('simulation'):
                    logger.error('ID #%d not corresponding to MC*Simulation production. (%s)' % (pid, rdd['Type']))
                    logger.debug('DEBUG data: %s' % (str(rdd)))
                    isSim = False
                    continue
                if 'Body' not in rdd:
                    logger.error('Could not retrieve JDL for Prod. #%d.' % pid)
                    continue
                if not isSim:
                    fp = open('./jdl-P%d.xml' % pid, 'w')
                    fp.write('<?xml version = "1.0">\n')
                    fp.write(rdd['Body'])
                    fp.flush()
                    fp.close()
                    mlog.debug('written JDL to disk for prod. #%d' % pid)
                parseTrWkflow(rdd, issim=isSim)
                # print(str(rdd))
                if singleId:
                    ret = rdd
                else:
                    ret[pid] = rdd
            except Exception as irx:
                logger.error("Prod. #{:d}".format(pid), exc_info=True)
                raise irx
    except Exception as erx:
        logger.error("{0}:".format(str(erx.__class__)), exc_info=True)
        raise erx
    return ret


def getSEfile(lfn, localPath, seName=logSE_name, localFilename=None):
    """
    Download file from LogSE and check file size matches on local storage(memory).
    """
    if not os.path.isdir(localPath):
        return True
    from DIRAC.Core.Base import Script
    # parseCommandLine()
    Script.initialize(ignoreErrors=True, enableCommandLine=False)
    from DIRAC.Resources.Storage.StorageElement import StorageElement
    logSE = StorageElement(seName)
    rr = processResponse(logSE.getFile(lfn, localPath))
    if rr is None or len(rr['Failed']) > 0:
        mlog.debug(str(rr))
        return True
    fname = os.path.split(lfn)[1]
    lfPath = os.path.join(localPath, fname)
    if not os.path.exists(lfPath):
        mlog.debug('Local file exists. Refusing to overwrite %s' % lfPath)
        return True
    if localFilename is not None:
        if localFilename != fname:
            os.rename(lfPath, os.path.join(localPath, localFilename))
            lfPath = os.path.join(localPath, localFilename)
    dsize = rr['Successful'][lfn]
    fsize = os.stat(lfPath)[stat.ST_SIZE]
    if dsize != fsize:
        mlog.debug('Incomplete file downloaded from SE:\n%s (%d bytes)\n%s (%d bytes)' % (lfn, dsize, lfPath, fsize))
        return True
    return False


def get_compressed_file_contents(fileURI, filenames=('GeneratorLog.xml', ), logger=mlog):
    """
    OBSOLETE: returns a dictionary with keys from filenames and uncompressed stream data as values
    """
    ret = {}
    zcont = get_file_contents(fileURI, logger)
    if zcont is None:
        logger.error('Error opening provided URL')
        return None
    payload = zipBytes(zcont)
    try:
        zfp = tarfile.open(fileobj=payload, mode='r:gz')
        arhNames = zfp.getnames()
        for fn in filenames:
            if fn in arhNames:
                fp = zfp.extractfile(fn)
                ret[fn] = fp.read()
                fp.close()
        zfp.close()
    except (IOError, EOFError, zlib.error, tarfile.ReadError) as err:
        logger.error(str(err), exc_info=True)
        ret = None
    payload.close()
    return ret


def get_file_contents(fileURI, logger=mlog):
    """OBSOLETE: used to retrieve file contents in memory from URI
    """
    import os
    ret = None
    if isinstance(fileURI, six.string_types) and (fileURI.startswith('/') or fileURI.find('://') == -1):
        if not os.path.isfile(fileURI):
            raise ValueError('Invalid local .xml file path.')
        fileURI = os.path.abspath(fileURI)
        fileURI = 'file://' + fileURI
    try:
        fp = urlopen(fileURI, timeout=10)
        if fp is None:
            raise Exception('Unknown handler for given location protocol.')
        nbytes = -1
        if 'content-length' in fp.info():
            nbytes = int(fp.info()['content-length'])
        ret = fp.read(nbytes)
        fp.close()
        if 0 < nbytes != len(ret):
            logger.error('Less data read from server %d/%d' % (len(ret), nbytes))
    except Exception as erx:
        logger.error("{0}:".format(sys.exc_info()[0]), exc_info=True)
    return ret
