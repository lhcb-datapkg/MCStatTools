#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright CERN for the benefit of the LHCb Collaboration                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Give the script a list of DIRAC prod IDs for which archived LOG files are previously downloaded using 
check_for_staged.py. The archives are uncompressed and generator statistics XML files are extracted locally 
for further processing."""


import os
import stat
import logging
import math
import glob
from optparse import OptionParser
import lbTarZ_utils as lbTballz
import six
# TODO: convert to using argparse and starting in main()

__version__ = '20211206'
dt_format = '%a, %d.%m.%Y @ %H:%M:%S'
ProdIDs = []

# Global options & flags
base_path = ''
runOptions = object()
runOptions.delta = 0.07
runOptions.nbLogs = 1000
runOptions.useOldAlgo = False
runOptions.cleanTarz = False

mlog = logging.getLogger('lbMcST-upk')


def parse_options():
    global runOptions
    logLevel = logging.INFO
    opt = OptionParser("usage: %prog [options] <ProdIDs>\n\n\t"
                       "<ProdIDs> : list of ProdID(s), comma-separated, with no blank spaces.\n\n"
                       "Please, use '\x1b[1m-h\x1b[0m'/'\x1b[1m--help\x1b[0m' option to get the full list of options.")
    ###
    opt.add_option("-n", "--number-of-logs",
                   action="store", type="int", dest="nbLogs",
                   default=1000,
                   metavar='<NB_LOGS>',
                   help="number of logs to download [default : %default]")
    ###
    opt.add_option("--delta-size",
                   action="store", type="float", dest="delta",
                   default=0.07,
                   metavar='<DELTA>',
                   help="log files smaller by <DELTA> from largest file of sample will be deleted/ignored "
                        "[default : %default]\nIt is used only when file size distribution is non-gaussian.")
    ###
    verb_options = ['debug', 'verbose', 'info', 'warn', 'warning', 'error', 'crit', 'critical']
    verb_options += map(lambda x:  x.upper(), verb_options)
    opt.add_option("-v", "--verb-level",
                   action='store', type='choice', dest='sLogLevel',
                   choices=verb_options,
                   default='info', metavar='<VERB_LEVEL>',
                   help='case insensitive verbosity level [CRIT, ERROR, WARN, INFO, DEBUG, VERBOSE; default: %default]')
    ###
    opt.add_option("-c", "--clean",
                   action="store_true", dest="cleanTarz",
                   default=False,
                   help="force script to remove compressed archives for processed productions; default: %default")
    ###
    opt.add_option("-k", "--compat", action="store_true", dest="useOldAlgo", default=False,
                   help="force the script to use old XML download algorithm and locations")
    ###
    opt.add_option("--usage",
                   action="store_true", dest="print_usage",
                   default=False,
                   help="show this help message and exit")
    ###
    (runOptions, args) = opt.parse_args()
    if runOptions.print_usage:
        opt.print_help()
        exit()
    if len(args) < 1:
        opt.error("Incorrect number of arguments, should be one comma-separated list of ProdID(s).")
    ###
    tt = runOptions.sLogLevel.lower()
    if tt == 'info':
        logLevel = logging.INFO
    elif tt == 'debug' or tt == 'verbose':
        logLevel = logging.DEBUG
    elif tt == 'warn' or tt == 'warning':
        logLevel = logging.WARN
    elif tt == 'error':
        logLevel = logging.ERROR
    elif tt == 'crit' or tt == 'critical':
        logLevel = logging.CRITICAL

    # exporting options to globals
    global base_path, mlog, ProdIDs
    base_path = os.path.abspath(os.curdir)
    if not os.path.isabs(base_path):
        base_path = os.path.join(os.path.abspath(os.curdir), base_path)
    ###
    logging.basicConfig(format='[%(asctime)s] %(name)s - %(levelname)s: %(message)s', datefmt=dt_format, level=logLevel)
    mlog = logging.getLogger('lbMcST')
    mlog.setLevel(mlog.parent.level)
    mlog.info('Verbosity level set to %s. Will be using UTC from here on.' % (runOptions.sLogLevel.upper()))
    # switch to UTC is most likely done when importing LHCbDIRAC
    ###
    # test for incompatible arguments ?
    # treating args
    if ',' not in args[0]:
        args[0] = args[0].replace(' ', ',')
    ProdIDs = args[0].split(',')
    ProdIDs = map(int, ProdIDs)
    ProdIDs = list(set(ProdIDs))


def getProdLocalArchives(pid, relPath=None, log=mlog):
    wkPath = base_path
    spid = None
    if relPath is not None:
        wkPath = os.path.join(base_path, relPath)
    if isinstance(pid, six.string_types):
        spid = pid
    if isinstance(pid, int):
        spid = str(pid)
    pwkPath = os.path.join(wkPath, spid)
    pdirs = glob.glob(pwkPath)
    if len(pdirs) != 1:
        log.error('Could not find working directory for prod. #%s. '
                  'Please use check_for_staged.py to download archived LOGs.' % spid)
        return []
    afiles = []
    for ext in ['tgz', 'tar.gz', 'tar']:
        afiles += glob.glob(os.path.join(pwkPath, '%s_*.%s' % (spid.zfill(8), ext)))
    return afiles


def preValidateLocalXMLs(prod_dir, mlog=mlog):
    sizes, files = [], []
    pdir_files = os.listdir(prod_dir)
    for fn in pdir_files:
        if os.path.isdir(os.path.join(prod_dir, fn)):
            continue
        if not (fn.lower().endswith('.xml') or fn.lower().endswith('.xml.tgz')):
            continue
        files.append(fn)
        sizes.append(os.stat(os.path.join(prod_dir, fn))[stat.ST_SIZE])
    # listing log files with outlier file sizes (4-3 sigma) - use runOptions.delta if not-gaussian
    if len(sizes) > 1:
        fnsizes = float(len(sizes))
        mean_size = float(sum(sizes))/fnsizes
        sig_size = sum(map(lambda x: (float(x)-mean_size)*(float(x)-mean_size), sizes))
        sig_size = math.sqrt(sig_size/(fnsizes-1.))
        sz_mean = int(mean_size)
        sz_deviation = int(4.*sig_size)
        if sz_mean - sz_deviation < 0:
            sz_deviation = int(3*sig_size)
        if sz_deviation < int(runOptions.delta * mean_size):
            sz_deviation = int(runOptions.delta * mean_size)
            
            def size_filter(x):
                return x < sz_deviation
            mlog.debug('Filtering XML files with outlier size below %d byte(s).' % sz_deviation)
        else:
            def size_filter(x):
                return x < (sz_mean-sz_deviation) or x > (sz_mean+sz_deviation)
            mlog.debug('Filtering XML files with outlier size of %d +/- %d byte(s).' % (sz_mean, sz_deviation))
        mlog.info('Prod. %d: get the sizes of the logs' % pid)
        smallFiles = []
        validFiles = []
        for i in range(0, len(files)):
            if size_filter(sizes[i]):
                smallFiles.append(files[i])
            else:
                validFiles.append(files[i])
        mlog.info("Outlier XML files for prod. #%d:\n%s" % (pid, ','.join(smallFiles)))
    else:
        validFiles = files
    return validFiles


if __name__ != '__main__':
    raise NotImplementedError('This module was designed to be called only as a script on command line.')

# script starts here!
parse_options()

mlog.info('Searching for production LOG archves to unpack...')
mlog.debug('Work path: %s\nRequested nb. XMLs: %d\nXML fragment veto level: %3.1f%%\nProd IDs to process: %s' %
           (base_path, runOptions.nbLogs, runOptions.delta * 100., str(ProdIDs)))
nbXMLs = 0
maxNbXMLs = int(1.1 * float(runOptions.nbLogs))  # 10% corrupted XML tolerance

for pid in ProdIDs:
    mlog.info('Processing prod. #%d : ' % pid)
    if runOptions.useOldAlgo:
        cwd_path = os.path.join(base_path, repr(pid))
    else:
        cwd_path = os.path.join(base_path, 'GenLogs-%d' % pid)
    # clean up older XML files in prod directory
    for fpath in glob.glob(os.path.join(cwd_path, '*.xml')):
        os.unlink(fpath)
    zfiles = getProdLocalArchives(pid)
    if len(zfiles) == 0:
        mlog.warning('No archived files found for prod. #%d. Skipping...' % pid)
        continue
    else:
        mlog.debug('Found %d compressed archives.' % len(zfiles))
    for zlgfile in zfiles:
        mlog.info("Prod. #%d : untarring %d XMLs from %s..." % (pid, maxNbXMLs-nbXMLs, os.path.split(zlgfile)[1]))
        lbTballz.untarProdFiles(pid, [zlgfile, ], cwd_path, maxNbXMLs-nbXMLs, filesToExtract=[r'.*GeneratorLog\.xml', ])
        nbXMLs = len(glob.glob(os.path.join(cwd_path, "GeneratorLog*.xml")))
        if nbXMLs > maxNbXMLs - 1:
            break
    if nbXMLs == 0:
        mlog.warning('Skipping prod. ID %d as no XML files could be extracted.' % pid)
        continue
    if runOptions.cleanTarz:
        for fpath in zfiles:
            os.remove(fpath)
    # ------------------------------------------------------------
    # Filter XMLs by file-size and output stats
    # ------------------------------------------------------------
    validFiles = preValidateLocalXMLs(cwd_path)
    if len(validFiles) == 0:
        mlog.warning('No valid XML log files left for prod. #%d after size filtering.' % pid)
    else:
        mlog.info('Prod. #%d: prevalidated %d XMLs of total %d files.' % (pid, len(validFiles), nbXMLs))

mlog.info('Run finished.')
logging.shutdown()
