﻿# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright CERN for the benefit of the LHCb Collaboration                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Project:     DBASE/MCStatTools
# Name:        GenXmlLogParser.py
# Purpose:     Parse GeneratorLog.xml and provide a mechanism for merging the
#              the generator counters. Parse jobDescription.xml files to give
#              detailed information about the corresponding production setup
#
# Author:      Alex T. GRECU <alexandru.grecu@gmail.com>
# Created:     29.07.2013 [dd.mm.yyyy]
# -------------------------------------------------------------------------------
"""Module including all XML processing and merging code."""

__version__ = '202109xx'

import os
import sys
import xml.dom.minidom as xdom
from xml.parsers.expat import ExpatError
import re
import math
import six

import urllib.request as ulib
# import shutil
import json
from datetime import datetime
import zlib
import tarfile
import gzip
# import copy
from io import BytesIO as zipBytes

import logging

mlog = logging.getLogger('lbMcST.%s' % __name__)
mlog.setLevel(mlog.parent.level)
mlog.info('Module %s v. %s loaded.' % (__name__, __version__))

logBaseName = 'GeneratorLog'
xml_log_name = '%s.xml' % logBaseName

# to correct malformed XML on-the-fly
correctiveRes = {
    r'crosssection\s+id = (\d+)\s*>': r'crosssection id="\1">',
    # may be removed when all available logs are properly formed!
    r'[\x00-\x1f\x7f-\xff]':          ''  # keep this filter until utf-8 is properly supported if ever
}
# Fill following list with object names for which merging must be done regardless of their presence
# in the GenStats to be merged
buggyStatObjects = []
# following dictionary defines rare processes for which missing cross-section stats are safe to ignore at merge
# process codes correspond to Pythia 8 definition
bSO_defaults = {201: 'Prompt photon: q g --> q gamma',
                202: 'Prompt photon: q qbar --> g gamma',
                221: 'Weak single boson: f fbar --> gamma*/Z0',
                222: 'Weak single boson: f fbar --> W+-',
                401: 'Charmonium: g g --> ccbar[3S1(1)] g',
                402: 'Charmonium: g g --> ccbar[3S1(8)] g',
                403: 'Charmonium: q g --> ccbar[3S1(8)] q',
                404: 'Charmonium: q qbar --> ccbar[3S1(8)] g',
                405: 'Charmonium: g g --> ccbar[1S0(8)] g',
                406: 'Charmonium: q g --> ccbar[1S0(8)] q',
                407: 'Charmonium: q qbar --> ccbar[1S0(8)] g',
                408: 'Charmonium: g g --> ccbar[3PJ(8)] g',
                409: 'Charmonium: q g --> ccbar[3PJ(8)] q',
                410: 'Charmonium: q qbar --> ccbar[3PJ(8)] g',
                411: 'Charmonium: g g --> ccbar[3S1(1)] gamma / ccbar[3PJ(1)] g',
                412: 'Charmonium: q g --> ccbar[3PJ(1)] q',
                413: 'Charmonium: q qbar --> ccbar[3PJ(1)] g',
                414: 'Charmonium: g g --> ccbar[3S1(8)] g',
                415: 'Charmonium: q g --> ccbar[3S1(8)] q',
                416: 'Charmonium: q qbar --> ccbar[3S1(8)] g',
                417: 'Charmonium: g g --> ccbar[3DJ(1)] g',
                418: 'Charmonium: g g --> ccbar[3PJ(8)] g',
                419: 'Charmonium: q g --> ccbar[3PJ(8)] q',
                420: 'Charmonium: q qbar --> ccbar[3PJ(8)] g',
                }
buggyStatObjects += ["pp%d" % x for x in bSO_defaults.keys()]


class GenXmlNode(object):
    xElementName = None
    xAttribNames = []
    xMandatoryAttribNames = []
    xChildNames = []
    
    def __init__(self, xmlElement=None):
        if xmlElement is not None and not isinstance(xmlElement, xdom.Element):
            xmlElement = None
        self._xElem = xmlElement
        self._attribs = dict()
        self._children = dict()
        self._value = None  # property will contain additive quantities only
        self._id = None
    
    def getAttribute(self, attrId):
        ret = None
        if self.hasAttribute(attrId):
            ret = self._attribs[attrId]
        return ret
    
    def hasAttribute(self, attrId):
        return attrId in self._attribs and self._attribs[attrId] is not None
    
    def hasChild(self, childId):
        childId = str(childId)
        return childId in self._children and self._children[childId] is not None
    
    def getChild(self, childId):
        ret = None
        childId = str(childId)
        if self.hasChild(childId):
            ret = self._children[childId]
        return ret
    
    def getNextSiblingNode(self):
        if self._xElem is None:
            return None
        xnd = self._xElem.nextSibling
        while xnd.nodeType != xdom.Element.ELEMENT_NODE:
            xnd = xnd.nextSibling
            if xnd is None:
                break
        return xnd

    def getId(self):
        return self._id

    def genericId(self, **params):
        """
        Returns the generic (hard-coded) counter id based on params passed
        as a dictionary with key names given by corresponding XML node
        attributes. If no data is provided this is just an alias for getId()
        """
        raise NotImplementedError('This method is only implemented by subclasses.')
    
    def getValue(self):
        return self._value
    
    def isBoundToXml(self):
        return self._xElem is not None
    
    @staticmethod
    def _getInt(value):
        if value is None:
            return None
        if isinstance(value, six.string_types):
            value = value.strip()
        try:
            ret = int(value)
        except ValueError as exx:
            mlog.debug("{0}: {1}".format(sys.exc_info()[0], exx))
            ret = None
        return ret
    
    @staticmethod
    def _getFloat(value):
        if value is None:
            return None
        if isinstance(value, six.string_types):
            value = value.strip()
        try:
            ret = float(value)
        except ValueError as exx:
            mlog.debug("{0}: {1}".format(sys.exc_info()[0], exx))
            ret = None
        return ret
    
    def __convertProperties(self):
        raise NotImplementedError('This method is only implemented in derived classes.')
    
    def fromXmlElement(self, xElem):
        """
        In subclass re-implementations(overloads), this method returns True when
        critical XML children fail to contain valid (numerical) data, e.g. nan.
        """
        if not isinstance(xElem, xdom.Element):
            raise TypeError('Invalid argument, xml.dom.minidom.Element expected.')
        if xElem.nodeType != xdom.Element.ELEMENT_NODE or \
                xElem.nodeName != self.xElementName or \
                not xElem.hasChildNodes():
            # mlog.debug(self._xElem.toprettyxml())
            raise ValueError('Invalid or malformed <%s> node.' % self.xElementName)
        for an in self.xAttribNames:
            if not xElem.hasAttribute(an):
                msg = '<%s> element missing \'%s\' attribute.' % (self.xElementName, an)
                if an not in self.xMandatoryAttribNames:
                    mlog.warning(msg)
                else:
                    raise ValueError('Malformed ' + msg)
        self._xElem = xElem
        cn = xElem.firstChild
        while cn is not None:
            if cn.nodeType == xdom.Element.TEXT_NODE:
                if cn.nodeValue is not None:
                    tnv = cn.nodeValue.strip()
                    if len(tnv) > 0:
                        if self._value is None:
                            self._value = tnv
                        else:
                            self._value += tnv
            if cn.nodeType != xdom.Element.ELEMENT_NODE:
                cn = cn.nextSibling
                continue
            if cn.nodeName not in self.xChildNames:
                mlog.debug('Unknown child <%s> of <%s> detected. Ignoring...' % (cn.nodeName, self.xElementName))
            else:
                nv = None
                if cn.hasChildNodes():
                    vnn = cn.firstChild
                    while vnn is not None and vnn.nodeType != xdom.Element.TEXT_NODE:
                        vnn = vnn.nextSibling
                    nv = vnn.nodeValue.strip()
                self._children[cn.nodeName] = nv
            cn = cn.nextSibling
        for an in self.xAttribNames:
            self._attribs[an] = None
            if xElem.hasAttribute(an):
                self._attribs[an] = xElem.getAttribute(an)
    
    def hasChildElement(self, ceName, getList=False):
        if not self.isBoundToXml() or not self._xElem.hasChildNodes():
            if getList:
                return []
            else:
                return False
        passChNodes = []
        for cn in self._xElem.childNodes:
            if cn.nodeType != xdom.Element.ELEMENT_NODE:
                continue
            if cn.nodeName == ceName:
                passChNodes.append(cn)
        if getList:
            ret = passChNodes
        else:
            ret = (len(passChNodes) > 0)
        return ret
    
    def toXmlElement(self, XNode):
        if not self.isBoundToXml():
            self._xElem = XNode.ownerDocument.createElement(self.xElementName)
            XNode.appendChild(self._xElem)
        if not self.isBoundToXml():
            raise ValueError('Invalid value for associated XML node.')
        if len(self.xMandatoryAttribNames) > 0:
            for an in self.xMandatoryAttribNames:
                self._xElem.setAttribute(an, 'not set')
        for cn in self.xChildNames:
            tcns = self.hasChildElement(cn, True)
            if len(tcns) == 0:
                tce = self._xElem.ownerDocument.createElement(cn)
                self._xElem.appendChild(tce)
            elif len(tcns) > 1:
                for ctn in tcns[1:]:
                    self._xElem.removeChild(ctn)
                tce = tcns[0]
            else:
                tce = tcns[0]
            tln = list(tce.childNodes)
            for ctn in tln:
                tce.removeChild(ctn)


# noinspection PyAbstractClass
class GenGlobalParam(GenXmlNode):
    """
    Implements the XML-Python interface for global parameters in the .xml log file.
    On 'addition' retains value of first operand and a warning is printed if
    the value of second operand (as stripped, case insensitive string) differs. May be used to
    enforce validity of logs being merged. The Id property is computed by default
    by adding the 'xLog_' prefix (e.g. version -> xLog_version) and used as key
    in the final GenStats dictionary.
    """
    
    def __init__(self, xNode=None, ocopy=None):
        super(GenGlobalParam, self).__init__()
        if xNode is not None:
            self.fromXmlElement(xNode)
        if isinstance(ocopy, GenGlobalParam):
            self.xElementName = ocopy.xElementName
            self._id = ocopy.genericId()
            self._value = ocopy.getValue()
            self._xElem = ocopy._xElem
            self._attribs = dict(ocopy._attribs)
            self._children = dict(ocopy._children)
    
    def fromXmlElement(self, xElem):
        self.xElementName = getattr(xElem, 'nodeName', None)
        if self.xElementName is None:
            raise TypeError('Can get tag name for XML element of type %s.' % (str(xElem.__class__)))
        super(GenGlobalParam, self).fromXmlElement(xElem)
        self._id = self.genericId()
    
    def genericId(self, **params):
        """
        See GenXmlNode method for help.
        """
        return 'xLog_' + self.xElementName
    
    def __add__(self, other):
        if other is None and self._id in buggyStatObjects:
            return self
        ov = other.getValue()
        if not isinstance(ov, six.string_types):
            mlog.info("Value type (%s) of <%s> node differs from previous file. Will ignore change..." %
                      (str(ov.__class__), self.xElementName))
        tv = str(self.getValue())
        if ov.strip().lower() != tv.strip().lower():
            mlog.info(
                "Value of <%s> node differs from previous file. \n (this) \"%s\" vs. (new) \"%s\""
                "\nWill ignore change..." % (self.xElementName, tv, ov))
        return self
    
    def recompute(self, coll):
        pass
    
    def getCounters(self):  # to ensure information is copied in the dict use to generate the web page
        return [(self.getId(), self.getValue()), ]
    
    def toXmlElement(self, xNode):
        xDoc = xNode.ownerDocument
        if not self.isBoundToXml() or self._xElem.ownerDocument != xDoc:
            self._xElem = xDoc.createElement(self.xElementName)
            xNode.appendChild(self._xElem)
        if not self.isBoundToXml():
            raise ValueError('Invalid value for associated XML node.')
        tvn = xDoc.createTextNode(str(self.getValue()).strip())
        self._xElem.appendChild(tvn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum."""
        return {'type': self.xElementName,
                'id': self._id,
                'value': self.getValue()
                }
    
    def fromDict(self, jsDict):
        """
        Populates current instance with data provided in jsDict loaded from JSON.
        """
        self.xElementName = jsDict['type']
        self._id = self.genericId()
        self._value = jsDict['value']
    
    def __repr__(self):
        ret = '%s.%s(\'<%s>%s</%s>\')' % \
              (__name__, self.__class__.__name__, self.xElementName, self.getValue(), self.xElementName)
        return ret


class GenCounter(GenXmlNode):
    """
    XML-Python interface for <counter> nodes. The Id in the final dictionary is
    given by reverse mapping of the mandatory name attribute from idmap dictionary.
    """
    idmap = {'L1':  "all events (including empty events)",
             'L2':  "events with 0 interaction",
             'N1':  "generated events", 'N2': "generated interactions",
             'N3':  "accepted events", 'N4': "interactions in accepted events",
             'N5':  "generated interactions with >= 1b",
             'N6':  "generated interactions with >= 3b",
             'N7':  "generated interactions with 1 prompt B",
             'N8':  "generated interactions with >= 1c",
             'N9':  "generated interactions with >= 3c",
             'N10': "generated interactions with >= prompt C",
             'N11': "generated interactions with b and c",
             'N12': "accepted interactions with >= 1b",
             'N13': "accepted interactions with >= 3b",
             'N14': "accepted interactions with 1 prompt B",
             'N15': "accepted interactions with >= 1c",
             'N16': "accepted interactions with >= 3c",
             'N17': "accepted interactions with >= prompt C",
             'N18': "accepted interactions with b and c",
             'N21': "z-inverted events",
             'I13': "generated (bb)", 'I24': "accepted (bb)",
             'I33': "generated (cc)", 'I42': "accepted (cc)"
             }
    xElementName = 'counter'
    xAttribNames = ['name', ]
    xMandatoryAttribNames = ['name', ]
    xChildNames = ['value', ]
    
    def __init__(self, oid='NS0', name=None, value=-1, ocopy=None):
        super(GenCounter, self).__init__()
        self._id = oid
        if isinstance(ocopy, GenCounter):
            self._id = ocopy._id
            name = ocopy._attribs['name']
            if 'value' in ocopy._children:
                self._children['value'] = ocopy._children['value']
            if hasattr(ocopy, '_value'):
                self._value = ocopy._value
        if name is not None:
            self._attribs['name'] = name
        if value > -1:  # not for copy constructor
            self._children['value'] = value
            self._value = value
    
    def genericId(self, **params):
        """ See GenXmlNode method for help."""
        if len(params) == 0 or 'name' not in params:
            return self.getId()
        oid = ''
        name = params['name']
        for (tid, nn) in GenCounter.idmap.items():
            if name.lower() == nn.lower():
                oid = tid
                break
        if len(oid) == 0:
            return self.getId()
        return oid
    
    def __convertProperties(self):
        corrupted = False
        xmlName = self.getAttribute('name')
        self._id = self.genericId(name=xmlName)
        sv = self.getChild('value')
        rt = GenXmlNode._getInt(sv)
        if rt is None:
            rt = 0
            corrupted = True
        self._children['value'] = rt
        self._value = rt
        return corrupted
    
    def fromXmlElement(self, xElem):
        if not super(GenCounter, self).fromXmlElement(xElem):
            self._id = self.genericId()
        return self.__convertProperties()
    
    def getCounters(self):
        return [(self.getId(), self._children['value']), ]
    
    def __add__(self, other):
        # result will be unbound to any xml node!
        # enforce check of counter being on the 'specials' list before pseudo-merging
        if other is None and self._id in buggyStatObjects:
            return self
        if not isinstance(other, GenCounter):
            raise TypeError('You cannot merge GenCounter and %s instances.' % getattr(other, '__class__', 'Unknown'))
        myid = self.getId()
        if myid != other.getId():
            raise ValueError('Makes no sense to add counter \'%s\' to counter \'%s\'.' % (myid, other.getId()))
        vname = self.getAttribute('name')
        if vname != other.getAttribute('name'):
            mlog.warning('\'name\' attributes differ. Will consider value from first operand: \'%s\'.' % vname)
        sumvalue = self.getValue() + other.getValue()
        return GenCounter(oid=self.getId(), name=vname, value=sumvalue)
    
    def recompute(self, coll):
        pass  # for compatibility with other counter types
    
    def toXmlElement(self, xNode):
        super(GenCounter, self).toXmlElement(xNode)
        self._xElem.setAttribute('name', GenCounter.idmap[self._id])
        vn = self.hasChildElement('value', True)
        tvn = vn[0]
        dn = self._xElem.ownerDocument.createTextNode(str(self.getChild('value')))
        tvn.appendChild(dn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum."""
        return {'type': self.xElementName,
                'id': self.genericId(name=self.getAttribute('name')),
                'value': self.getValue()
                }
    
    def fromDict(self, jsDict):
        """ Populates current instance with data provided in jsDict loaded from JSON. """
        self._attribs['name'] = GenCounter.idmap[jsDict['id']]
        self._id = jsDict['id']
        self._value = jsDict['value']
        self._children['value'] = self._value
    
    def __str__(self):
        ret = self.toDict()
        del ret['type']
        tt = '%s%s' % (self.__class__.__name__, str(ret))
        return tt


class GenXSection(GenXmlNode):
    """
    XML-Python interface for <crosssection> nodes. The Id in the final dictionary is
    computed by adding 'pp' prefix to the mandatory 'id' attribute. The final 'value'
    is computed as mean of intermediary values stored in _children['tvalues']! The
    'description' value is kept from the first parsed log file. Default unit is millibarn.
    """
    xElementName = 'crosssection'
    xAttribNames = ['id', ]
    xMandatoryAttribNames = ['id', ]
    xChildNames = ['description', 'generated', 'value']
    
    def __init__(self, ocopy=None):
        super(GenXSection, self).__init__()
        self._value = -1.0  # the medium value when merged
        self._procCount = -1
        self._procid = None
        if isinstance(ocopy, GenXSection):
            self._children['description'] = ocopy._children['description']
            self._children['generated'] = ocopy._children['generated']
            self._children['value'] = ocopy._children['value']
            self.setProcId(ocopy.getProcId())
            if 'tvalues' in ocopy._children:
                self._children['tvalues'] = list(ocopy._children['tvalues'])
    
    def setProcId(self, procId):
        if isinstance(procId, six.string_types):
            procId = int(procId.strip())
        self._procid = procId
        self._id = self.genericId(id=self._procid)
        self._attribs['id'] = procId
    
    def getProcId(self):
        """
        Value of 'id' XML node attribute with no prefix added.
        """
        return self._procid
    
    def genericId(self, **params):
        """
        See GenXmlNode method for help.
        """
        if len(params) == 0 or 'id' not in params.keys():
            return self.getId()
        return 'pp%s' % (str(params['id']))
    
    def getChildValue(self, chName):
        if not isinstance(chName, six.string_types):
            return None
        chname = chName.lower()
        if chname not in GenXSection.xChildNames:
            return None
        if chname not in self._children.keys():
            return None
        return self._children[chname]
    
    def __convertProperties(self):
        corrupted = False
        self.setProcId(self.getAttribute('id'))
        dd = self.getChild('description')
        if dd is not None:  # cleaning the string of trailing non-ascii characters
            self._children['description'] = dd.strip('" *')
        rt = GenXmlNode._getInt(self.getChild('generated'))
        if rt is None:
            rt = 0
            corrupted = True
        self._children['generated'] = rt
        rt = GenXmlNode._getFloat(self.getChild('value'))
        if rt is None:
            rt = 0.0
            corrupted = True
        self._children['value'] = rt
        return corrupted
    
    def fromXmlElement(self, xElem):
        super(GenXSection, self).fromXmlElement(xElem)
        return self.__convertProperties()
    
    def getCounters(self):
        return [(self.getId(), self._children['value']), ]
    
    def __add__(self, other):
        # very important to maintain correct number of xsection values being added -> correct mean value
        if other is None and self._id in buggyStatObjects:
            return self
        if not isinstance(other, GenXSection):
            raise TypeError('You cannot add GenXSection instances to %s. Counter \'%s\' not on ignored list.' %
                            (getattr(other, '__class__', 'Unknown'), self._id))
        if self.getId() != other.getId():
            raise ValueError('Makes no sense to add cross-sections for processes \'%d\' and \'%d\'.' %
                             (self.getProcId(), other.getProcId()))
        ret = GenXSection(ocopy=self)
        if 'tvalues' in ret._children:
            ret._children['tvalues'].append(other._children['value'] * float(other._children['generated']))
        else:
            ret._children['tvalues'] = [self._children['value'] * float(self._children['generated']),
                                        other._children['value'] * float(other._children['generated'])]
        ret._children['generated'] += other._children['generated']
        ret._value = -1.0  # use recompute to get final value in this field.
        return ret
    
    def toXmlElement(self, xElem):
        super(GenXSection, self).toXmlElement(xElem)
        self._xElem.setAttribute('id', str(self.getProcId()))
        for ten in GenXSection.xChildNames[0:2]:
            tn = self.hasChildElement(ten, True)[0]
            ttn = self._xElem.ownerDocument.createTextNode(str(self.getChild(ten)))
            tn.appendChild(ttn)
        tn = self.hasChildElement('value', True)[0]
        ttn = self._xElem.ownerDocument.createTextNode('%.5g' % (self.getChild('value')))
        tn.appendChild(ttn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum.
        """
        ret = dict([('type', self.xElementName), ('id', self._id), ('description', self.getChild('description')),
                    ('processId', self._procid), ('value', self._children['value']),
                    ('generated', self._children['generated'])])
        if 'tvalues' in self._children:
            ret['tvalues'] = list(self._children['tvalues'])
        return ret
    
    def fromDict(self, jsDict):
        """ Populates current instance with data provided in jsDict loaded from JSON.
        """
        self._id = jsDict['id']
        self._procid = jsDict['processId']
        self._children['value'] = jsDict['value']
        self._children['generated'] = jsDict['generated']
        self._children['description'] = jsDict['description']
        if 'tvalues' in jsDict:
            self._children['tvalues'] = list(jsDict['tvalues'])
            self.recompute({})
    
    def recompute(self, coll):
        if 'tvalues' in self._children:
            sumxs = sum(self._children['tvalues'])
            mxs = -1.
            # for back-tracing tvalues are stored as
            # xs_i * nGenerated_i so when recomputing
            # one needs to divide by total nGenerated
            xngen = self._children['generated']
            xsval = self._children['value']
            if xngen > 0 and xsval > 1.e-15:
                if sumxs/xsval/float(xngen) > 0.97:
                    mxs = sumxs / float(xngen)
            # otherwise just take mean
            if mxs < 0.:
                # verify file produced by very old version
                nbv = len(self._children['tvalues'])
                vset = list(set(self._children['tvalues']))
                if len(vset)/float(nbv) > 0.1:
                    mlog.error('Weird temporary values for xsection %s detected!' % self.getId())
                    mxs = self._children['value']
                else:
                    # do not count zeroes when computing arithmetic mean - buggy!?
                    nvalid = len(set([xv for xv in self._children['tvalues'] if xv > 1.e-15]))
                    if nvalid > 0:
                        mxs = sumxs / float(nvalid)
                    else:
                        mxs = 0.
            self._children['value'] = mxs
            self._value = mxs
            del self._children['tvalues']
        else:  # case for only one file!
            self._value = self._children['value']
    
    def __str__(self):
        ret = self.toDict()
        del ret['type']
        ret['value [mb]'] = ret['value']
        del ret['value']
        tt = '%s%s' % (self.__class__.__name__, str(ret))
        return tt


class GenEfficiency(GenXmlNode):
    """
    XML-Python interface for <efficiency> nodes. The Id in the final dictionary is
    given by reverse mapping of the mandatory name attribute from idmap dictionary.
    'value' and 'error' are computed on !final! summed up values.
    """
    idmap = {'E0': "full event cut",
             'E1': "generator level cut",
             'E2': "generator particle level cut",
             'E3': "generator anti-particle level cut"
             }
    cntNameMap = {'E0': ('N19', 'N20'), 'E1': ('I1', 'I2'), 'E2': ('S1', 'S2'), 'E3': ('S3', 'S4')}
    xElementName = 'efficiency'
    xAttribNames = ['name', ]
    xMandatoryAttribNames = ['name', ]
    xChildNames = ['before', 'after', 'value', 'error']
    
    def __init__(self, ocopy=None):
        super(GenEfficiency, self).__init__()
        if isinstance(ocopy, GenEfficiency):
            self._id = ocopy.getId()
            self._attribs = dict(ocopy._attribs)
            self._children = dict(ocopy._children)
            self._value = -1.0  # use recompute to get final value in this field.
    
    def getCounters(self):
        dd = dict(zip(GenEfficiency.xChildNames[0:2], GenEfficiency.cntNameMap[self.getId()]))
        return [(v, GenXmlNode._getInt(self._children[k])) for (k, v) in dd.items()]
    
    def genericId(self, **params):
        """
        See GenXmlNode method for help.
        """
        if len(params) == 0 or 'name' not in params.keys():
            return self.getId()
        tid = ''
        xname = params['name']
        for (tid, nn) in GenEfficiency.idmap.items():
            if xname.lower() == nn.lower():
                break
        if len(tid) == 0:
            return self.getId()
        return tid
    
    def __convertProperties(self):
        corrupted = False
        nn = self.getAttribute('name')
        self._id = self.genericId(name=nn)
        rt = GenXmlNode._getInt(self.getChild('before'))
        if rt is None:
            rt = 0
            corrupted = True
        self._children['before'] = rt
        rt = GenXmlNode._getInt(self.getChild('after'))
        if rt is None:
            rt = 0
            corrupted = True
        self._children['after'] = rt
        rt = self._getFloat(self.getChild('value'))
        self._children['value'] = rt
        if rt is None:
            rt = 0.0
        rt = self._getFloat(self.getChild('error'))
        if rt is None:
            rt = 0.0
        self._children['error'] = rt
        return corrupted
    
    def fromXmlElement(self, xElem):
        super(GenEfficiency, self).fromXmlElement(xElem)
        return self.__convertProperties()
    
    def __add__(self, other):
        if other is None:  # allow efficiencies to miss across production
            return self
        if not isinstance(other, GenEfficiency):
            raise TypeError('You cannot add GenEfficiency instances to %s.' % getattr(other, '__class__', 'Unknown'))
        if self.getId() != other.getId():
            raise ValueError(
                'Makes no sense to add different efficiencies \'%s\' and \'%s\'.' % (self.getId(), other.getId()))
        if self.getAttribute('name') != other.getAttribute('name'):
            raise ValueError('Name attributes for GenEfficiency objects may not differ.')
        ret = GenEfficiency(ocopy=self)
        ret._children['before'] += other._children['before']
        ret._children['after'] += other._children['after']
        ret._children['value'] = -1.0
        ret._children['error'] = -1.0
        return ret
    
    def recompute(self, coll):
        if self._value == -1.0 and self._children['value'] == self._children['error']:
            i1 = self.getChild('before')
            i2 = self.getChild('after')
            if i1 == 0:
                self._value = 0.0
                self._children['error'] = 0.0
            else:
                self._value = float(i2) / float(i1)
                self._children['error'] = math.sqrt(i2 * (i1 - i2) / i1 / i1 / i1)
            self._children['value'] = self._value
    
    def toXmlElement(self, xElem):
        super(GenEfficiency, self).toXmlElement(xElem)
        self._xElem.setAttribute('name', GenEfficiency.idmap[self._id])
        for ten in GenEfficiency.xChildNames[0:2]:
            tn = self.hasChildElement(ten, True)[0]
            ttn = self._xElem.ownerDocument.createTextNode(str(self.getChild(ten)))
            tn.appendChild(ttn)
        for ten in GenEfficiency.xChildNames[2:]:
            tn = self.hasChildElement(ten, True)[0]
            ttn = self._xElem.ownerDocument.createTextNode('%.5g' % (self.getChild(ten)))
            tn.appendChild(ttn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum."""
        return dict([('type', self.xElementName), ('id', self.genericId(name=self.getAttribute('name'))),
                     ('before', self.getChild('before')), ('after', self.getChild('after'))])
    
    def fromDict(self, jsDict):
        """ Populates current instance with data provided in jsDict loaded from JSON.
        """
        self._id = jsDict['id']
        self._attribs['name'] = GenEfficiency.idmap[self._id]
        self._children['after'] = jsDict['after']
        self._children['before'] = jsDict['before']
        self._children['value'] = -1.0
        self._children['error'] = -1.0
    
    def __str__(self):
        ret = self.toDict()
        del ret['type']
        tt = '%s%s' % (self.__class__.__name__, str(ret))
        return tt


class GenFraction(GenXmlNode):
    """
    XML-Python interface for <fraction> nodes. The Id in the final dictionary is
    given by reverse mapping of the mandatory name attribute from dictionary selected
    from 'groups' using the idmap() method return. 'value' and 'error' are recomputed
    on final summed up 'number' values. See counter documentation for details.
    """
    groups = {'b':   {'gp_': (('B0', 'B+', 'Bs0', 'b-Baryon'), (14, 2), (3, 2)),
                      'gp~': (('anti-B0', 'B-', 'anti-Bs0', 'anti-b-Baryon'), (15, 2), (4, 2)),
                      'g1x': (('Bc+', 'Bc-'), (22, 1), (11, 1)),
                      'gp*': (('B(L=0,J=0)', 'B* (L=0, J=1)', 'B** (L=1, J=0,1,2)'), (46, 1), (43, 1))},
              'c':   {'gp_': (('D0', 'D+', 'Ds+', 'c-Baryon'), (34, 2), (25, 2)),
                      'gp~': (('anti-D0', 'D-', 'Ds-', 'anti-c-Baryon'), (35, 2), (26, 2)),
                      'gp*': (('D(L=0,J=0)', 'D* (L=0, J=1)', 'D** (L=1, J=0,1,2)'), (52, 1), (49, 1))},
              'sig': {'gp_': (('',), (), (90, 0)),
                      'gp~': (('~',), (), (99, 0))}
              }
    xElementName = 'fraction'
    xAttribNames = ['name', ]
    xMandatoryAttribNames = ['name', ]
    xChildNames = ['number', 'value', 'error']
    
    def __init__(self, ocopy=None):
        GenXmlNode.__init__(self)
        if isinstance(ocopy, GenFraction):       # copy constructor
            self._id = ocopy.getId()
            self._type = ocopy.getFracType()
            self._part = ocopy.getPartName()
            (self._mq, self._pgroup, ii) = ocopy.getGroupInfo()
            self._attribs = dict(ocopy._attribs)
            self._children = dict(ocopy._children)
            self._value = -1.0  # use recompute to get final value in this field.
    
    def getCounters(self):
        return [(self.getId(), GenXmlNode._getInt(self._children['number'])), ]
    
    def isAntiParticle(self):
        return (self._pgroup == 'gp~') or (self._part.find('anti-') > -1) or (self._part.find('~') > -1)
    
    def __idmap(self, fType, qgrp, pgrp, idx, pName='missing particle name'):
        cName = 'C_unknown'
        tdidx = (fType == 'accepted' and 1) or (fType == 'generated' and 2)
        if fType == 'signal':
            tdidx = 2
        idx0 = GenFraction.groups[qgrp][pgrp][tdidx][0]
        mul = GenFraction.groups[qgrp][pgrp][tdidx][1]
        cName = 'I%d' % (idx0 + idx * mul)
        return cName
    
    def idmap(self, name):
        if isinstance(name, bytes):
            name = str(name)
        tokens = name.split(' ')
        if len(tokens) <= 1:
            raise ValueError('Could not find GenFraction id as name might be invalid.')
        _type = tokens[0].strip()
        _part = tokens[1].strip()
        _pgroup = 'zzz'
        _mq = 'q'
        cl = 'I%d'
        cn = -1
        if len(tokens) > 2:
            suff = ' '.join(tokens[2:])
            if suff != 'in sample':
                _part += (' ' + suff)
        if _type != "signal":
            for (q, di) in GenFraction.groups.items():
                for (igp, plist) in di.items():
                    if _part in plist[0]:
                        cn = plist[0].index(_part)
                        _pgroup = igp
                        break
                if cn > -1:
                    _mq = q
                    break
        else:  # signal fraction (new!)
            _mq = 'sig'
            _pgroup = 'gp_'
            if _part.find('~') != -1:
                _pgroup = 'gp~'
            cn = 0
        cl = self.__idmap(_type, _mq, _pgroup, cn, _part)
        return cl, _type, _part, _mq, _pgroup, cn
    
    def genericId(self, **params):
        """
        See GenXmlNode method for help. Duplicates GenFraction::idmap().
        """
        if len(params) == 0 or 'name' not in params.keys():
            return self.getId()
        xname = params['name']
        lid = self.idmap(xname)[0]
        if len(lid) == 0:
            return self.getId()
        return lid
    
    def __convertProperties(self):
        corrupted = False
        nn = self.getAttribute('name')
        (self._id, self._type, self._part, self._mq, self._pgroup, iii) = self.idmap(nn)
        if self._pgroup == 'zzz' and iii == -1:
            mlog.debug('Fraction formula for %s %s %s was is not defined. Please, update script and Twiki. \
                        Ignoring fraction...' % (self._type, self._part, str(self.getGroupInfo())))
            return True
        rt = GenXmlNode._getInt(self.getChild('number'))
        if rt is None:
            rt = 0
            return True  # no need to further fill object as it should be discarded
        self._children['number'] = rt
        rt = self._getFloat(self.getChild('value'))
        if rt is None:
            rt = 0.0
        self._children['value'] = rt
        rt = self._getFloat(self.getChild('error'))
        if rt is None:
            rt = 0.0
        self._children['error'] = rt
        return corrupted
    
    def fromXmlElement(self, xElem):
        super(GenFraction, self).fromXmlElement(xElem)
        return self.__convertProperties()
    
    def __add__(self, other):
        if other is None:  # allow missing fractions accross production
            return self
        if not isinstance(other, GenFraction):
            raise TypeError('You cannot add GenFraction instances to %s.' % getattr(other, '__class__', 'Uknown'))
        if self.getId() != other.getId():
            raise ValueError(
                'Makes no sense to add different fractions with id \'%s\' and \'%s\'.' % (self.getId(), other.getId()))
        if self.getAttribute('name') != other.getAttribute('name'):
            raise ValueError('Name attributes for GenFraction objects may not differ.')
        ret = GenFraction(ocopy=self)
        ret._children['number'] += other._children['number']
        ret._children['value'] = -1.0
        ret._children['error'] = -1.0
        ret._value = -1.0  # use recompute to get final value in this field.
        return ret
    
    def recompute(self, coll):
        if self._value > -1.0:
            return
        isum = self.getCounters()[0][1]
        (myqc, mygrp, myii) = self.getGroupInfo()
        if myii > -1:
            for ii in range(len(GenFraction.groups[myqc][mygrp][0])):
                if ii == myii:
                    continue
                oid = self.__idmap(self.getFracType(), myqc, mygrp, ii)
                if oid not in coll:
                    mlog.error(str(coll))
                    raise ValueError('Cannot compute fraction \'%s\' since counter \'%s\' is missing.' %
                                     (self.getAttribute('name'), oid))
                ps = coll[oid].getCounters()[0][1]
                if ps < 0:
                    raise ValueError('Invalid counter value for counter \'%s\'.' % oid)
                isum += ps
        else:  # for signal counters
            pn = self.getPartName()
            if self.isAntiParticle():  # change rule from particle name to anti-particle name is not clear
                pn = pn.replace('~', '')
            isum = 0.0
            for fid in coll.getIdsByType('fraction'):
                pfid = fid.replace('~', '')
                if pfid.find(pn) > -1:
                    isum += coll[fid].getCounters()[0][1]
        cval = float(self.getCounters()[0][1])
        if int(isum) == 0:
            err = 0.0
            isum = 1
        else:
            if cval > isum:
                mlog.error(str(self))
            err = math.sqrt(cval * (isum - cval) / isum / isum / isum)
        self._value = cval / float(isum)
        self._children['error'] = err
        self._children['value'] = self._value
    
    def getPartName(self):
        return self._part
    
    def getFracType(self):
        return self._type
    
    def getGroupInfo(self):
        ii = -1
        if self._mq in GenFraction.groups and self._pgroup in GenFraction.groups[self._mq]:
            grp = GenFraction.groups[self._mq][self._pgroup][0]
            if self._part in grp:
                ii = grp.index(self._part)
        return self._mq, self._pgroup, ii
    
    def getNameFromTokens(self, otype=None, part=None):
        ret = None
        if otype is None:
            otype = self._type
        if part is None:
            part = self._part
        if otype == 'signal':
            ret = '%s %s in sample' % (otype, part)
        else:
            ret = '%s %s' % (otype, part)
        return ret
    
    def toXmlElement(self, xElem):
        super(GenFraction, self).toXmlElement(xElem)
        self._xElem.setAttribute('name', self.getNameFromTokens())
        tn = self.hasChildElement('number', True)[0]
        ttn = self._xElem.ownerDocument.createTextNode(str(self.getChild('number')))
        tn.appendChild(ttn)
        for ten in GenFraction.xChildNames[1:]:
            tn = self.hasChildElement(ten, True)[0]
            ttn = self._xElem.ownerDocument.createTextNode('%.5g' % (self.getChild(ten)))
            tn.appendChild(ttn)
    
    def toDict(self):
        """Returns object data as a dictionary to be used in JSON encoding of generator statistics object/sum."""
        ret = {'name': self.getAttribute('name'), 'type': self.xElementName, 'number': self.getChild('number')}
        ret['id'] = self.genericId(name=ret['name'])
        return ret
    
    def fromDict(self, jsDict):
        """ Populates current instance with data provided in jsDict loaded from JSON.
        """
        self._attribs['name'] = jsDict['name']
        (cn, self._type, self._part, self._mq, self._pgroup, iii) = self.idmap(jsDict['name'])
        self._id = jsDict['id']
        self._value = -1.0
        self._children['number'] = jsDict['number']
        self._children['value'] = -1.0
        self._children['error'] = -1.0
    
    def __str__(self):
        ret = self.toDict()
        del ret['type']
        srcCnt = 'grpInfo: %s' % (str(self.getGroupInfo()))
        tt = '%s(%s;%s)' % (self.__class__.__name__, str(ret), srcCnt)
        return tt


class GenJobLog(object):
    """
    Provide clear link between MC production job and generator counters produced by different generation tools. Maps to
    a XML file containing the counters of the generator FSR within a data file. Derived classes should implement
    the data extraction/parsing.
    """
    pass


class GenCounterSet(object):
    """
    Providing a hard link between MC Generator instance and corresponding counters in a log file
    in order to match order of generator instances writing to the XML file
    """
    
    def __init__(self, statlog, genName, toolName):
        self.parent = statlog
        self.generator = genName
        self.tool = toolName
        self.fastGen = self.generator.lower() in ['redecay', 'pgun']
        self.hasSpecial = self.generator.lower() in ['bcvegpy', ]
        self.stream = {}
    
    def fakeDict(self, objName):
        objName = objName.lower()
        rdict = {'id': 'xlog_%s' % objName, 'type': objName, 'value': None}
        if objName == 'generator':
            rdict['value'] = self.generator
        if objName == 'method':
            rdict['value'] = self.tool
        return rdict


class GenStats(object):
    """
    XML-Python interface parsing/merging/saving GeneratorLog*.xml files. Parsing is perfomed by
    GenXmlNode derived classes. Node name to class mapping is given by 'ChildrenMap' static property.
    Implements __add__ operator and partial dict class interface for addressing counters.
    Use splitStreams property to keep values when multiple streams are written to the log file.
    """
    MinBiasEvtType = '30000000'
    TimeStampFormat = '%a, %d %b %Y %H:%M:%S -0000'     # as email.utils.formatdate; RFC 2822
    RootElementName = 'generatorCounters'
    ChildrenMap = {'counter':      GenCounter,
                   'crosssection': GenXSection,
                   'efficiency':   GenEfficiency,
                   'fraction':     GenFraction,
                   'version':      GenGlobalParam,
                   'eventType':    GenGlobalParam,
                   'method':       GenGlobalParam,
                   'generator':    GenGlobalParam
                   }
    
    class fmtVersion(object):
        """
        Internal class to implement smart versioning.
        """
        def __init__(self, verstring):
            if isinstance(verstring, GenStats.fmtVersion):
                self.raw = verstring.raw
                self.ver = list(verstring.ver)
                self.version = verstring.version
            else:
                if not isinstance(verstring, six.string_types):
                    verstring = str(verstring)
                verstring.strip(',. \n\t\r')
                self.raw = verstring.split('.')
                self.ver = []
                for ts in self.raw:
                    if ts.isdigit():
                        self.ver.append(int(ts))
                    else:
                        self.ver.append(ts)
                self.version = verstring
        
        def duplicate(self):
            return GenStats.fmtVersion(self)
        
        def __prepare_arg__(self, other):
            if isinstance(other, six.string_types):
                return GenStats.fmtVersion(other)
            if not isinstance(other, GenStats.fmtVersion):
                raise NotImplementedError('Cannot compare with this version encoding...')

        def __eq__(self, other):
            other = self.__prepare_arg__(other)
            if len(self.raw) != len(other.raw):
                vobj = None
                nm = max(len(self.raw), len(other.raw))
                mm = min(len(self.raw), len(other.raw))
                if nm > len(self.raw):
                    if self.raw != other.raw[0:len(self.raw)]:
                        return False
                    vobj = other
                if nm > len(other.raw):
                    if other.raw != self.raw[0:len(other.raw)]:
                        return False
                    vobj = self
                ret = True
                for i in range(mm, nm):
                    tt = vobj.raw[i]
                    if not tt.startswith('0') or len(tt) > 1:
                        ret = False
                        break
                return ret
            else:
                return self.raw == other.raw

        def __ne__(self, other):
            return not self.__eq__(other)

        def __ge__(self, other):
            return self > other or self == other
        
        def __le__(self, other):
            raise self < other or self == other
        
        def __gt__(self, other):
            other = self.__prepare_arg__(other)
            if len(self.raw) != len(other.raw):
                nm = max(len(self.raw), len(other.raw))
                mm = min(len(self.raw), len(other.raw))
                vobj = None
                if nm > len(self.raw):
                    if self.raw != other.raw[0:len(self.raw)]:
                        return self.raw > other.raw[0:len(self.raw)]
                    vobj = other
                if nm > len(other.raw):
                    if other.raw != self.raw[0:len(other.raw)]:
                        return self.raw[0:len(other.raw)] > other.raw
                    vobj = self
                ret = False
                for i in range(mm, nm):
                    tt = vobj.raw[i]
                    if not tt.startswith('0') or len(tt) > 1:
                        ret = True
                        break
                return ret
            else:
                return self.raw > other.raw
        
        def __lt__(self, other):
            return not self.__gt__(other)
        
    def __init__(self, filepath=None, prodstats=None, cpystream=True):
        """
            Initialize the GenStats object.
        Args:
            filepath (string): path to XML file to parse
            prodstats (GenStats): statistics object to inherit some internal options (propagate in a production merge)
        """
        if isinstance(prodstats, GenStats):
            self.__copy(prodstats, cpystream=cpystream)
        else:
            self.logger = mlog
            self._src = None
            self._fileCounter = 0
            self._xdoc = None
            self._streams = list()
            self._genLogs = list()
            self._initChildStream()
            self._icstream = 0
            self._fullpath = None
            self._xmlFileSize = -1
            self.genToolSpecial = False     # determine at parsing if special generation tool used in production
            # assume by default production w/ spillover streams
            self.splitStreams = True
            # Global list of process IDs to consider for Special Signal generation
            self.specialSignalProcs = []
            self.isMerged = False
            self._xmlFiles = list()
            self._xmlSizes = list()  # stores file sizes to veto outlier file sizes (before parsing?!)
            self._fmtver = GenStats.fmtVersion('0.1')  # a generic value to default to most primitive parsing method
            if filepath is not None:
                self._fullpath = filepath
                self.loadXML(self._fullpath)
            
    def __copy(self, pstats, cpystream=True):
        pstats.__selftest()
        self.logger = pstats.logger
        self._src = pstats._src
        self._fileCounter = pstats._fileCounter
        self._xdoc = pstats._xdoc
        self._fullpath = pstats.getXMLFullPath()
        self._xmlFileSize = pstats._xmlFileSize
        self.genToolSpecial = pstats.genToolSpecial
        self.splitStreams = pstats.splitStreams
        self.specialSignalProcs = list(pstats.specialSignalProcs)
        self.isMerged = pstats.isMerged
        self._icstream = pstats.selStreamIndex()
        self._xmlFiles = list(pstats._xmlFiles)
        self._xmlSizes = list(pstats._xmlSizes)
        self._fmtver = pstats._fmtver.duplicate()
        self._genLogs = [GenCounterSet(self, pstats.mcGen(si), pstats.mcTool(si)) for si in range(pstats.streamCount())]
        self._streams = list()
        if cpystream:
            for jj in range(pstats.streamCount()):
                self._initChildStream()
                for kk in GenStats.ChildrenMap.keys():  # deep copy!
                    for ij in range(len(pstats.children(jj)[kk])):
                        self.children(jj)[kk].append(GenStats.ChildrenMap[kk](ocopy=pstats.children(jj)[kk][ij]))
        else:
            self._initChildStream()
        
    def selStreamIndex(self):
        return self._icstream
    
    def getXMLFullPath(self):
        return self._fullpath
    
    def __str__(self):
        tt = ''
        if self._xdoc is None and self.getXMLFullPath() is None and len(self._streams) == 0:
            tt = 'uninitialized'
        if self._src is not None:
            tt = '(%s)' % self._src
        msgs = ['GenStats %s object:' % tt, ]
        if len(self._xmlFiles) == 0 and len(self._streams) == 0:
            msgs.append('!empty object!')
            return '\n'.join(msgs)
        msgs.append(' - %d counter streams\n' % (len(self._streams)))
        for ii in range(len(self._streams)):
            self.selectStream(ii)
            cObjs = self.children(ii)
            if len(cObjs['method']) > 0:
                msgs.append('*** Stream %d (method %s):\n' % (ii, cObjs['method'][0].getValue()))
            else:
                msgs.append('*** Single Stream Log:\n')
            for kk in GenStats.ChildrenMap.keys():
                nn = len(cObjs[kk])
                msgs.append(' - % 3d %s object(s)' % (nn, kk))
        return '\n'.join(msgs)
    
    def selectStream(self, index):
        if index == self._icstream:
            return True
        if index < 0 or index > len(self._streams):
            return False
        self._icstream = index
        return True
    
    def streamCount(self):
        return len(self._streams)
    
    def mcGen(self, istream=0):
        return self._genLogs[istream].generator
    
    def mcTool(self, istream=0):
        return self._genLogs[istream].tool
    
    def _initChildStream(self):
        """
        Each stream is initialized as a dictionary of known type name vs. list of objects of given known type.
        """
        self._streams.append(dict([(kk, list()) for kk in GenStats.ChildrenMap.keys()]))
   
    def children(self, istream=None):
        if self._icstream == self.streamCount():
            mlog.debug('Creating new stream @ index %d' % self._icstream)
            self._initChildStream()
        if istream is None:
            istream = self._icstream
        nbstreams = self.streamCount()
        if istream > nbstreams:
            print('Avoiding out of range error...')
            return None
        if isinstance(istream, int) and istream < nbstreams:
            return self._streams[istream]
        return None
   
    def _updateSizeStats(self, fname, fsz):
        # this works only w/ local XML paths!
        if fname in self._xmlFiles:
            return True
        wkpath = os.path.split(self.getXMLFullPath())[0]
        if not os.path.exists(os.path.join(wkpath, fname)):
            return True
        self._xmlFiles.append(fname)
        self._xmlSizes.append(fsz)
        return False
   
    def _vetoBySize(self, newsz):
        pass
   
    def _readzFile(self, filePath):
        ret = None
        try:
            zfp = tarfile.open(filePath, mode='r:gz')
            arhNames = zfp.getnames()
            # kind of restrictive!
            if xml_log_name not in arhNames:
                zfp.close()
                return None
            fp = zfp.extractfile(xml_log_name)
            ret = fp.read()
            fp.close()
            zfp.close()
        except (IOError, EOFError, zlib.error, tarfile.ReadError) as err:
            self.logger.error(str(err))
            ret = None
        return ret
   
    def _readFile(self, fileUrl, compressed=False):
        ret = None
        if isinstance(fileUrl, six.string_types) and (fileUrl.startswith('/') or fileUrl.find('://') == -1):
            if not os.path.isfile(fileUrl):
                raise ValueError('Invalid local .xml file path.')
            fileUrl = os.path.abspath(fileUrl)
            fileUrl = 'file://' + fileUrl
        try:
            req = ulib.Request(fileUrl)
            req.add_header('Accept-encoding', 'gzip')
            fp = ulib.urlopen(req, timeout=10)
            if fp is None:
                raise Exception('Unknown handler for given location protocol.')
            nbytes = -1
            if not compressed and 'Content-Encoding' in fp.info():
                compressed = fp.info().get('Content-Encoding') == 'gzip'
            if 'content-length' in fp.info():
                nbytes = int(fp.info().get('Content-length'))
            # mlog.debug('Getting %d bytes from data stream.\n%s' % (nbytes, str(fp.info())))
            if compressed:
                ret = zipBytes(fp.read(nbytes))
            else:
                ret = fp.read(nbytes)
            self._fullpath = fp.geturl()
            if compressed:
                ufp = gzip.GzipFile(fileobj=ret)
                uret = ufp.read()
                ufp.close()
                ret.close()
                ret = uret
                nbytes = len(ret)
            fp.close()
            if 0 < nbytes != len(ret):
                mlog.error('Less data read from server %d/%d' % (len(ret), nbytes))
            self._xmlFileSize = len(ret)
        except Exception as exx:
            mlog.error(str(exx))
        return ret
   
    def loadXML(self, filepath):
        """
        Perform common initial checks and call parsing code based on log format <version>.
        """
        sxContent = ''
        if filepath.endswith('.tgz') or filepath.endswith('.gz'):
            sxContent = self._readzFile(filepath)
        else:
            sxContent = self._readFile(filepath)
        sxFormedContent = None
        xmlFn = os.path.split(filepath)[1]
        if sxContent is None or len(sxContent) == 0:
            mlog.error('Could not read data from \'%s\'.' % self.getXMLFullPath())
            return
        try:
            xdoc = xdom.parseString(sxContent)
        except ExpatError:  # try to recover by correcting the XML on the fly!
            mlog.debug('Parsing error (%s). Attempting to recover XML structure on the fly.' % xmlFn)
            sxFormedContent = sxContent
            for (reExpr, reRepl) in correctiveRes.items():
                sxFormedContent = re.sub(reExpr, reRepl, sxFormedContent)
            xdoc = xdom.parseString(sxFormedContent)
        if xdoc.documentElement is None or xdoc.documentElement.nodeName != GenStats.RootElementName:
            raise IOError('.xml file seems invalid!', filepath)
        if not xdoc.documentElement.hasChildNodes():
            raise IOError('.xml file seems truncated. Did Gauss crash?', filepath)
        self._xdoc = xdoc
        if sxFormedContent is not None:
            mlog.debug('Succesfully parsed \'%s\'' % xmlFn)
        ve = self._xdoc.documentElement.getElementsByTagName('version')
        if len(ve) == 1:
            vo = GenGlobalParam(ve[0])
            # ver = str(vo.getValue())
            self._fmtver = GenStats.fmtVersion(vo.getValue())
        # XML format version selection here
        if self._fmtver > "1.0":
            if self._fmtver > "1.1":
                mlog.warning('XML logs version %s may not be fully supported.' % self._fmtver.version)
            self.parseXDoc(self._fmtver.version)
        else:
            self.parseXDocBasic()  # basically produces invalid GenStats in v4r3+
        self._src = os.path.split(filepath)[1]
        self._fileCounter = 1
    
    def parseXDoc(self, fmtVer='1.1'):
        # get list of generators and tools
        mes = self._xdoc.documentElement.getElementsByTagName('method')
        fn = os.path.split(self._fullpath)[1]
        self.splitStreams = len(mes) > 1
        for i in range(len(mes)):
            me = mes[i]
            mo = GenGlobalParam(me)
            ne = mo.getNextSiblingNode()
            if ne is None or not ne.nodeName == u'generator':
                continue
            # mlog.info(ne.toprettyxml(indent='  '))
            go = GenGlobalParam(ne)
            if i > 0:
                self._initChildStream()
                self.selectStream(i)
            self.children()['method'].append(mo)
            self.children()['generator'].append(go)
            self._genLogs.append(GenCounterSet(self, go.getValue(), mo.getValue()))
        mths = [lmn.tool.lower() for lmn in self._genLogs]
        fastDecay = any([x.fastGen for x in self._genLogs])
        spillLevel = 0
        t = any([x.find('next.') > -1 for x in mths])
        t = t and any([x.find('prev.') > -1 for x in mths])
        if t:
            spillLevel += 1
        t = any([x.find('nextnext.') > -1 for x in mths])
        t = t and any([x.find('prevprev.') > -1 for x in mths])
        if t:
            spillLevel += 1
        ctmap = (0,)
        xsmap = (0,)
        # for method list order: [fsig] nn n p pp sig -- 0 1 2 3 4 5
        # assume map for counters: sig pp p nn n [fsig]
        # assume map for x-sections: n nn p pp sig [fsig/may be missing!]
        if spillLevel == 0 and fastDecay:  # for fast gen tools always use signal tool counters?!
            ctmap = (1, 0)
            xsmap = (1, 0)
        if spillLevel == 1:
            if fastDecay:
                ctmap = (3, 2, 1, 0)
                xsmap = (1, 2, 3, 0)
            else:
                ctmap = (2, 1, 0)
                xsmap = (0, 1, 2)
        if spillLevel == 2:
            if fastDecay:
                ctmap = (5, 4, 3, 1, 2, 0)
                xsmap = (2, 1, 3, 4, 5, 0)
            else:
                ctmap = (4, 3, 2, 0, 1)
                xsmap = (1, 0, 2, 3, 4)
        xevts = self._xdoc.documentElement.getElementsByTagName('eventType')
        if len(xevts) > len(ctmap):
            mlog.error('Unsupported XML file version (%s). Too many event type nodes detected...' % fmtVer)
            raise ValueError('Too many event type nodes detected. Please, report production ID on JIRA.')
        for j in range(len(xevts)):  # parse event type nodes
            self.selectStream(ctmap[j])  # select stream from counter map
            oo = GenGlobalParam()
            oo.fromXmlElement(xevts[j])
            self.children()['eventType'].append(oo)
            ce = oo.getNextSiblingNode()
            while ce is not None:
                nt = ce.nodeName
                if nt == 'eventType' or nt == 'method':
                    break
                ox = GenXmlNode(ce)
                if nt in ['counter', 'efficiency', 'fraction']:
                    oo = GenStats.ChildrenMap[ce.nodeName]()
                    if not oo.fromXmlElement(ce):
                        self.children()[nt].append(oo)
                    else:
                        mlog.debug('Bad <%s> in %s:\n%s' % (nt, fn, ce.toprettyxml(indent='  ')))
                else:
                    mlog.warning('Unknown XML node <%s> in %s. Ignoring...' % (ce.nodeName, fn))
                ce = ox.getNextSiblingNode()
        xxs = self._xdoc.documentElement.getElementsByTagName('crosssection')
        ii = -1
        ilstream = -1
        for j in range(len(xxs)):  # parse xsection nodes
            oo = GenXSection()
            corrupted = oo.fromXmlElement(xxs[j])
            if oo.getId() in self or ii < 0:
                ii += 1
                if ii == len(xsmap):
                    break
                ilstream = xsmap[ii]
                if not self.selectStream(ilstream):  # some generators may duplicate xsection counters
                    continue
            if not corrupted:
                self.children(istream=ilstream)['crosssection'].append(oo)  # last active stream!
            else:
                mlog.debug('Bad <crosssection> in %s:\n%s' % (fn, xxs[j].toprettyxml(indent='  ')))
        if ilstream not in xsmap:
            mlog.error('XML file \'%s\' does not contain cross-sections!' % fn)
            return
        # in case one of the spill-over xsection group is missing (just copy latest non-empty group)
        # also for the case when special signals are generated w/ spill-over (see below xsection adjustment!)
        # last set of <crosssection> should be signal anyway!
        for kj in range(xsmap.index(ilstream)+1, len(xsmap)):
            msid = xsmap[kj]
            if len(self.children(istream=msid)['crosssection']) != 0:
                continue
            mlog.debug('Group <crosssection> missing for stream #%d. '
                       'Copying from stream #%d...' % (msid, ilstream))
            self.children(istream=msid)['crosssection'] = \
                [GenXSection(ocopy=ixs) for ixs in self.children(istream=ilstream)['crosssection']]
        # bring signal on first position
        if len(ctmap) > 1:
            si = max(ctmap)
            t = self._genLogs[0]
            self._genLogs[0] = self._genLogs[si]
            self._genLogs[si] = t
            t = self._streams[0]
            self._streams[0] = self._streams[si]
            self._streams[si] = t
        # print(len(self._streams))
        for ii in range(self.streamCount()):
            self._genLogs[ii].stream = self._streams[ii]
        # test consistency of stream considered as signal stream
        self.selectStream(0)
        # mlog.debug(self.makeGenerationResults())
        genToolName = self._genLogs[0].tool.lower()  # special for other generators e.g. BcVegPy
        hasRepeatedHadronization = any([x.tool.lower().find("repeatedhadronization") > -1 for x in self._genLogs])
        self.genToolSpecial = genToolName.find('special') > -1
        # in future use 'all' on a list of exceptions if they accumulate:
        if genToolName.find('signal') == -1 and not self.genToolSpecial and genToolName.find('repeatdecay') == -1:
            mlog.warning('No \'signal\' in tool name (%s) for signal in file %s...' % (self._genLogs[0].tool, fn))
        # temporarily (!?) skip validation checks for generators other than Pythia (i.e. not all counters for BcVegPy)
        if self._genLogs[0].generator.lower().find('pythia') == -1:
            return
        (ol1, ol2, on1, on2) = tuple([self[nk] for nk in ['L1', 'L2', 'N1', 'N2']])
        if ol2 is None and self.genToolSpecial:    # for special signal it may be possible
            ol2 = GenCounter(oid='L2', value=0)
        if any([ax is None for ax in (ol1, ol2, on1, on2)]):
            err = IOError('Could not locate all counters for consistency check in file %s' % fn)
            raise err
        (l1, l2, n1, n2) = (ol1.getValue(), ol2.getValue(), on1.getValue(), on2.getValue())
        if int(l2) == 0:
            mlog.warning('File %s: Number of events with no interactions is zero.' % fn)
        if int(l1) != int(l2 + n1):
            mlog.error('File %s: All event count != empty + non-empty events.' % fn)
        xsl = self.children()['crosssection']
        isum = 0
        clxsl = []
        for xs in xsl[::-1]:
            if isum == n2:
                mlog.debug('Matched number of interactions w/ xsections at index #%d. Truncating...' % xsl.index(xs))
                break
            if xs.getProcId() == 0:
                continue
            xsg = xs.getChildValue('generated')
            if xsg is None:
                mlog.debug('Cannot read <generated> for cross-section:\n%s' % str(xs))
                continue
            isum += xsg
            clxsl.append(xs)
        if len(clxsl) < len(xsl):
            # drop xsections w/o generated
            self.children()['crosssection'] = clxsl
        if n2 == 0:
            raise IOError('File %s: Number of generated interactions is zero.' % fn)
        if isum == 0:
            mlog.debug('Used %d crosssection nodes to computer nb of interactions.\n%s' % (len(xsl), str(self)))
            raise IOError('File %s: Total number of interactions reported in cross-sections is zero.' % fn)
        toll = ((float(n2) - float(isum)) / float(n2) * 100.)   # tolerance in %
        icheckFail = n2 < isum or toll > 0.05
        if genToolName.find('repeatdecay') > -1:    # disable check for Generation.RepeatDecay.*
            icheckFail = False
        if hasRepeatedHadronization:
            # mlog.debug("Production w/ SignalRepeatedHadronization - increase tolerance...")
            toll = abs(toll) * float(n2) / float(max(n2, isum))
            icheckFail = toll > 0.25
        # introduce 0.25% tolerance for SignalRepeatedHadronization productions
        if int(l2) == 0 and genToolName.find('special') > -1:
            # for special signal w/ fixed luminosity - not sane (follow up!) !!!
            icheckFail = False
        # add high tolerance for SignalPlain productions which may use exotic filtering
        if icheckFail and genToolName.find('signalplain') > -1:
            icheckFail = toll > 2.5
        # match between N2 and iSum may be used to extract correct processes for signal in spill-over prods
        if icheckFail:
            raise IOError(
                'File %s: Total generated interactions not recovered (within tolerance) in cross-sections [%d vs. %d].'
                % (fn, int(n2), isum))
    
    def parseXDocBasic(self):
        """ Parsing based on an empirical walk-through the XML DOM. """
        child = self._xdoc.documentElement.firstChild
        obj = None
        while child is not None:
            if child.nodeType != xdom.Element.ELEMENT_NODE:
                child = child.nextSibling
                continue
            if child.nodeName not in GenStats.ChildrenMap:
                mlog.warning('No class mapping for element <%s>. Ignoring...' % child.nodeName)
                child = child.nextSibling
                continue
            obj = GenStats.ChildrenMap[child.nodeName]()
            if obj.fromXmlElement(child):
                mlog.warning('Ignoring XML element <%s> containing invalid critical numeric data...' % child.nodeName)
                child = child.nextSibling
                continue
            if obj.xElementName == 'version':  # overwritten by latest found node!
                self._fmtver = GenStats.fmtVersion(obj.getValue())
            if not self.splitStreams:
                if obj.getId() in self:
                    mlog.warning('Counter \'%s\' already registered. '
                                 'Will ignore latest value (new counter stream)!' % obj.getId())
                    child = child.nextSibling
                    continue
            else:
                oldBuggyFormat = (obj.xElementName == 'crosssection' and len(self.children()['method']) == 0)
                if oldBuggyFormat:
                    self.splitStreams = False
                    mlog.warning('No spill-over events detected. Switching back to signal only mode !!!')
                if obj.xElementName == 'method' and len(self.children()['method']) == 0:
                    if self._icstream > 0:
                        self._streams.reverse()  # spill-over statistics first
                        self._icstream = 0
                    else:
                        self.splitStreams = False  # switch to single signal mode!!!
                        mlog.warning('No spill-over events detected. Switching back to signal only mode !!!')
                if self._icstream > 0 and obj.xElementName == 'crosssection' and \
                        len(self.children()['crosssection']) == 0:
                    self._icstream = 0  # restart from index 0
                if obj.getId() in self:
                    # counters are written as signal followed by UE/minbias or spill-over - indicated by eventType nodes
                    # no other method applied to know the correct association between counters and cross-sections
                    # for spill-over events !
                    # cross-sections are written first for minbias/UE then for signal - given by method nodes
                    # signal statistics needs to be always at index 0
                    if obj.xElementName in ['eventType', 'method', 'crosssection']:
                        self._icstream += 1
            self.children()[obj.xElementName].append(obj)
            # mlog.debug('On stream %d registered object %s.' % (self._icstream,  str(obj)))
            child = child.nextSibling
        # print self._streams
        if self.splitStreams:  # if having spill-over events
            self._streams.reverse()
            self._icstream = len(self._streams) - 1
        for si in range(0, len(self._streams)):
            self.selectStream(si)
            genName = 'xLog_generator' in self.children() and self.children()['xLog_generator'][-1] or 'Sim_default'
            toolName = 'xLog_method' in self.children() and self.children()['xLog_method'][-1] or 'unknown'
            if si == 0:
                if toolName == 'unknown':
                    toolName = 'Signal.unknown'
            self._genLogs.append(GenCounterSet(self, genName, toolName))
        # mlog.debug(str(self))
        # enforce rule on presence of nodes for each stream?! special case for Redecay?!
        self._xmlFiles.append(os.path.split(self._fullpath)[1])
    
    def getXmlFileName(self):
        if self._fullpath is None and len(self._xmlFiles) > 0:
            return 'GenStats_Sum(%d files)' % self._fileCounter
        if self._fullpath is None or not os.path.isfile(self._fullpath):
            return 'empty/invalid GenStats object'
        return os.path.split(self._fullpath)[1]
    
    def __getitem__(self, oid):
        iloc = self.__locate(oid, True)
        if iloc is None:
            return None
        return self.children(self._icstream)[iloc[1]][iloc[0]]
    
    def __contains__(self, key):
        return self.__locate(key, getLocation=False)

    def __locate(self, key, getLocation=False, selStream=None):
        ret = None
        istream = self._icstream
        if selStream is not None:
            self.selectStream(selStream)
        for k in self.children(self._icstream).keys():
            for ii in range(len(self.children(self._icstream)[k])):
                obj = self.children(self._icstream)[k][ii]
                oid = obj.getId()
                if key == oid:
                    ret = (ii, k)
                    break
            if ret is not None:
                break
        if not getLocation:
            ret = ret is not None
        if selStream is not None:
            self.selectStream(istream)
        return ret
    
    # def has_key(self, name, getLocation=False):
    #    return self.__locate(name, getLocation=getLocation)
    
    def getIdsByType(self, statName, dosort=False, istream=None):
        """
        Args:
            statName (str): object type for which IDs are needed
            dosort (bool): specify to sort list or returned IDs
            istream (int): which statistics stream to use (default: the currently selected one)

        Returns:
            List of ids for specified object type in GenStats object.
        """
        if statName not in GenStats.ChildrenMap:
            raise ValueError('No statistics object of type \'%s\' is known to this class.' % statName)
        if istream is None:
            istream = self._icstream
        if istream < 0 or istream > len(self._streams):
            return None
        # nstats = len(self.children(istream)[statName])
        # ret = [self.children(istream)[statName][i].getId() for i in range(nstats)]
        ret = [sobj.getId() for sobj in self.children(istream)[statName]]
        if dosort:
            ret.sort()
        return ret
    
    def isCompatible(self, other):
        global buggyStatObjects
        gFailed = False
        othisFn = self.getXmlFileName()
        otherFn = other.getXmlFileName()
        # save object state
        ithis = self.selStreamIndex()
        iother = other.selStreamIndex()
        self.selectStream(0)
        other.selectStream(0)
        # enforce same event type and XML version (iff at least one log has these attributes!)
        if not self['xLog_version'] is None:
            this_ver = self['xLog_version'].getValue().strip()
            other_ver = other['xLog_version'].getValue().strip()
            if this_ver != other_ver:
                gFailed = True
                mlog.error(
                    'Different <version> value detected (%s vs. %s) in file %s.' % (this_ver, other_ver, otherFn))
        if not self['xLog_eventType'] is None:
            this_evt = self['xLog_eventType'].getValue().strip()
            other_evt = other['xLog_eventType'].getValue().strip()
            if this_evt != other_evt:
                gFailed = True
                mlog.error('Different <eventType> detected (%s vs. %s) in file %s.' % (this_evt, other_evt, otherFn))
        if not self['xLog_generator'] is None:
            this_gen = self['xLog_generator'].getValue().strip()
            other_gen = other['xLog_generator'].getValue().strip()
            if this_gen != other_gen:
                gFailed = True
                mlog.error('Different <generator> detected (%s vs. %s) in file %s.' % (this_gen, other_gen, otherFn))
        self.selectStream(ithis)
        other.selectStream(iother)
        if gFailed:
            return False
        scThis = self.streamCount()
        scThat = other.streamCount()
        cntIgnore = [] and self.splitStreams or ['pp0']
        cntIgnore += buggyStatObjects
        for kk in GenStats.ChildrenMap.keys():
            bFailed = False
            if scThis != scThat:
                mlog.warning('Different number of streams in files %s[%d] != %s[%d].' %
                             (othisFn, scThis, otherFn, scThat))
            for jj in range(max(scThis, scThat)):
                thisCNames = self.getIdsByType(kk, istream=jj)
                otherCNames = other.getIdsByType(kk, istream=jj)
                if thisCNames is None and otherCNames is not None:
                    mlog.warning('Stream %d not present in %s. Will append stream from %s.' % (jj, othisFn, otherFn))
                    continue
                if otherCNames is None and thisCNames is not None:
                    mlog.warning('Stream %d not present in %s. Will append stream from %s.' % (jj, otherFn, othisFn))
                    continue
                assert otherCNames is None and thisCNames is None, "Cannot work w/ GenStats objects w/ no counters."
                # noinspection PyTypeChecker
                diffThis2Other = [x for x in thisCNames if x not in otherCNames]
                # noinspection PyTypeChecker
                diffOther2This = [x for x in otherCNames if x not in thisCNames]
                info_msg = ''
                if len(diffThis2Other) > 0 or len(diffOther2This) > 0:
                    info_msg = 'Diffs between %s counters on stream %d in file ^1::\'%s\' and ^2::\'%s\':\n' % \
                               (kk, jj, othisFn, otherFn)
                    if kk not in ['crosssection', 'efficiency', 'fraction']:
                        bFailed = True
                    else:
                        # WARN issued for cross-sections and efficiencies as not affected by file count
                        if self.splitStreams:
                            loc = self.__locate('pp0', getLocation=True, selStream=jj)
                            if loc is not None:
                                del self._streams[jj]['crosssection'][loc[0]]
                                mlog.warning('\033[1mSwitching to strategy based on '
                                             'dynamic computing of total cross-section !\033[0m')
                    # allow merging for specified counters that may be missing
                    ignoreThis2Other = [x for x in diffThis2Other if x in cntIgnore]
                    if len(ignoreThis2Other) > 0:
                        diffThis2Other = [x for x in diffThis2Other if x not in ignoreThis2Other]
                    ignoreOther2This = [x for x in diffOther2This if x in cntIgnore]
                    if len(ignoreOther2This) > 0:
                        diffOther2This = [x for x in diffThis2Other if x not in ignoreOther2This]
                    if len(buggyStatObjects) > 0 and len(diffThis2Other) == 0 and len(diffOther2This) == 0:
                        info_msg += 'No remaining diffs. Following missing counters were ignored: %s.' % cntIgnore
                        bFailed = False
                    else:
                        info_msg += 'Diffs between counters after filtering allowed missing counters:'
                if len(diffThis2Other) > 0:
                    info_msg += '\n- %s list in ^1 and not in ^2: %s.' % (kk, ', '.join(diffThis2Other))
                if len(diffOther2This) > 0:
                    info_msg += '\n- %s list in ^2 and not in ^1: %s.' % (kk, ', '.join(diffOther2This))
                if len(info_msg) > 0:
                    if bFailed:
                        mlog.error(info_msg)
                    else:  # for xsection and efficiencies
                        mlog.warning(info_msg)
                gFailed = gFailed or bFailed
        return not gFailed
    
    def findCompatibles(self, others):
        if not isinstance(others, list) or len(others) == 0 or not isinstance(others[0], GenStats):
            return []
        return [x for x in others if self.isCompatible(x)]
    
    def __selftest(self):
        isbk = self._icstream
        for j in range(len(self._streams)):
            self.selectStream(j)
            for kk in GenStats.ChildrenMap.keys():
                sidlist = self.getIdsByType(kk, istream=j)
                for cn in sidlist:
                    if self[cn] is None:
                        mlog.warning('On stream %d, %s %s is None.' % (j, kk, cn))
        self.selectStream(isbk)
    
    def __add__(self, other):
        if not self.isCompatible(other):  # test compatibility between statistics!
            mlog.warning('Ignoring incompatible counter set in \'%s\'...' % (other.getXmlFileName()))
            # keep failing till all missing counters are correctly vetoed
            return self
        # bFailed = False
        osidx = other.selStreamIndex()
        tsidx = self.selStreamIndex()
        try:
            osum = GenStats(prodstats=self)
            # TODO: change isCompatible to avoid filtering twice which is performed below
            scThis = osum.streamCount()
            scThat = other.streamCount()
            if scThis != scThat:
                mlog.warning('Will try to join files with different number of streams [%d != %d].' % (scThis, scThat))
            for jj in range(max(scThis, scThat)):
                if jj >= osum.streamCount():
                    osum._initChildStream()
                for kk in GenStats.ChildrenMap.keys():
                    sidlist = osum.getIdsByType(kk, istream=jj)
                    oidlist = other.getIdsByType(kk, istream=jj)
                    if sidlist is None and oidlist is not None:  # fill data from stream on other
                        mlog.debug('Copy %s on stream %d from other Genstats to be joined...' % (kk, jj))
                        osum.children(jj)[kk] = \
                            [GenStats.ChildrenMap[kk](ocopy=oobj) for oobj in other.children(jj)[kk]]
                        # for ij in range(len(other.children(jj)[kk])):
                        #     osum.children(jj)[kk].append(GenStats.ChildrenMap[kk](ocopy=other.children(jj)[kk][ij]))
                        continue
                    if oidlist is None:
                        oidlist = []
                    if jj < scThis:
                        self.selectStream(jj)
                    if jj < scThat:
                        other.selectStream(jj)
                    newids = sidlist
                    if len(oidlist) > 0:
                        newids = [x for x in oidlist if x not in sidlist]
                    if len(newids) > 0:  # debug
                        mlog.debug('New %s IDs on stream %d: %s' % (kk, jj, str(newids)))
                    for ik in sidlist:
                        oloc = self.__locate(ik, getLocation=True, selStream=jj)
                        # osum.children(jj)[kk].append(GenStats.ChildrenMap[kk](ocopy=self[ik]))
                        if jj > 0 and other[ik] is None:    # correct for missing nodes on other streams
                            continue
                        osum.children(jj)[kk][oloc[0]] += other[ik]
                    for ii in newids:
                        # only for spill-over to keep compatibility and use values from old format logs!
                        # ignore pp0 if current stream does not have one
                        if self.splitStreams and kk == "crosssection" and ii == 'pp0':
                            mlog.debug('Ignoring total cross-section. Will recompute it later...')
                            continue
                        if ii in self:
                            # oloc = self.__locate(ii, getLocation=True, selStream=jj)
                            osum.children(jj)[kk].append(GenStats.ChildrenMap[kk](ocopy=self[ii]))
                        if ii in other:
                            # oloc = other.__locate(ii, getLocation=True, selStream=jj)
                            osum.children(jj)[kk].append(GenStats.ChildrenMap[kk](ocopy=other[ii]))
                # print(len(osum._streams))
        except (ValueError, TypeError, IOError):
            mlog.warning("Error merging object\n%s\nTO\n%s\n### Conflicting GenStats will be ignored...\n" %
                         (str(other), str(self)), exc_info=True)
            # bFailed = True
            # print(exx.args)
            # if bFailed:
            # print(str(self) + '\n' + '#'*13 + '\n' + str(other))
            self.selectStream(tsidx)
            other.selectStream(osidx)
            del osum
            return self
        osum.__selftest()
        osum._fileCounter = self._fileCounter + 1
        osum.selectStream(0)
        self.selectStream(tsidx)
        other.selectStream(osidx)
        osum._xmlFiles.append(os.path.split(other._fullpath)[1])
        osum._xmlSizes.append(other._xmlFileSize)
        osum._src = "%d sum" % (len(osum._xmlFiles))
        osum._fullpath = None   # indicates a sum of objects
        return osum
    
    def brRecompute(self):
        for ii in range(len(self._streams)):
            # mlog.info('Recomputing counters for stream %d: %s' % (ii,  self._genLogs[ii].tool))
            if self._genLogs[ii].fastGen:  # skip fast generation counters
                continue
            sCounters = self.children(ii)
            for kk in sCounters.keys():
                for jj in range(0, len(sCounters[kk])):
                    sCounters[kk][jj].recompute(self)
        self.isMerged = True
    
    def saveFile(self, filepath, sformat='json', compress=False):
        """
        Store (merged) generator statistics in a given format (JSON or XML).
        Beware, the XML format currently saves only data for the signal.
        All data streams are saved in JSON format (generating an array of GenStats objects).
        """
        if os.path.isfile(filepath):
            mlog.warning('File \'%s\' exists. Overwriting it...' % (os.path.split(filepath)[1]))
        if sformat.lower() == 'xml':
            contents = self.encodeXML()
        elif sformat.lower() == 'json':
            contents = self.encodeJSON()
        else:
            raise NotImplementedError('Cannot persistify object in unknown (%s) format.' % sformat)
        if compress:
            fp = gzip.open(filepath + '.gz', 'wb')
        else:
            fp = open(filepath, 'wb')
        fp.write(contents)
        fp.flush()
        fp.close()
    
    def encodeXML(self):
        if self._xdoc is None:
            self._xdoc = xdom.Document()
            de = self._xdoc.createElement(self.RootElementName)
            self._xdoc.appendChild(de)
            cTypes = list(GenStats.ChildrenMap.keys())
            cTypes.sort()
            for i in range(len(cTypes)):
                kk = cTypes[i]
                for ocnt in self.children()[kk]:
                    ocnt.recompute(self)
                    ocnt.toXmlElement(self._xdoc.documentElement)
        return self._xdoc.toprettyxml('  ', '\n')
    
    def encodeJSON(self):
        self.__selftest()
        tstamp = datetime.utcnow().strftime(GenStats.TimeStampFormat)      # as email.utils.formatdate; RFC 2822
        ret = {u'timestamp':  tstamp, u'streams': [], u'isMerged': self.isMerged, u'streamsCRC32': '',
               u'fmtVersion': self._fmtver.version}
        for jj in range(len(self._streams)):
            sData = list()
            for ok, olist in self.children(jj).items():
                for ii in range(len(olist)):
                    obj = olist[ii]
                    sData.append(obj.toDict())
            if 'generator' not in self.children(jj):
                sData.append(self._genLogs[jj].fakeDict('generator'))
            if 'method' not in self.children(jj):
                sData.append(self._genLogs[jj].fakeDict('method'))
            ret[u'streams'].append(sData)
        senc = json.dumps(ret['streams'], sort_keys=True)
        cksum = zlib.crc32(senc) & 0xffffffff
        ret[u'streamsCRC32'] = '0x%08x' % cksum
        ret[u'nbLogs'] = self._fileCounter
        ret[u'xmlFiles'] = self._xmlFiles
        return json.dumps(ret, sort_keys=True, indent=2, separators=(',', ': '))
    
    def loadJSON(self, filepath, compressed=False):
        if compressed and not filepath.endswith('.gz'):
            filepath += '.gz'
        if not os.path.exists(filepath):
            mlog.error('JSON file not found: \'%s\'' % filepath)
            return False
        fconts = self._readFile(filepath, compressed=compressed)
        if fconts is None:
            return False
        odict = json.loads(fconts)
        if u'streamsCRC32' in odict:
            cksuma = int(odict[u'streamsCRC32'], 16)
            senc = json.dumps(odict[u'streams'], sort_keys=True)
            cksumb = zlib.crc32(senc) & 0xffffffff
            if cksuma != cksumb:
                mlog.warning('Found CRC32 checksum for streams data differs from computed checksum.')
                mlog.debug('Stored checksum 0x%08x vs. 0x%08x computed.' % (cksuma, cksumb))
        self.splitStreams = bool(len(odict[u'streams']) > 1)
        self._fileCounter = odict[u'nbLogs']
        self.isMerged = odict[u'isMerged']
        self._xmlFiles = list(odict[u'xmlFiles'])
        self._fmtver = GenStats.fmtVersion(odict[u'fmtVersion'])  # kept <version> in counter list, corruption check!
        for jj in range(len(odict[u'streams'])):
            sData = odict[u'streams'][jj]
            if jj > 0:
                self._initChildStream()
            self.selectStream(jj)
            gl = [x for x in sData if x['type'] == 'generator']
            tl = [x for x in sData if x['type'] == 'method']
            mlog.debug('%s\n%s' % (str(gl), str(tl)))
            if len(gl) == 1 and len(tl) == 1:
                og = GenGlobalParam()
                og.fromDict(gl[0])
                ot = GenGlobalParam()
                ot.fromDict(tl[0])
                if jj == 0:
                    self.genToolSpecial = ot.getValue().lower().find('special') > -1
                self._genLogs.append(GenCounterSet(self, og.getValue(), ot.getValue()))
            for kk in GenStats.ChildrenMap.keys():
                for jsDict in filter(lambda x: x['type'] == kk, sData):
                    obj = GenStats.ChildrenMap[kk]()
                    obj.fromDict(jsDict)
                    self.children()[kk].append(obj)
        self._src = os.path.split(filepath)[1]
        return True
    
    # needs a stream index for multi-streamed parsing.
    # Not such a good idea to choose other index but 0 aka signal stream!
    def makeGenerationResults(self, istream=0):
        ret = {}
        self.selectStream(istream)
        for kk in GenStats.ChildrenMap.keys():
            for ocnt in self.children(istream)[kk]:
                for el in ocnt.getCounters():
                    if isinstance(ocnt, GenGlobalParam):  # just to avoid some warnings!
                        ret[el[0]] = el[1]
                        continue
                    try:
                        ret[el[0]] = float(el[1])
                    except ValueError as vex:
                        mlog.warning("{0}: {1}".format(sys.exc_info()[0], vex))
                        ret[el[0]] = el[1]
                    if el[0] == 'I90':
                        ret['signal'] = ocnt.getPartName()
                    if el[0] == 'I99':
                        ret['anti-signal'] = ocnt.getPartName()
        self.brRecompute()
        # if self.streamCount() < 2 and 'pp0' not in ret:
        #    raise ValueError("Missing total cross-section in log file with one stream.")
        # (!) actually total XS = 0 for special prodTools is a physical HACK - see LHCBGAUSS-1515
        # hack for Special prodTools which output blank total xsection value
        if self._genLogs[0].hasSpecial and 'pp0' in ret:
            if '%3.1f' % ret['pp0'] == '0.0':
                # temporarily add special value to inhibit xsection calcs
                ret['C1N2'] = -1.0
        #        force recomputing the total cross-section
        #  mlog.warning('Production Tool %s does not provide a valid total cross-section.'
        #  % (self._genLogs[0].generator))
        #        del ret['pp0']
        #        # OR use hard process xsection from external generator
        #        #if 'pp9999' in ret:
        #        #    ret['C1'] = ret['pp9999']
        #        #    ret['pp0'] = ret['C1']
        # computing total XS for signal productions
        if 'pp0' not in ret:
            xs = 0.  # will be in mb
            for oxs in self.children(istream)['crosssection']:
                # total xsection derived only from externally provided proc IDs
                if self.genToolSpecial and len(self.specialSignalProcs) > 0 and \
                        oxs.getProcId() not in self.specialSignalProcs:
                    continue
                mlog.debug('Add %s to total XS value %g mb' % (oxs.getProcId(), xs))
                xs += oxs.getValue()
            mlog.warning('Total pp cross-section computed: %6.3f mb' % xs)
            ret['pp0'] = xs
        ret['C1'] = ret['pp0']
        del ret['pp0']
        if 'N2' in ret and 'C1N2' not in ret:
            ret['C1N2'] = ret['C1'] * float(ret['N2'])
        return ret
    
    def __eq__(self, other):
        if not self.isCompatible(other):
            return False
        mycnts = self.makeGenerationResults()
        otcnts = other.makeGenerationResults()
        ret = True
        for (k, v) in mycnts.items():
            if v != otcnts[k]:
                ret = False
                break
        return ret
    
    def getAdditionCounter(self):
        return self._fileCounter


class GenProdStats(object):
    pass

if __name__ == '__main__':
    mlog.error("This is a Python module and should not be used as a script.\n"
               "Import and use it in your scripts in the following way:")
    print(" from GenXmlLogParser import GenStats")
    print(" statObj1 = GenStats(filepath1) # filepath1 can also be an URL using http(s) or ftp")
    print(" statObj2 = GenStats(filepath2)")
    print(" statMerged = statObj1 + statObj2")
    print(" statMerged.brRecompute()  # recomputing is also done automatically when saving the merged XML")
    print("then use saveFile or makeGenerationResults to save merged \"counters\" to XML")
    print("or create a data dictionary to use in GaussStat.py to generate statistics web pages.")
    print("Production metadata is extracted from LHCbDirac book-keeping so a valid grid certificate/proxy is needed.")
    mlog.error('This module cannot be run as a script.')
