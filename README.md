## MCStatTools package

#### Table of contents
1. [Environment Setup](#envsetup)
2. [Generating Statistics Tables](#downloadbuild)
3. [Publish Statistics Tables](#mcstatpub)
4. [Tips and Tools](#mcstattricks)
5. [Development and Support](#devstuff)


This package provides scripts to allow the collection of generator-level counter data 
for Monte Carlo production, and generation of statistics tables (at event generator level) 
as defined in [this Twiki](https://twiki.cern.ch/twiki/bin/view/LHCbBackup/GengaussCounters). 
Details about use cases of this package (mostly of historical importance) can also be consulted 
on this (**:warning: no longer maintained**) [Twiki](https://twiki.cern.ch/twiki/bin/view/LHCb/DownloadAndBuild).

The package contains in the directory `scripts` a series of Python (3.x and later) and (BASH)-shell scripts.

The basic steps to set up the runtime environment and generate statistics table for a given set of production IDs (extracted from [LHCbDIRAC](https://lhcb-portal-dirac.cern.ch/DIRAC/)) are the following:

1. **Environment Setup** <a name="envsetup"></a>

Clone locally the branch of the package you want to use

    git clone https://gitlab.cern.ch/lhcb-datapkg/MCStatTools.git

or

    git lb-clone-pkg McStatTools

(the latter command is available if the LHCb environment is set up).

As the package makes extensive use of the ``LHCbDIRAC`` Python interface, one needs to set up ``LHCbDIRAC``:

``
source /cvmfs/lhcb.cern.ch/lhcbdirac/lhcbdirac
``

Finally, to be able to access the ``LHCbDIRAC`` services you need to generate the authentication token using either one of the following commands:

``lhcb-proxy-init``

``dirac-proxy-init``

In case your forget to do so, the script will try to initialise the proxy for you and respawn itself automatically.

In order to proceed to the next step you will need a list of production IDs which can be obtained from the ``LHCbDIRAC`` web portal (in case you know the production request IDs) or in a few other ways as detailed [here on Twiki](https://twiki.cern.ch/twiki/bin/view/LHCb/DownloadAndBuild#1_Get_the_productions_ID_nuber).

2. **Generate the Statistics Tables** <a name="downloadbuild"></a>

The script to use for this operation is ``DownloadAndBuildStat.py``. Before running it, one should ensure that the local disk has 
enough free space to store the intermediary (XML) files. A rough estimate is about 10-15 MB for each production ID (assuming maximum number of XMLs to download remains at default of 1000).

In the following a short description of the available command-line options for this script is given (more details on [Twiki](https://twiki.cern.ch/twiki/bin/view/LHCb/DownloadAndBuild#2_Retrieve_XML_files_and_produce)):

    $ python DownloadAndBuildStat.py --help  
    
    usage: DownloadAndBuildStat.py [-h] [-n NB_LOGS] [-f BKK_PATHS_FILE] [-r]
                                   [-s SIG_PIDS] [-i] [-w <WG-name>] 
                                   [--use-mem] [--delta-size DELTA] [-l] 
                                   [-d WORKPATH] [-v | -V VERB_LEVEL] 
                                   [PIDs ...]

    positional arguments:
      PIDs                   Comma separated (no blanks) list of production IDs to
         process. The script stops execution if no MC*Simulation production IDs 
         are provided/found for processing. Mind the -r/--req flag which allows using
         Request IDs instead of Production IDs!

    optional arguments:
      -h, --help            show this help message and exit

      -n NB_LOGS, --number-of-logs NB_LOGS
                            number of logs to download [default : 1000]

      -f BKK_PATHS_FILE, --file BKK_PATHS_FILE
                            path to text file containing BKK paths for 
          productions. The file must contain one BKK path per line. 
          (!) <PIDs> are not ignored if also present on command line.

      -r, --req         inform script to consider the <PIDs> as a list of
                        Request IDs and extract MC*Simulation
                        production/transformation IDs automatically from
                        LHCbDIRAC.

      -s SIG_PIDS, --sig SIG_PIDS
                            Signal process ID(s) for special generation tool. 
          Use multiple times to give a list of IDs. Process IDs are event 
          generator specific and should be extracted from event type 
          definition specific file from Gen/DecFiles package. Specify signal
          process IDs for all productions(event types) to process. Default: [].

      -i, --ask-once        ask only once for physics working group name when 
          generating statistics tables and cache value for all productions 
          [default : False].

      -w <WG-name>, --phys-wg <WG-name>
                            Provide physics WG name to use in processing 
          productions. This implies that the script is run non-interactively 
          ('-i' is ignored if also present) and argument value maps to a valid 
          WG repository location. WG names should be w/o trailing '-WG' and 
          may be case insensitive (e.g. bandq for BandQ-WG). Default: "".

      --use-mem             instruct new download algorithm to use memory as 
          temporary buffer [False].

      --delta-size DELTA    XML files smaller by <DELTA> from largest file 
          of sample will be deleted/ignored [default : 0.07]. It is used only 
          when file size distribution is non-gaussian.

      -l, --use-local-logs  Use already downloaded XML logs 
          (to recover processing after crash).

      -d WORKPATH, --dir WORKPATH
                            change work path to provided value. If path is 
          not absolute, a relative path to current directory is resolved 
          [default: os.getcwd()].

      -v, --verbose         Increase logging verbosity by one level 
          specifying multiple times [warn].

      -V VERB_LEVEL, --verb-level VERB_LEVEL
                            case insensitive verbosity level (crit, err, warn,
          info, debug, verb) [warn]. Complete words are also accepted as valid
          values e.g. crit, critical. Verbosity increase  (-v flag) is ignored 
          when level is expresively specified.

Here is detailed information for selected command line flags and parameters: 
* `-n` parameter allows changing the maximum number of XML logs to download and merge. It is recommended not to lower 
the default value to ensure a small error on the merged counter values. Be aware this value is after all just a 
loose upper limit on the number of XML files which will be downloaded and merged since errorneous XMLs cannot be detected _a priori_ 
  during the download procedure and the number is ultimately limited to the number of XMLs archived by the LHCbDIRAC production system.
  
* `-f` makes use of the `LHCbDIRAC` external script `dirac-bookkeeping-prod4path` to determine the MC production IDs automatically. 

* `-r/--req` informs the script that the numerical list of production IDs should be interpreted as Request IDs and corresponding
MC*Simulation production IDs must be extracted from the LHCbDIRAC system. **BEWARE!** Only production IDs with status `Archived`, 
`Completed` or `Idle` are extracted so for requests with on-going Simulation steps, the list may be truncated. 
 
   This flag is for now `False`, but the option will become the default for future versions (after commissioning)

* `-v` flag increases the verbosity level of the script at run time from default `warn`. `-V` could be used to set a specific 
logging level and when present it will take precedence over `-v` as they are mutually exclusive. It is recommended to use `debug` level 
especially when a possible bug is identified and you're collecting log messages to attach to a `LHCBGAUSS` JIRA task reporting the issues.

* `--use-mem` instructs the script to save intermediary files to memory rather than disk. This feature is experimental or in 
other words less tested so please, report any strange behaviour if encountered.

* `--delta-size` specifies the XML file size variation which is tollerated while filtering the download files for possible malformed XML documents. 
In case the file size follows a Gaussian distribution, a 3 sigma tollerance is used instead. This feature is inherited from very old versions of the 
script and it may be dropped in future versions.
  
* `--use-local-logs` instructs the script to avoid re-downloading the XML files and search for them 
in the production-specific directory on disk. Currently, this directory has a name of the form 
`GenLogs-<prodID>`.

* `-i` flag instructs the script to ask only once for the physics WG with which all processed production IDs should be 
associated. This interactive procedure is now implemented to make use of the web API so that direct acess to EOS should be 
optional and scripts should not necessarily be run on lxplus any longer.

* `-w` is used to tell the script which physics WG must all processed production IDs be associated with. This implies 
the script will run in non-interactive mode and the presence of `-i` flag (defined above) will be ignored. 

* `-s` parameter is a special/experimental parameter which enables the script to use an alternate 
method for determining the total generation cross-section. All cross-section values matching the  
(generator specific) event process ID from the given list will be summed into the total interaction cross-section 
(as opposed to summing all cross-section values used by the default method). Thus, if the user would like to
 obtain statistics tables with correct cross-section values (**only**) for special signal productions, 
the generator code for the corresponding event process IDs must be appended to this list. 
Moreover, the user **must not** mix processing of special signal productions with processing of MC 
production for which the signal is extracted from minimum bias as this will most likely result in 
  invalid interaction cross-section values for the latter.

* **PIDs** is a list of comma separated (no blank spaces) numerical IDs associated to the LHCbDIRAC transformation of types `MC*Simulation`
which can be retrieved from the `Data/Transformation Monitor` (and `Data/Production Request`) application(s) on the [LHCbDIRAC web portal](https://lhcb-portal-dirac.cern.ch/DIRAC).
Some further (not very up-to-date nor complete) details about production management in LHCbDIRAC can be consulted on [Twiki](https://twiki.cern.ch/twiki/bin/view/LHCb/ProductionManagementGuide).

The output of the script includes an extensive/trimmed log output that can be also saved into a file using the `tee` command and 
the following files and directories which have double usage as intermediary/final output and data for debugging:

- `Prod_<00_prodID>_Generation_log.json[.gz]` - files allow reloading of production data without re-accessing XML files
- `<PhysApp-WG>_Generation_Sim..-<MC_conditions>[.html | _pid-<prodID>.json[.gz]]` - files contain final statistics tables as (gzipped) JSON dictionaries; the HTML files merge (w/o overwriting) 
all statistics tables for specific WG, production conditions and event type, placing the new tables at the top of the list corresponding to a given event type code 
- `GenLogs-<prodID>` - a directory containing all temporary working data for a given production (most likely only the XML files renamed to be unique).

Please, make sure all these files are copied in a publicly accesible location (see [Tips and Tools](#mcstattricks) for some details about AFS) before proceeding to the next last step, requiring expert intervention for official publication of the statistics tables. Obviously, the same files and the extensively logs would be greatly appreciated by maintainers when reporting bugs.

3. **Publishing the statistics tables** <a name="mcstatpub"></a>

If you are preparing to submit statistics tables for a new physics WG for the first time, please bring up at the [Simulation Developments](https://indico.cern.ch/category/14309/) 
meeting this subject and discuss the feasibility of having a separate repository for the statistics tables corresponding to MC productions for your WG. Please, check first
whether your WG statistics tables for the current Simulation software stack are not already published under a more inclusive MC production physics category. This information 
should usually be passed on from previous MC liaison(s).

The current approach for reporting new tables to be published online, implies creating 
a JIRA task in [LHCBGAUSS](https://its.cern.ch/jira/browse/LHCBGAUSS) project (ask the Simulation coveners for permission if you cannot do this). 
Choose as Component _Generators Statistics_. One should also add the physics WG name, e.g. _PhWG-BnoC_. Please, be aware that there is no one-to-one 
mapping to the corresponding working group repository for published statistics tables. For instance, for **Sim09** the following groups are defined:

|   MCStatTools Nickname    | JIRA PhWG Label | Comments                                                              |
|:-------------------------:|:---------------:|:----------------------------------------------------------------------|
|        B2OpenCharm        |    PhWG-B2OC    | B to Open Charm                                                       |
|           BandQ           |    PhWG-B&Q     | B and Quarkonia                                                       |
|           Charm           | PhWG-Charm | Charm                                                                 |
| _QCD_, _EW_ and _Exotica_ | PhWG-QEE | Please, give production specific category depending on signal process |
| SemiLep | PhWG-SL | Semileptonic decays                                                   |
| Charmless | PhWG-BnoC | Charmless b-hadron decays                                             |
| IFT | PhWG-IFT | Ion and Fixed Target                                                  |
| RD | PhWG-RD | Rare decays                                                           |
| CEP | --- | Central Exclusive Processes/Production |
|PIDCO | PPWG-PID&CO | PID and Calorimeter Objects |

Alternatively, in the title you are welcome to mention the WG for which tables were generated (in case a new WG category needs to be added,
please, discuss the issue and obtain formal approval from conveners during Simulation meeting).
In the description you should give a pointer to the _public_ location containing the tables to be updated - e.g.
public directory on lxplus (AFS) or on EOS/cernbox. The tables will then be added to the Gauss website 
(for [SIM09](http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/index.shtml) and for [SIM10](http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM10STAT/index.shtml)) by the collaboration wide responsible.

As last action, please, check and report in the JIRA task any issues in publishing the tables or acknowledge everything is OK. 
In case you fail to do this action, please, be advised that JIRA tasks are closed tacitly one month after responsible confirms publication of tables on the website 
(automatically assuming no issues appeared when publishing). For further discussion about the specific tables, please, feel free to re-open the JIRA task.

At the moment, for publishing new statistics tables, **please**, limit the number of JIRA tasks you open and most importantly, *please*, refrain from re-opening older ones. 

As JIRA use is gradually limited and/or replaced, the [MCStatTools GITlab repository](https://gitlab.cern.ch/lhcb-datapkg/MCStatTools) provides an issue template (*mc_prod_stat_tables*) 
which can be used as an alternative to opening JIRA tasks - provided the completion instructions are followed.


4. **Tricks and Tools** <a name="mcstattricks"></a>

* Sometimes, when statistics tables are created on various filesystems and then copied for sharing 
in your `public` area, it is possible that copied files will retain some restrictive access rights. To 
ease processing of such tables it would be best that MC liaison first checks that the `z5` group has 
read access consistently for the shared directory tree. Furthermore, on AFS the ACLs take precedence 
over the user/group access rights. To check the ACL for a given directory/files one could use the 
command

      $ fs listacl <path>

   To be sure the shared directory will be accessible globally you want to have the following among the listed ACLs:

      system:anyuser rl

  which means that globally the directory is listable and readable. To correct this AC and allow access one would use

      $ fs setacl -dir <path> -acl system:anyuser rl

  or alternatively for a directory tree

      $ find <root-path> -type d -exec fs setacl -dir {} -acl system:anyuser rl \;

  which will traverse the directory tree used root `<root-path>` and change the ACL for each directory it encounters.

* To quickly check MC production definition starting with production IDs users may 
  use the `show_prod_meta.py` script. This script can be used also to check whether a 
  specific production has XML log files stored in LHCbDIRAC dedicated storage element - 
  alternatively the script will spit out lots of errors and this should be interpreted 
  as a clear sign that either something is really wrong with LHCbDIRAC or most likely 
  the XML log files for the specified production were stored on the old CASTOR tapes 
  and they require special treatment by the _Production Team_.

* Latest table update logs for each physics and applications working group (PAWG) are also 
available in plain text for browsers able to process gzipped content. For now logs are stored in 
  files named according to the template `log.<PAWG name in lower case>.[nn.]gz` where `nn` runs from 1 to 9
  so that up to 9 logs are kept online. E.g. the log for the latest update to tables for the 
  Semileptonics PAWG are available [here](http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/log.semilep.gz).

* Please, contact maintainer for any further information you would like to appear here.


5. **Development and Support** <a name="devstuff"></a>

Code, patches and personal time contributions are always welcome! Of course, you should rather use `ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/MCStatTools.git` for cloning.
This package also follows the main policy in LHCb collaboration of protecting `master` and development branches from direct pushing and encourage code reviews before accepting merge requests.
So please, always create a new branch with your changes and keep the main package maintainers in the loop by involving them in the code review.

Though it depends on `LHCbDIRAC` Python interface and the package may be unusable if no production version for Python 2.x is available, one could clone the old (Python2) master branch (tagged `v4r7p10`), for legacy purposes using:

    git clone -b v4r7p10 https://gitlab.cern.ch/lhcb-datapkg/MCStatTools.git
