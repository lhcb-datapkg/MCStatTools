## Read-only web API for MCStatTools database

A HTTP (un)REST(full) API is (being) developed to provide programatic access to the repository containing the generator statistics information (in JSON format). The responses are in JSON format which 
can be easily processed as native JS-object or Python dictionary. Entry point parameters are presented as 
`key : value` pairs which would need apropriate encoding depending on the way the HTTP requests are implemented.

The entry point for the API is [https://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/api/v1/](https://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/api/v1/).
Accessing the point without arguments will return the generic error response of the form:

```json
{"error":"Cannot identify data with incomplete request fields (or other error message)."}
```

Most of the functionality of the interface is accessible through HTTP GET requests (see Python requests package for client implementations) - some of the functionality must, at the moment, be accessed
using POST requests, but there final goal is to have a uniform access model based only on GET. Due to basic implementation, for this version the document name in the API accessing URL is being 
ignored and only the URL parameters are used to provide the implemented services.
 
Here follows a list of these parameters:

* In order to retrieve the nickname and decay descriptor for a given event type number one can use:

  _GET params_: `decfiles : vXrY`; `evttype : <8-digit event number>`

  _Response_: ```{"evttype": "<8-digit nb>", "nick": "<nick_name>", "decay": "<decay_descriptor>"}```

* To retrieve a list of physics working groups defined for the repository use parameter `pawg` (e.g. [https://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/api/v1/?pawg](https://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/api/v1/?pawg)).

   _Response_: ```{"WG_names": ["name1", "name2", ...]}```

* To list the configurations used in official MC production for a particular physics working group:

  _GET params_: `wg : <WG_name>`; `appconfs : "list"`

  _Response_: ```{"wgName": <WG_name>, "appcfgs": ["Beam6500GeV-2022-MagUp-...", ...]}```

  Mind the APPCONFIG tags do not contain the Simulation version prefix of the form `SimNN-`

Following services will be soon implemented.

* For given physics working group and APPCONFIG tag, list event type numbers corresponding to storage statistics tables:
  _GET params_: `wg: <WG_name>`; `appcfg : <APPCONFIG_tag>`; `evtTypes: "list"`; `unlisted: "yes|[no]"` (optional parameter)
  _Response_: `{"wgName": "<WG_name>", "appcfg_tag": "<APPCONFIG_tag>", "evtTypes": ["30000002", ...]}`
  `unlisted` parameter has default value "no"; when "yes" also records unlisted on web are taken into account

* For given physics working group and APPCONFIG tag, retrieve all corresponding MC productions IDs

* For given physics working group, APPCONFIG tag and event type number retrieve the statistics table records corresponding 
to samples produced using different subversions of the Simulation software stack.

* TODO: Extend API to include access services to obsolete statistics tables.
